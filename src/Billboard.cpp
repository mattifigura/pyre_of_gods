#include "Billboard.h"
#include "SceneManager.h"

Billboard::Billboard(Manager* parent, vector2df size)
	: GLObject(parent, vector3df(), vector3di())
	, m_fAnimSpeed(0.175f)
	, m_fFrameWidth(0)
	, m_fFrameHeight(0)
	, m_iXFrames(0)
	, m_FrameSize(size)
{

}

void Billboard::Update(bool bForce)
{
	// if we aren't visible then don't update anything
	if (!bForce && !IsVisible())
		return;

	// get delta time
	double delta = DeltaTime::GetDelta();
	
	// update each billboard's quads based on the camera's rotation
	vector<GLVertex> verts = GetVertices();
	if (verts.size())
	{
		GLfloat dir = GetSceneManager()->GetActiveCamera()->GetDirection();
		GLfloat c = GetSceneManager()->GetActiveCamera()->GetDirectionCos();
		GLfloat s = GetSceneManager()->GetActiveCamera()->GetDirectionSin();

		for (GLuint i = 0; i < m_Boards.size(); ++i) {
			// update the billboard position
			Board *b = &m_Boards[i];
			if (b->dead)
				continue;

			// update the billboard
			b->frame += b->animspeed * delta;

			if (b->frame >= m_Animations[b->anim].size())
			{
				b->frame = 0;
				OnAnimationFinish(i);
			}
			else if (b->frame < 0)
			{
				b->frame = m_Animations[b->anim].size() - 0.01f;
				OnAnimationFinish(i);
			}

			vector3df pos = b->pos;
			vector2di size = b->size;

			ChangeQuadCoords(i,
				pos + vector3df((GLfloat)-size.x * s, (GLfloat)size.y, (GLfloat)-size.x * c),
				pos + vector3df((GLfloat)size.x * s, (GLfloat)size.y, (GLfloat)size.x * c),
				pos + vector3df((GLfloat)size.x * s, (GLfloat)-size.y, (GLfloat)size.x * c),
				pos + vector3df((GLfloat)-size.x * s, (GLfloat)-size.y, (GLfloat)-size.x * c), false);

			vector2df frame = m_Animations[b->anim][(int)b->frame];
			ChangeQuadUV(i, frame, vector2df(frame.x + m_fFrameWidth, frame.y + m_fFrameHeight), false);
		}
		UpdateVBO();

		CalculateInView(m_Boards[0].pos);
	}

	// call GLObject::UpdateTransformationMatrix()
	GLObject::UpdateTransformationMatrix();
}

void Billboard::Render()
{
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	//glDepthMask(false);
	//glDisable(GL_CULL_FACE);
	GLObject::Render();
	//glEnable(GL_CULL_FACE);
	//glDepthMask(true);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void Billboard::RenderShadows()
{
	// do nothing
}

void Billboard::SetTexture(const char* fileName)
{
	// load the texture
	GLObject::SetTexture(fileName);

	// set the frame width and height accordingly
	m_fFrameWidth = m_FrameSize.x / GetTextureWidth();
	m_fFrameHeight = m_FrameSize.y / GetTextureHeight();

	// set the number of frames
	m_iXFrames = GetTextureWidth() / m_FrameSize.x;

	//cout << m_fFrameWidth << "x" << m_fFrameHeight << " - " << GetTextureWidth() << "x" << GetTextureHeight() << endl;

	// remake the animations
	RemakeAnimations();
}

GLuint Billboard::AddBoard(Board& board, bool bCreate)
{
	GLuint pos = m_Boards.size();
	m_Boards.push_back(board);
	AddQuad(GLVertex(vector3df(), GLColour(255), vector2df(), GLNormal()),
		GLVertex(vector3df(), GLColour(255), vector2df(1, 0), GLNormal()),
		GLVertex(vector3df(), GLColour(255), vector2df(1, 1), GLNormal()),
		GLVertex(vector3df(), GLColour(255), vector2df(0, 1), GLNormal()));

	if (bCreate)
		CreateBufferObjects();

	return pos;
}

void Billboard::SetAnimationSpeed(GLfloat speed)
{
	m_fAnimSpeed = speed;
}

float Billboard::GetAnimationSpeed()
{
	return m_fAnimSpeed;
}

GLuint Billboard::AddAnimation(vector2di start, vector2di end)
{
	// add an animation to this animated object
	vector<vector2df> anim;
	vector<vector2di> animframes;

	int iX = start.x;
	int iY = start.y;
	int length = (end.y * m_iXFrames) - (start.y * m_iXFrames) + (end.x + 1) - start.x;

	for (int i = 0; i < length; ++i) {
		anim.push_back(vector2df(m_fFrameWidth * iX, m_fFrameHeight * iY));
		animframes.push_back(vector2di(iX, iY));
		++iX;

		if (iX >= m_iXFrames)
		{
			iX = 0;
			++iY;
		}
	}

	m_Animations.push_back(anim);
	m_AnimationFrames.push_back(animframes);
	return m_Animations.size() - 1;
}

void Billboard::RemakeAnimations()
{
	// called after texture is loaded so that the animations are handled correctly
	for (GLuint i = 0; i < m_Animations.size(); i++) {
		for (GLuint j = 0; j < m_Animations[i].size(); j++) {
			m_Animations[i][j].Set(m_AnimationFrames[i][j].y * m_fFrameWidth, m_AnimationFrames[i][j].x * m_fFrameHeight);
		}
	}
}

void Billboard::SetAnimation(GLuint pos, GLuint anim)
{
	m_Boards[pos].anim = anim;
}

GLuint Billboard::GetAnimation(GLuint pos)
{
	return m_Boards[pos].anim;
}

void Billboard::SetCurrentFrame(GLuint pos, GLfloat frame)
{
	m_Boards[pos].frame = frame;
}

void Billboard::OnAnimationFinish(GLuint pos)
{
	// this can be overriden by classes (ie particle emitter) and is called
	// when a board's animation ends
}