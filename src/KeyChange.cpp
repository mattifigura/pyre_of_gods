#include "KeyChange.h"
#include "GUIManager.h"
#include "Game.h"

KeyChange::KeyChange(Manager* parent, string control)
	: YesNoDialog(parent, "Enter New Key...", "Enter a new key:", GameGUI::KeyBindingChange)
	, m_ControlName(control)
	, m_Key(0)
{
	// change this to an accept/cancel dialog
	Button* b = dynamic_cast<Button*>(GetChild(0)->GetChild(2));
	if (b)
		b->SetText("Accept");
	b = dynamic_cast<Button*>(GetChild(0)->GetChild(3));
	if (b)
		b->SetText("Cancel");

	// get the game gui
	m_pGameGUI = parent->GetRoot()->CastTo<Game>()->GetGameGUI();

	// add the key text
	vector2df sized(460, 240);
	m_KeyText = GetGUIManager()->CreateText("########################################", vector2df(sized.x * 0.5f - 100, 100), vector2di(200, 30), GetGUIManager()->GetFont("font20b"), GetChild(0));
	m_KeyText->CentreText(true);

	// make sure this is a menu object
	SetMenu();
}

void KeyChange::Update(bool bForce)
{
	GLint key = GetGUIManager()->GetLastKey();

	if (key != m_Key)
	{
		m_Key = key;
		m_KeyText->SetText(m_pGameGUI->GetKeyName(key));
	}

	GUIElement::Update(bForce);
}

string KeyChange::GetControl()
{
	return m_ControlName;
}

GLint KeyChange::GetKey()
{
	return m_Key;
}