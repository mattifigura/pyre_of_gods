#ifndef _INGREDIENT_H_
#define _INGREDIENT_H_

#include "GUIElement.h"
#include "MeshView.h"
#include "Text.h"

struct ItemClass;

class Ingredient : public GUIElement {
	public:
		Ingredient(Manager* parent, GUIElement* guiParent, vector3df pos, const ItemClass* ic, GLuint amount, GLuint amountOwned);

		virtual void MouseDownFunction(GLuint button);

	protected:

	private:
		Text*		m_Name;
		Text*		m_Amount;
		string		m_FullName;
		MeshView*	m_MeshView;
};

#endif