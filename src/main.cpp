#include <iostream>
#include "Game.h"
#include "NetworkManager.h"

// The "Pyre of Gods" game - a voxel based game that is now abandoned

int main(int argc, char** argv)
{
	// PyreOfPixels :)
	cout << "Welcome to Pyre Of Gods!" << endl;

	// create the game object and run it!
	Game* PyreOfGods = new Game();
		
	if (!PyreOfGods->Create())
	{
		delete PyreOfGods;
		return -1;
	}
	
	PyreOfGods->Run();

	delete PyreOfGods;

	return 1;
}
