#ifndef _INVENTORY_H_
#define _INVENTORY_H_

#include "MeshView.h"
#include "Text.h"
#include "Item.h"

struct ItemFilter {
	ItemType type_filter;
	SubType subtype_filter;

	ItemFilter() { type_filter = NO_TYPE; subtype_filter = NO_SUBTYPE; }
	ItemFilter(ItemType type, SubType subtype = NO_SUBTYPE) { type_filter = type; subtype_filter = subtype; }
	void operator=(const ItemFilter& other) { type_filter = other.type_filter; subtype_filter = other.subtype_filter; }
	bool operator==(const ItemFilter& other) { return (type_filter == other.type_filter && subtype_filter == other.subtype_filter); }
	bool operator!=(const ItemFilter& other) { return !(*this == other); }
};

class Inventory;

class InventoryCell : public GUIElement {
	public:
		InventoryCell(Manager* parent, Inventory* guiParent, vector3df pos, vector3di size, GLuint quad);

		bool SetItem(Item* item);
		Item* GetItem();
		void UpdateText();
		void SetText(int amount);
		void SetItemFilter(const ItemFilter& filter);
		bool CompareItemFilter(const ItemFilter& filter);
		bool CompareItemToItemFilter(const ItemFilter& filter);
		bool PassesFilters(Item* item);

		void ItemLabel();

		virtual bool IsHovered();
		virtual void Update(bool bForce = false);
		virtual void MouseDownFunction(GLuint button);

	protected:

	private:
		Item* m_Item;
		Text* m_Text;
		MeshView* m_MeshView;
		Inventory* m_Inventory;
		GLuint m_Quad;
		bool m_bSwitched;
		ItemFilter m_Filter;

		bool FilterCheck(const ItemClass* ic, const ItemFilter& filter = ItemFilter());
};

class Inventory : public GUIElement {
	public:
		Inventory(Manager* parent, GUIElement* guiParent, vector<Item*>* inventory, GLuint slots, vector3df pos, vector3di size, bool bBackground = true);

		virtual void MouseDownFunction(GLuint button);
		virtual void SetTexture(Texture tex);
		virtual void Update(bool bForce = false);

		void ChangeQuadColour(int pos, GLColour col1, GLColour col2, GLColour col3, GLColour col4);

		InventoryResult	AddItem(Item* item);
		Item*			GetItem(GLuint slot);
		Item*			FindNextItem(Item* item, GLint& slot);
		Item*			FindNextItem(const ItemClass* item, GLint& slot);
		Item*			FindItem(const ItemFilter& filter);
		GLuint			FindItemSlot(Item *item);
		int				GetItemAmount(const ItemClass* item);
		int				GetItemAmount(const ItemFilter& filter);
		bool			SetItem(Item *item, GLuint slot);
		void			RemoveItem(GLuint slot);
		void			DecreaseStackSize(GLuint slot, GLshort amount = 1);
		GLuint			GetSlotCount();
		GLuint			GetSlot(InventoryCell* ic);
		InventoryCell*	GetCell(GLuint slot);
		void			MoveCell(GLuint slot, const vector2df& pos);

		void			SetSlotLeftClicked(void(*fn)(GUIElement*));
		void			SetSlotRightClicked(void(*fn)(GUIElement*));

		static void		CellLeftClicked(GUIElement* e);
		static void		CellRightClicked(GUIElement* e);

		Item*			GetHeldItem();
		void			RemoveHeldItem();
		void			DropHeldItem();
		void			DecreaseHeldItemStackSize(GLshort amount = 1);

		void			UpdateFromInventory();

		static void		ClearCursorItem();

	protected:
		static Item* m_CursorItem;
		static MeshView* m_CursorMesh;

	private:
		//Item* (*m_Inventory)[40]; // 40 is the required size of inventory arrays - not all 40 need be used however
		vector<Item*>*	m_Inventory;
		GLuint			m_Slots;
		bool			m_bCreated;

		int GetFreeSlot(Item* item);

		void UpdateInventory(Item* item, GLuint slot);
};

#endif
