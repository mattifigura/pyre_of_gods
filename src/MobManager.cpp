#include "MobManager.h"
#include "SceneManager.h"
#include "NetworkManager.h"
#include "GUIManager.h"
#include <sstream>

MobManager::MobManager(SceneManager* sceneman)
	: m_pSceneMan(sceneman)
	, m_Mobs(100, NULL)
	, m_GUIMobHealthBars(100, NULL)
{
	// create a new buffer
	m_Buffer = new unsigned char[MOB_BUFFER_SIZE];
	memset(m_Buffer, 0, MOB_BUFFER_SIZE);
	m_RLen = 0;
	m_MsgBuffer = new unsigned char[BUFFER_SIZE];
	memset(m_MsgBuffer, 0, BUFFER_SIZE);
}

MobManager::~MobManager()
{
	// clean up
	delete[] m_Buffer;
	delete[] m_MsgBuffer;
}

void MobManager::Update()
{
	// if we have data to unpack, do so now
	if (m_RLen > 0 && Mob::MobsLoaded())
	{
		// uncompress the data
		uLongf size = MOB_BUFFER_SIZE;
		uncompress(m_Buffer, &size, m_MsgBuffer, m_RLen);

		// get the gui manager
		GUIManager* pGUIMan = dynamic_cast<GUIManager*>(m_pSceneMan->GetRoot()->GetManager(gui_manager));

		// loop through the data and sort out mobs!
		int pos = 0;
		const size_t len = sizeof(MobData);
		while (pos < size)
		{
			// extract mob data
			MobData data = MobDataFromBuffer(&m_Buffer[pos]);

			// check if a mob exists at the location
			Mob* mob = m_Mobs[data.index];
			
			if (mob)
			{
				// firstly check that this is actually the same mob and not a freak overwrite
				MobData& currentData = mob->GetMobData();
				if (currentData.maxhealth != data.maxhealth || currentData.id != data.id)
				{
					// this must be a different mob, delete this mob and create a new one
					m_pSceneMan->DeleteObject(mob);
					mob = NULL;
					m_Mobs[currentData.index] = NULL;
					GUIElement::Close(m_GUIMobHealthBars[currentData.index]);
					m_GUIMobHealthBars[currentData.index] = NULL;
				}
				else
				{
					// check for health change
					int health = currentData.health;
					bool bDead = health == 0;
					if (data.health < health)
					{
						// this mob has been damaged, show some damage text
						std::stringstream ss;
						ss << health - data.health;

						vector3df pos3(currentData.pos + vector3df(-3 + rand() % 6, 7 + rand() % 8, -4 + rand() % 6));
						vector2df pos = m_pSceneMan->ConvertToScreenCoordinates(pos3);
						Text* damage = pGUIMan->CreateText(ss.str().c_str(), pos, vector2di(100, 25), pGUIMan->GetFont("font20b"));
						damage->SetAlpha(0.9f);
						damage->AnchorToWorld(pos3);
						damage->SetAnimationSmoothness(48.f);
						damage->FadeTo(0, false, true);
					}

					// update the mob existing here
					currentData = data;

					// update the health bar
					ProgressBar* hpbar = m_GUIMobHealthBars[data.index];
					hpbar->SetCurrentPosition(data.health);

					// check if we need to delete this mob
					if (bDead || data.health <= 0)
					{
						m_pSceneMan->DeleteObject(mob);
						m_Mobs[data.index] = NULL;
						GUIElement::Close(hpbar);
						m_GUIMobHealthBars[data.index] = NULL;
					}
				}
			}
			
			if (!mob)
			{
				// check if this is a dead mob
				if (data.health <= 0)
				{
					pos += len;
					continue;
				}
				
				// we need to create a new mob here
				mob = m_pSceneMan->CreateMob(data.id, data.pos);
				mob->GetMobData() = data;
				m_Mobs[data.index] = mob;

				// create a mob health bar
				ProgressBar* hpbar = pGUIMan->CreateProgressBar(vector2df(), vector2di(100, 23));
				hpbar->SetMaxValue(data.maxhealth);
				hpbar->SetCurrentPosition(data.health);
				hpbar->SetColours(0, GLColour(200, 30, 100, 100), GLColour(250, 130, 100, 40));
				hpbar->SetColours(2, GLColour(200, 30, 100, 100), GLColour(250, 130, 100, 40));
				hpbar->SetAlpha(0.75f);
				m_GUIMobHealthBars[data.index] = hpbar;
				m_Mobs[data.index]->AttachHealthBar(hpbar);
			}

			// move onto the next position
			pos += len;
		}

		// reset data length
		m_RLen = 0;
	}
}

void MobManager::ReceiveData(unsigned char* buffer, int rlen)
{
	// copy the data into our internal buffer
	memset(m_MsgBuffer, 0, BUFFER_SIZE);
	memcpy(m_MsgBuffer, buffer, rlen);
	m_RLen = rlen;
}

const vector<Mob*>* MobManager::GetMobs() const
{
	return &m_Mobs;
}