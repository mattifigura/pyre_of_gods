#ifndef _MISC_H_
#define _MISC_H_

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <vector>
#include <thread>
#include <functional>
#include <iostream>
#include <locale>
#include <sys/stat.h>

#include "Matrix.h"
#include "Plane.h"
#include "Triangle.h"
#include "TextureManager.h"
#include "DeltaTime.h"
#include "MessageDefinitions.h"
#include "zlib.h"

// this file contains useful code used throughout PyreOfGods

// for texture loading I really can't be bothered to type TextureManager::GetTexture()
// so instead we have a static pointer to the function :)
//typedef GLuint (*texture_load)(const char*);

//const texture_load LoadTexture = &TextureManager::GetInstance()->GetTexture;

/*#define VERIFY_ITERATOR(it) \
	if (!*it) \
	{ \
	cout << "Iterator invalid!" << endl; \
	break; \
	}*/

using std::cout;
using std::cin;
using std::endl;
using std::vector;
using std::map;
using std::string;
using std::pair;

#define INPUT_MOUSE 0
#define INPUT_KEY 1
#define INPUT_CHAR 2

enum {
	scene_manager = 0,
	gui_manager
};

class Root;

class Manager {
public:
	Manager(Root* root, TextureManager* texMan) : m_pTexMan(texMan), m_pRoot(root) {}
	virtual ~Manager() {}

	TextureManager* GetTextureManager() { return m_pTexMan; }
	Root* GetRoot() { return m_pRoot; }
	template<class T> T* CastTo() { return dynamic_cast<T*>(this); }

protected:
	TextureManager* m_pTexMan;
	Root*			m_pRoot;

private:

};

class Root {
	public:
		Root() {};
		virtual ~Root() { for (unsigned int i = 0; i < m_Managers.size(); i++) delete m_Managers.at(i); m_Managers.clear(); };

		void AddManager(Manager* man) { m_Managers.push_back(man); }
		Manager* GetManager(int index) { return m_Managers.at(index); }
		template<class T> T* CastTo() { return dynamic_cast<T*>(this); }

		vector2di& GetScreenSize() { return m_ScreenSize; }
		void SetScreenSize(GLuint width, GLuint height) { m_ScreenSize.Set(width, height); }

	protected:

	private:
		vector<Manager*>	m_Managers;
		vector2di			m_ScreenSize;
};

static bool IsMouseInRect(vector2dd& mouse, vector2df pos1, vector2df pos2)
{
	return ((mouse.x >= pos1.x && mouse.x <= pos2.x) && (mouse.y >= pos1.y && mouse.y <= pos2.y));
}

template<typename T> static void Interpolate(T& current, T result, float smooth = 0.05f, float diff = 0.1f)
{
	T fDif = result - current;
	double delta = DeltaTime::GetDelta() * 100;

	if (abs(fDif) < diff)
	{
		current = result;
	}
	else
	{
		current += (fDif / smooth) * (T)delta;
	}
}

template<typename T> static void Limit(T &var, T lower, T upper)
{
	if (var < lower)
	{
		var = lower;
	}
	else if (var > upper)
	{
		var = upper;
	}
}

template<class fn, class... arg>
static void RunInThread(fn& function, arg&&... args)
{
	std::thread(function, args...).detach();
}

template<typename T> static T max(T val1, T val2)
{
	return (val1 > val2 ? val1 : val2);
}

template<typename T, typename... Ts> static T max(T val1, T val2, Ts&&... args)
{
	return (val1 > val2 ? max(val1, std::forward<Ts>(args)...) : max(val2, std::forward<Ts>(args)...));
}

template<typename T> static T min(T val1, T val2)
{
	return (val1 < val2 ? val1 : val2);
}

template<typename T, typename... Ts> static T min(T val1, T val2, Ts&&... args)
{
	return (val1 < val2 ? min(val1, std::forward<Ts>(args)...) : min(val2, std::forward<Ts>(args)...));
}

template<typename T> static void SetBit(T &n, int pos, int val)
{
	if (val == 1)
	{
		n |= 1 << pos;
	}
	else if (val == 0)
	{
		n &= ~(1 << pos);
	}
}

template<typename T> static bool GetBit(const T n, int pos)
{
	return (n & 1 << pos) == (1 << pos);
}

static void ToUV(vector4df& dest, vector2di src)
{
	float s = 32.0f / 1024.0f;
	dest.x = src.x * s;
	dest.z = (src.x + 1) * s;
	dest.y = src.y * s;
	dest.w = (src.y + 1) * s;
}

static bool RayCollisionAABB(const line3df& l, const vector3df& bl, const vector3df& tr, const vector3df& oo, GLfloat& out)
{
	//if (l.start >= bl && l.start <= tr)
	//	return true;
	
	GLfloat t1 = (bl.x - l.start.x) * oo.x;
	GLfloat t2 = (tr.x - l.start.x) * oo.x;
	GLfloat t3 = (bl.y - l.start.y) * oo.y;
	GLfloat t4 = (tr.y - l.start.y) * oo.y;
	GLfloat t5 = (bl.z - l.start.z) * oo.z;
	GLfloat t6 = (tr.z - l.start.z) * oo.z;

	GLfloat tMin = max(min(t1, t2), min(t3, t4), min(t5, t6));
	GLfloat tMax = min(max(t1, t2), max(t3, t4), max(t5, t6));

	if (tMax < 0)
		return false;

	if (tMin > tMax)
		return false;

	out = tMin;
	return true;
}

static string ToLower(string s)
{
	string out = "";
	std::locale loc;
	for (GLuint i = 0; i < s.length(); ++i) {
		out += std::tolower(s[i], loc);
	}
	return out;
}

static float OneOver(GLuint n)
{
	switch (n)
	{
		case 0:	
		case 1:
		default:
			return 1;
		case 2:
			return 0.5f;
		case 3:
			return 0.3333f;
		case 4:
			return 0.25f;
		case 5:
			return 0.2f;
		case 8:
			return 0.125f;
		case 15:
			return 0.06667f;
		case 17:
			return 0.058823f;
	}
}

struct ParticleDefinition {
	string texture;
	vector3df pos;
	vector3df dir;
	GLfloat speed;
	GLuint amount;
	vector2df psize;
	GLfloat spread;
	GLfloat alpha;
	vector2df size;
	vector<std::pair<vector2di, vector2di>> anims;
};

template <class function, class... arguments> static void Timer(unsigned int time, bool newthread, function&& fn, arguments&&... args)
{
	std::function<typename std::result_of<function(arguments...)>::type()> execute(std::bind(std::forward<function>(fn), std::forward<arguments>(args)...));

	if (newthread)
		std::thread([time, execute]() {
		std::this_thread::sleep_for(std::chrono::milliseconds(time));
		execute();
	}).detach();
	else
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(time));
		execute();
	}
}

static void IntToByte(const int& in, unsigned char* out)
{
	union {
		int i;
		unsigned char b[4];
	} conv;
	conv.i = in;
	memcpy(out, conv.b, 4);
}

static void ByteToInt(const unsigned char* in, int& out)
{
	union {
		int i;
		unsigned char b[4];
	} conv;
	memcpy(conv.b, in, 4);
	out = conv.i;
}

static void UIntToByte(const unsigned int& in, unsigned char* out)
{
	union {
		unsigned int i;
		unsigned char b[4];
	} conv;
	conv.i = in;
	memcpy(out, conv.b, 4);
}

static void ByteToUInt(const unsigned char* in, unsigned int& out)
{
	union {
		unsigned int i;
		unsigned char b[4];
	} conv;
	memcpy(conv.b, in, 4);
	out = conv.i;
}

static void ShortToByte(const short& in, unsigned char* out)
{
	union {
		short s;
		unsigned char b[2];
	} conv;
	conv.s = in;
	memcpy(out, conv.b, 2);
}

static void ByteToShort(const unsigned char* in, short& out)
{
	union {
		short s;
		unsigned char b[2];
	} conv;
	memcpy(conv.b, in, 2);
	out = conv.s;
}

static void UShortToByte(const unsigned short& in, unsigned char* out)
{
	union {
		unsigned short s;
		unsigned char b[2];
	} conv;
	conv.s = in;
	memcpy(out, conv.b, 2);
}

static void ByteToUShort(const unsigned char* in, unsigned short& out)
{
	union {
		unsigned short s;
		unsigned char b[2];
	} conv;
	memcpy(conv.b, in, 2);
	out = conv.s;
}

static void FloatToByte(const float& in, unsigned char* out)
{
	union {
		float f;
		unsigned char b[4];
	} conv;
	conv.f = in;
	memcpy(out, conv.b, 4);
}

static void ByteToFloat(const unsigned char* in, float& out)
{
	union {
		float f;
		unsigned char b[4];
	} conv;
	memcpy(conv.b, in, 4);
	out = conv.f;
}

static bool DirectoryExists(string name)
{
	// use stat to determine if the given directory exists
	struct stat info;

	// check if an object exists with the given name
	if (stat(name.c_str(), &info) != 0)
		return false;

	// check if this is a directory
	if (info.st_mode & S_IFDIR)
		return true;

	// this isn't a directory so return false
	return false;
}

static bool FileExists(string name)
{
	// use stat to determine if the given file exists
	struct stat info;

	// check if an object exists with the given name
	if (stat(name.c_str(), &info) != 0)
		return false;

	// check if this is a directory
	if (info.st_mode & S_IFDIR)
		return false;

	// this must be an existing file so return true
	return true;
}

#pragma pack(1)
struct Stats {
	unsigned short health;
	unsigned short energy;
	unsigned short maxhealth;
	unsigned short maxenergy;

	static unsigned char* ToBuffer(const Stats& stats) { return reinterpret_cast<unsigned char*>(const_cast<Stats*>(&stats)); }
	static Stats& FromBuffer(unsigned char* buffer) { return *reinterpret_cast<Stats*>(buffer); }
};
#pragma pack()

// animations
struct AnimDetails {
	string name;
	GLfloat speed;
};

struct AnimFrame {
	vector<vector3df> rotations;
	vector<vector3df> translations;
	GLfloat time;

	AnimFrame() : time(0.f) {}

	AnimFrame(GLuint size, GLfloat t = 0.f)
		: time(t)
	{
		for (GLuint i = 0; i < size; ++i) {
			rotations.push_back(vector3df());
			translations.push_back(vector3df());
		}
	}

	AnimFrame(const AnimFrame& other)
		: time(other.time)
	{
		for (GLuint i = 0; i < other.rotations.size(); ++i) {
			rotations.push_back(other.rotations[i]);
			translations.push_back(other.translations[i]);
		}
	}
};

struct Animation {
	map<GLfloat, AnimFrame> frames;
	GLfloat length;

	Animation() : length(10.f) {}
};

static GLfloat Lerp(GLfloat s, GLfloat e, GLfloat t)
{
	return (s * (1.f - t) + e * t);
}

static vector3df Lerp(vector3df s, vector3df e, GLfloat t)
{
	return vector3df(Lerp(s.x, e.x, t), Lerp(s.y, e.y, t), Lerp(s.z, e.z, t));
}

static GLint GetFaceFromNormal(const vector3di& normal)
{
	// get an integer representation of the face
	if (normal.x < 0) return 0;
	if (normal.x > 0) return 1;
	if (normal.z < 0) return 2;
	if (normal.z > 0) return 3;
	if (normal.y < 0) return 4;
	if (normal.y > 0) return 5;

	// by default return 0
	return 0;
}

// GLVertex structure
// The structure is as follows (in bytes):
//  0  0  0  0  0  0  0  0  0  0  1  1  1  1  1  1  1  1  1  1  2  2  2  2  2  2  2  2  2  2  3  3 
//  0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5  6  7  8  9  0  1 
// [ X        ][ Y        ][ Z        ][ R G B A  ][ UV X     ][ UV Y     ][ Normal   ][ Light    ]
#pragma pack(1)
struct GLColour {
	GLubyte r;
	GLubyte g;
	GLubyte b;
	GLubyte a;

	GLColour() { r = 255; g = 255; b = 255; a = 255; }
	GLColour(GLubyte _r, GLubyte _g, GLubyte _b, GLubyte _a) { Set(_r, _g, _b, _a); }
	GLColour(GLubyte _c, GLubyte _a) { Set(_c, _c, _c, _a); }
	GLColour(GLubyte _c) { Set(_c, _c, _c, 255); }
	GLColour(const GLColour& o) { Set(o); }
	void Set(GLubyte _r, GLubyte _g, GLubyte _b, GLubyte _a) { r = _r; g = _g; b = _b; a = _a; }
	void Set(const GLColour& o) { Set(o.r, o.g, o.b, o.a); }
	void Intensity(GLfloat i)
	{
		// intensity range is -1.0 to 1.0, will limit to 255 and 0
		++i;
		r = (GLubyte)(max(min(r * i, 255.f), 0.f));
		g = (GLubyte)(max(min(g * i, 255.f), 0.f));
		b = (GLubyte)(max(min(b * i, 255.f), 0.f));
		//Limit(r, (GLubyte)0, (GLubyte)255);
		//Limit(g, (GLubyte)0, (GLubyte)255);
		//Limit(b, (GLubyte)0, (GLubyte)255);
	}

	void operator=(const GLColour& o) { Set(o); }
	void operator*=(GLfloat v) { r = (GLubyte)(r * v); g = (GLubyte)(g * v); b = (GLubyte)(b * v); }
	GLColour operator+(const GLColour& o) { return GLColour(r + o.r, g + o.g, b + o.g, a + o.a); }
};

struct GLNormal {
	GLint x : 10;
	GLint y : 10;
	GLint z : 10;
	GLint zero : 2;

	GLNormal() { x = 1; y = 1; z = 1; zero = 1; }
	GLNormal(float _x, float _y, float _z)
	{
		x = (GLint)(_x * 511.f);
		y = (GLint)(_y * 511.f);
		z = (GLint)(_z * 511.f);
		zero = 1;
	}
	GLNormal(const vector3df& in)
	{
		x = (GLint)(in.x * 511.f);
		y = (GLint)(in.y * 511.f);
		z = (GLint)(in.z * 511.f);
		zero = 1;
	}
	void GetValues(vector3df& out) const
	{
		out.x = (GLfloat)x / 511.0f;
		out.y = (GLfloat)y / 511.0f;
		out.z = (GLfloat)z / 511.0f;
	}
	vector3df ToVector()
	{
		return vector3df((GLfloat)x / 511.f, (GLfloat)y / 511.f, (GLfloat)z / 511.f);
	}
	void FromVector(const vector3df& in)
	{
		x = (GLint)(in.x * 511.f);
		y = (GLint)(in.y * 511.f);
		z = (GLint)(in.z * 511.f);
		zero = 1;
	}
	bool operator==(const GLNormal& o)
	{
		return (abs(x - o.x) <= FLT_MIN && abs(y - o.y) <= FLT_MIN && abs(z - o.z) <= FLT_MIN);
	}
	void operator=(const GLNormal& o) { x = o.x; y = o.y; z = o.z; }
	GLNormal& operator-() { x = -x; y = -y; z = -z; return *this; }
};

struct GLTexCoord {
	GLshort u;
	GLshort v;

	GLTexCoord() { u = 0; v = 0; }
	GLTexCoord(const vector2df& other) { u = static_cast<GLshort>(other.x * 8192); v = static_cast<GLshort>(other.y * 8192); }
	void operator=(const GLTexCoord& other) { u = other.u; v = other.v; }
	vector2df ToVector() const { return vector2df((GLfloat)u / 8192.f, (GLfloat)v / 8192.f); }
};

struct GLVertex {
	vector3df	pos;
	GLColour	col;
	GLTexCoord	uv;
	GLNormal	n;
	GLColour	light;
	GLNormal	t;

	GLVertex(vector3df _pos, GLColour _col, GLTexCoord _uv, GLNormal _n, GLColour l = GLColour(), GLNormal _t = GLNormal(0, 1, 0)) { pos = _pos; col = _col; uv = _uv; n = _n; light = l; t = _t; }
	GLVertex(const GLVertex& other) { pos = other.pos; col = other.col; uv = other.uv; n = other.n; light = other.light; t = other.t; }
};

struct GLBoneVertex {
	vector3df	pos;
	GLColour	col;
	GLTexCoord	uv;
	GLNormal	n;
	GLColour	light;
	GLNormal	t;
	vector4di	boneIDs;
	vector4df	boneWeights;

	GLBoneVertex(vector3df _pos, GLColour _col, GLTexCoord _uv, GLNormal _n, GLColour l = GLColour(), GLNormal _t = GLNormal(0, 1, 0), vector4di bIDs = vector4di(-1), vector4df bWeights = vector4df()) { pos = _pos; col = _col; uv = _uv; n = _n; light = l; t = _t; boneIDs = bIDs; boneWeights = bWeights; }
	GLBoneVertex(const GLBoneVertex& other) { pos = other.pos; col = other.col; uv = other.uv; n = other.n; light = other.light; t = other.t; boneIDs = other.boneIDs; boneWeights = other.boneWeights; }
};

struct Bone {
	//quaternion	rot;
	vector3df	rot;
	vector3df	pos;
	vector3df	bind;
	GLint		parent;
	matrix4f	transform;

	Bone() : rot(), pos(), bind(), parent(-1), transform() {}

	void SortTransform(std::vector<Bone>& bones)
	{
		// set identity, translate by bind position
		transform.Identity();
		//transform.Translate(-bind);

		// create a rotation matrix from the quaternion and rotate the transform
		//matrix4f m;
		//m.SetFromQuaternion(rot);
		//transform = m.Multiply(transform);
		transform.SetRotation(rot, bind);

		// translate backwards by bind position
		transform.Translate(pos);

		// if we have a parent, use their matrix to transform ours
		if (parent > -1)
			transform = bones[parent].transform.Multiply(transform);
	}
};
#pragma pack()

static GLNormal CalculateNormal(const triangle3df& tri)
{
	// get u and v
	vector3df u = tri.p2 - tri.p1;
	vector3df v = tri.p3 - tri.p1;

	// calculate normal
	vector3df n;
	n.x = (u.y * v.z) - (u.z * v.y);
	n.y = (u.z * v.x) - (u.x * v.z);
	n.z = (u.x * v.y) - (u.y * v.z);

	// return the normal
	return GLNormal(n);
}

#endif
