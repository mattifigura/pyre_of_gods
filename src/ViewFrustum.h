#ifndef _VIEW_FRUSTUM_H_
#define _VIEW_FRUSTUM_H_

#include "Misc.h"
#include "Plane.h"

class ViewFrustum {
	public:
		enum {
			far_plane = 0,
			near_plane,
			left_plane,
			right_plane,
			bottom_plane,
			top_plane,

			plane_count
		};

		ViewFrustum();

		void Set(matrix4f& matrix);

		vector3df FarLeftUp();
		vector3df FarLeftDown();
		vector3df FarRightUp();
		vector3df FarRightDown();

		bool PointInFrustum(const vector3df& p) const;
		bool QuickPointInFrustum(const vector3df& p, GLfloat radius) const;

	protected:

	private:
		plane3df m_Planes[plane_count];
};

#endif