#ifndef _ANIMATEDOBJECT_H_
#define _ANIMATEDOBJECT_H_

#include "GLObject.h"

class AnimatedObject : public GLObject {
	public:
		AnimatedObject(Manager* parent, vector3df pos, vector3di size);
		virtual ~AnimatedObject();
		
		virtual void Update(bool bForce = false);
		
		void SetAnimationSpeed(float speed);
		float GetAnimationSpeed();
		
		int AddAnimation(vector3di start, vector3di end);
		
		void SetAnimation(int anim);
		int GetAnimation();
		
		void SetCurrentFrame(float frame);
		
	protected:
	
	private:
		float				m_fFrameWidth;
		float				m_fFrameHeight;
		float				m_fAnimSpeed;
		int					m_iXFrames;
		float				m_fCurrentFrame;
		int					m_iCurrentAnim;
		
		std::vector<vector<vector2df> >	m_Animations;
};

#endif
