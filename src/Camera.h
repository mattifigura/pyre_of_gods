#ifndef _CAMERA_H_
#define _CAMERA_H_

#include "Misc.h"
#include "ViewFrustum.h"

class SceneManager;

class Camera {
	public:
		Camera(SceneManager* parent);
		~Camera();

		void LookAt(const vector3df& pos);
		void Translate(const vector3df& pos);
		void SetPosition(const vector3df& pos);
		void SetRotation(const vector3df& rot);

		const vector3df& GetPosition();

		void Update();
		const GLfloat* GetCameraMatrix();
		matrix4f& GetMatrix();

		const ViewFrustum& GetViewFrustum();

		const GLfloat& GetDirection();
		void SetDirection(GLfloat dir);

		const GLfloat& GetDirectionCos();
		const GLfloat& GetDirectionSin();

		vector3df GetTarget() const;

	protected:

	private:
		SceneManager*	m_pSceneMan;
		matrix4f		m_Matrix;
		matrix4f		m_RetMatrix;
		vector3df		m_Position;
		vector3df		m_Target;
		ViewFrustum		m_Frustum;
		GLfloat			m_fDirection;
		GLfloat			m_fDirCos;
		GLfloat			m_fDirSin;
		vector3df		m_Rotation;
};

#endif