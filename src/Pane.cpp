#include "Pane.h"
#include "GUIManager.h"

Pane::Pane(const char* text, Font* font, Manager* parent, GUIElement* guiParent, vector3df pos, vector3di size, GLubyte alpha, bool movable)
	: GUIElement(parent, guiParent, pos, size)
	, m_bMovable(movable)
{
	// create a new movable window pane
	AddQuad(vector2df(), vector2df(0.5, 0.5), vector2df((GLfloat)size.x, (GLfloat)size.y), vector2df(0.5, 0.5), GLColour(0, alpha));
	
	AddQuad(vector2df(), vector2df(0.5, 0.5), vector2df((GLfloat)size.x, 1), vector2df(0.5, 0.5), GLColour(51, 255));
	AddQuad(vector2df(0, (GLfloat)size.y - 1), vector2df(0.5, 0.5), vector2df((GLfloat)size.x, (GLfloat)size.y), vector2df(0.5, 0.5), GLColour(51, 255));
	AddQuad(vector2df(0, 1), vector2df(0.5, 0.5), vector2df(1, (GLfloat)size.y - 1), vector2df(0.5, 0.5), GLColour(51, 255));
	AddQuad(vector2df((GLfloat)size.x - 1, 1), vector2df(0.5, 0.5), vector2df((GLfloat)size.x, (GLfloat)size.y - 1), vector2df(0.5, 0.5), GLColour(51, 255));

	AddQuad(vector2df(2, 2), vector2df(0.5, 0.5), vector2df((GLfloat)size.x - 2, (GLfloat)size.y - 2), vector2df(0.5, 0.5), GLColour(0, 150));
	
	// add a shadow
	//AddQuad(vector2df(0, -16), vector2df(31.0/64.0, 24.0/64.0), vector2df((GLfloat)size.x, 0), vector2df(33.0/64.0, 19.0/64.0));
	//AddQuad(vector2df(0, (GLfloat)size.y), vector2df(31.0/64.0, 19.0/64.0), vector2df((GLfloat)size.x, (GLfloat)size.y + 16), vector2df(31.0/64.0, 24.0/64.0));
	//AddQuad(vector2df(-16, 0), vector2df(19.0/64.0, 31.0/64.0), vector2df(0, (GLfloat)size.y), vector2df(23.0/64.0, 31.0/64.0));
	//AddQuad(vector2df((GLfloat)size.x, 0), vector2df(23.0/64.0, 31.0/64.0), vector2df((GLfloat)size.x + 16, (GLfloat)size.y), vector2df(19.0/64.0, 31.0/64.0));
	GLfloat sh = 8;
	AddQuad(GLVertex(vector3df(0, -sh, -20), GLColour(0, 0), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df((GLfloat)size.x, -sh, -20), GLColour(0, 0), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df((GLfloat)size.x, 0, -20), GLColour(0, 50), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(0, 0, -20), GLColour(0, 50), vector2df(0.5, 0.5), GLNormal()));
	AddQuad(GLVertex(vector3df(0, (GLfloat)size.y, -20), GLColour(0, 50), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df((GLfloat)size.x, (GLfloat)size.y, -20), GLColour(0, 50), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df((GLfloat)size.x, (GLfloat)size.y + sh, -20), GLColour(0, 0), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(0, (GLfloat)size.y + sh, -20), GLColour(0, 0), vector2df(0.5, 0.5), GLNormal()));
	AddQuad(GLVertex(vector3df(-sh, 0, -20), GLColour(0, 0), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(0, 0, -20), GLColour(0, 50), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(0, (GLfloat)size.y, -20), GLColour(0, 50), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(-sh, (GLfloat)size.y, -20), GLColour(0, 0), vector2df(0.5, 0.5), GLNormal()));
	AddQuad(GLVertex(vector3df((GLfloat)size.x, 0, -20), GLColour(0, 50), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df((GLfloat)size.x + sh, 0, -20), GLColour(0, 0), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df((GLfloat)size.x + sh, (GLfloat)size.y, -20), GLColour(0, 0), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df((GLfloat)size.x, (GLfloat)size.y, -20), GLColour(0, 50), vector2df(0.5, 0.5), GLNormal()));

	AddQuad(GLVertex(vector3df(-sh, -sh, -20), GLColour(0, 0), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(0, -sh, -20), GLColour(0, 0), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(0, 0, -20), GLColour(0, 50), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(-sh, 0, -20), GLColour(0, 0), vector2df(0.5, 0.5), GLNormal()));
	AddQuad(GLVertex(vector3df((GLfloat)size.x, -sh, -20), GLColour(0, 0), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df((GLfloat)size.x + sh, -sh, -20), GLColour(0, 0), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df((GLfloat)size.x + sh, 0, -20), GLColour(0, 0), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df((GLfloat)size.x, 0, -20), GLColour(0, 50), vector2df(0.5, 0.5), GLNormal()), true);
	AddQuad(GLVertex(vector3df(-sh, (GLfloat)size.y, -20), GLColour(0, 0), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(0, (GLfloat)size.y, -20), GLColour(0, 50), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(0, (GLfloat)size.y + sh, -20), GLColour(0, 0), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(-sh, (GLfloat)size.y + sh, -20), GLColour(0, 0), vector2df(0.5, 0.5), GLNormal()), true);
	AddQuad(GLVertex(vector3df((GLfloat)size.x, (GLfloat)size.y, -20), GLColour(0, 50), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df((GLfloat)size.x + sh, (GLfloat)size.y, -20), GLColour(0, 0), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df((GLfloat)size.x + sh, (GLfloat)size.y + sh, -20), GLColour(0, 0), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df((GLfloat)size.x, (GLfloat)size.y + sh, -20), GLColour(0, 0), vector2df(0.5, 0.5), GLNormal()));
	CreateBufferObjects();
	
	// add the text on this pane
	Text* t = GetGUIManager()->CreateText(text, vector3df(), vector2di(size.x, 0), font, this);
	t->SetOffset(vector3df(0, 6, 0));
	t->CentreText(true);
}

void Pane::SetMovable(bool movable)
{
	m_bMovable = movable;
}

bool Pane::IsMovable()
{
	return m_bMovable;
}

string Pane::GetName()
{
	Text* t = dynamic_cast<Text*>(GetChild(0));
	if (t)
		return t->GetText();

	return "";
}

void Pane::Update(bool bForce)
{
	// if we aren't visible then don't update anything
	if (!bForce && !IsVisible())
		return;
	
	// pane moving
	if (m_bMovable && IsPressed(left_mouse))
	{
		vector3dd mouse = GetGUIManager()->GetMousePosition();
		vector3dd diff = mouse - m_PrevMousePos;
		
		TranslatePosition(vector2df((GLfloat)diff.x, (GLfloat)diff.y));
		
		m_PrevMousePos = mouse;
	}
	
	// call GUIElement::Update()
	GUIElement::Update(bForce);
}

void Pane::MouseDownFunction(GLuint button)
{
	// start dragging this window around
	m_PrevMousePos = GetGUIManager()->GetMousePosition();
}
