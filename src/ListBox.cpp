#include "ListBox.h"
#include "GUIManager.h"
#include <sstream>

ListBox::ListBox(Manager* parent, GUIElement* guiParent, vector3df pos, vector3di size)
	: GUIElement(parent, guiParent, pos, size)
	, m_Selected(0)
	, m_ItemCount(0)
	, m_ItemsString("")
	, m_ListHeight(0)
	, m_Scrollbar(0)
{
	// add the quads
	AddQuad(vector2df(1, 0), vector2df(0.5, 0.5), vector2df((GLfloat)size.x - 1, (GLfloat)size.y), vector2df(0.5, 0.5), GLColour(0, 255));
	AddQuad(vector2df(1, 0), vector2df(0.5, 0.5), vector2df((GLfloat)size.x - 1, (GLfloat)size.y), vector2df(0.5, 0.5), GLColour(0, 255));
	//AddQuad(vector2df(), vector2df(0.5, 0.5), vector2df(), vector2df(0.5, 0.5), GLColour(255, 255, 255, 30));
	AddQuad(GLVertex(vector3df(0, 0, -20), GLColour(180, 70), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(0, 0, -20), GLColour(180, 70), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(0, 0, -20), GLColour(230, 70), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(0, 0, -20), GLColour(230, 70), vector2df(0.5, 0.5), GLNormal()));
	CreateBufferObjects();

	// add a scrollbar
	m_Scrollbar = GetGUIManager()->CreateScrollbar(this, pos, size, vertical_scroll, guiParent);
	
	// set the render boundary
	SetRenderBoundary(vector4df(0, 0, (GLfloat)size.x, (GLfloat)size.y));

	// set the default function
	SetFunction(left_mouse, ListBox::DefaultFunction);

	// add the text
	m_Items = GetGUIManager()->CreateText("", vector2df(5, 4), vector2di(size.x, 1), NULL, this);
	m_ItemSpacing = m_Items->GetFont()->GetDetails().z * m_Items->GetLineSpacing();

	// select the first item
	Select(0);
}

bool ListBox::IsHovered()
{
	if (!IsVisible())
	{
		return false;
	}

	if (!GetGUIManager())
	{
		return false;
	}

	vector4df b = m_RenderBoundary;
	return IsMouseInRect(GetGUIManager()->GetMousePosition(), vector2df(b.x, b.y), vector2df(b.z, b.w));
}

void ListBox::MouseDownFunction(GLuint button)
{
	// do nothing
}

void ListBox::AddItem(string text, bool remake)
{
	//GetGUIManager()->CreateText(text.c_str(), vector2df(5, 6) + vector2df(0, (GLfloat)GetChildren().size() * 24), NULL, this);
	m_Items->AppendText(text.append("\n"), remake);
	m_ItemsString.append(text);
	m_Scrollbar->AddHeight(m_ItemSpacing);
	++m_ItemCount;
	m_ListHeight = m_Scrollbar->GetHeight();

	if (m_ItemCount * m_ItemSpacing > (m_RenderBoundary.w - m_RenderBoundary.y))
	{
		ChangeQuadCoords(0, vector2df(2, 0), vector2df((GLfloat)GetSize().x - 2, (GLfloat)(m_ItemCount + 1) * m_ItemSpacing + 2));
		ChangeQuadCoords(1, vector2df(2, 0), vector2df((GLfloat)GetSize().x - 2, (GLfloat)(m_ItemCount + 1) * m_ItemSpacing + 2));
	}
}

void ListBox::ChangeItem(GLuint pos, string text)
{
	// change the given row in the text
	string values, line;
	GLuint i = 0;
	std::istringstream iss(m_Items->GetText());
	while (std::getline(iss, line, '\n'))
	{
		if (i == pos)
		{
			values.append(text.append("\n"));
		}
		else
			values.append(line.append("\n"));

		++i;
	}
	m_Items->SetText(values, true);
	m_ItemsString = values;
}

void ListBox::RemoveItem(GLuint pos)
{
	// remove the given row in the text
	string values, line;
	GLuint i = 0;
	std::istringstream iss(m_Items->GetText());
	while (std::getline(iss, line, '\n'))
	{
		if (i != pos)
			values.append(line.append("\n"));

		++i;
	}
	m_Items->SetText(values, true);
	m_ItemsString = values;
	m_ItemCount--;
}

string ListBox::GetSelectedText()
{
	return GetItem(m_Selected);
}

GLuint ListBox::GetSelected()
{
	return m_Selected;
}

string ListBox::GetItem(GLuint pos)
{
	// get the text at the given position
	string ret = "";
	GLuint i = 0;
	std::istringstream iss(m_Items->GetText());
	while (std::getline(iss, ret, '\n'))
	{
		if (i == pos)
			break;
		else
			++i;
	}

	return ret;
}

void ListBox::ClearList()
{
	//KillChildren();
	m_Items->ClearText();
	m_Scrollbar->ScrollToTop();
	m_Scrollbar->ResetHeight();
	m_ItemCount = 0;
	m_ItemsString = "";
	Select(0);
}

void ListBox::SetDepth(GLfloat depth)
{
	GUIElement::SetDepth(depth);
	m_Scrollbar->SetDepth(depth + 1);
}

void ListBox::SetVisible(bool visible)
{
	GLObject::SetVisible(visible);
	m_Scrollbar->SetVisible(visible);
}

void ListBox::SetBackgroundAlpha(GLubyte alpha)
{
	ChangeQuadColour(0, GLColour(0, alpha));
	ChangeQuadColour(1, GLColour(0, alpha));
}

void ListBox::Search(string s)
{
	// search for the given string in the items
	string str = "";
	string fin = "";
	string src = ToLower(s);
	m_ItemCount = 0;
	std::istringstream iss(m_ItemsString);
	m_Scrollbar->ScrollToTop();
	m_Scrollbar->ResetHeight();
	m_Items->ClearText(false);
	while (std::getline(iss, str, '\n'))
	{
		if (ToLower(str).find(src) != string::npos)
		{
			fin.append(str.append("\n"));
			m_Scrollbar->AddHeight(m_ItemSpacing);
			++m_ItemCount;
		}
	}
	if (fin == "")
		fin = " ";
	m_Items->SetText(fin);
	Select(0);
}

void ListBox::ClearSearch()
{
	m_Items->SetText(m_ItemsString);
	m_Scrollbar->ScrollToTop();
	m_Scrollbar->ResetHeight();
	m_Scrollbar->AddHeight(m_ListHeight);
	m_ItemCount = (GLuint)(m_ListHeight / m_ItemSpacing);
	Select(0);
}

void ListBox::Create()
{
	m_Items->CreateCharacterBuffers(true);
}

void ListBox::Select(GLuint i)
{
	m_Selected = i;
	ChangeQuadCoords(2, vector2df(2, (GLfloat)m_Selected * m_ItemSpacing + 2), vector2df((GLfloat)GetSize().x - 2, (GLfloat)(m_Selected + 1) * m_ItemSpacing - 2));
}

bool ListBox::ItemExists(string item)
{
	string str = "";
	string src = ToLower(item);
	std::istringstream iss(m_ItemsString);
	while (std::getline(iss, str, '\n'))
	{
		if (ToLower(str).find(src) != string::npos)
		{
			return true;
		}
	}

	return false;
}

GLuint ListBox::GetFirstItem(string item)
{
	string str = "";
	string src = ToLower(item);
	std::istringstream iss(m_ItemsString);
	GLuint count = 0;
	while (std::getline(iss, str, '\n'))
	{
		if (ToLower(str).find(src) != string::npos)
		{
			return count;
		}

		++count;
	}

	return 0;
}

void ListBox::SetRenderBoundary(vector4df boundary)
{
	if (!m_Scrollbar)
	{
		GUIElement::SetRenderBoundary(boundary);
		return;
	}
	
	vector2df scroll = m_Scrollbar->GetScrollAmount();
	vector2df pos = GetAbsolutePosition();
	m_RenderBoundary.Set(scroll.x + pos.x + boundary.x, scroll.y + pos.y + boundary.y, scroll.x + pos.x + boundary.z, scroll.y + pos.y + boundary.w);
	m_OriginalRenderBoundary = boundary;
}

void ListBox::DefaultFunction(GUIElement* e)
{
	ListBox* lb = dynamic_cast<ListBox*>(e);

	if (lb)
	{
		vector2dd mousePos = lb->GetGUIManager()->GetMousePosition();
		vector2df pos = lb->GetAbsolutePosition();
		for (GLuint i = 0; i < lb->m_ItemCount; ++i) {
			if (mousePos.y >= pos.y + i * lb->m_ItemSpacing && mousePos.y < pos.y + (i + 1) * lb->m_ItemSpacing)
			{
				lb->Select(i);
				break;
			}
		}
	}
}
