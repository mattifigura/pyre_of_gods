#include "Label.h"
#include "GUIManager.h"

Label::Label(Manager* parent, string title, string text, GUIElement* e)
	: GUIElement(parent, NULL, vector2df(), vector2di())
	, m_Element(e)
{
	// calculate the size of this label
	vector2df size(360/*(GLfloat)GetGUIManager()->GetFont("media/font20b")->GetStringWidth(title) + 40*/, 40);

	// add the title to this label
	Text* t = GetGUIManager()->CreateText(title.c_str(), vector3df(), vector2di(size.x, 0), GetGUIManager()->GetFont("font20b"), this);
	t->SetOffset(vector3df(0, 6, 0));
	t->CentreText(true);

	// add the description
	t = GetGUIManager()->CreateText(text.c_str(), vector3df(10, 40, 0), vector2di(size.x - 20, 0), GetGUIManager()->GetFont("font14b"), this);
	t->SetLineSpacing(1.05f);
	t->SplitByWord(true);
	size.y += t->GetHeight() + 8.f;

	// animate
	vector2di _s = e->GetSize();
	vector2di ScreenSize = parent->GetRoot()->GetScreenSize();
	SetPosition(vector2df(e->GetPosition()) + vector2df(0, _s.y * 0.5f));
	vector2df pos = vector2df(e->GetPosition()) + vector2df(-size.x - 30, floor((_s.y - size.y) * 0.5f));

	if (pos.y + size.y >= ScreenSize.y - 10.f)
		pos.y -= (pos.y + size.y - ScreenSize.y + 10.f);
	if (pos.x < 10.f)
		pos.x = 10.f;

	MoveTo(pos);
	SetScale(vector3df());
	ScaleTo(vector2df(1));
	
	// create a new label
	AddQuad(vector2df(), vector2df(0.5, 0.5), vector2df(size.x, size.y), vector2df(0.5, 0.5), GLColour(0, 255));

	AddQuad(vector2df(), vector2df(0.5, 0.5), vector2df(size.x, 1), vector2df(0.5, 0.5), GLColour(51, 255));
	AddQuad(vector2df(0, size.y - 1), vector2df(0.5, 0.5), vector2df(size.x, size.y), vector2df(0.5, 0.5), GLColour(51, 255));
	AddQuad(vector2df(0, 1), vector2df(0.5, 0.5), vector2df(1, size.y - 1), vector2df(0.5, 0.5), GLColour(51, 255));
	AddQuad(vector2df(size.x - 1, 1), vector2df(0.5, 0.5), vector2df(size.x, size.y - 1), vector2df(0.5, 0.5), GLColour(51, 255));

	AddQuad(vector2df(2, 2), vector2df(0.5, 0.5), vector2df(size.x - 2, size.y - 2), vector2df(0.5, 0.5), GLColour(0, 150));

	// add a shadow
	GLfloat sh = 8;
	AddQuad(GLVertex(vector3df(0, -sh, -20), GLColour(0, 0), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(size.x, -sh, -20), GLColour(0, 0), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(size.x, 0, -20), GLColour(0, 50), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(0, 0, -20), GLColour(0, 50), vector2df(0.5, 0.5), GLNormal()));
	AddQuad(GLVertex(vector3df(0, size.y, -20), GLColour(0, 50), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(size.x, size.y, -20), GLColour(0, 50), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(size.x, size.y + sh, -20), GLColour(0, 0), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(0, size.y + sh, -20), GLColour(0, 0), vector2df(0.5, 0.5), GLNormal()));
	AddQuad(GLVertex(vector3df(-sh, 0, -20), GLColour(0, 0), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(0, 0, -20), GLColour(0, 50), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(0, size.y, -20), GLColour(0, 50), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(-sh, size.y, -20), GLColour(0, 0), vector2df(0.5, 0.5), GLNormal()));
	AddQuad(GLVertex(vector3df(size.x, 0, -20), GLColour(0, 50), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(size.x + sh, 0, -20), GLColour(0, 0), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(size.x + sh, size.y, -20), GLColour(0, 0), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(size.x, size.y, -20), GLColour(0, 50), vector2df(0.5, 0.5), GLNormal()));

	AddQuad(GLVertex(vector3df(-sh, -sh, -20), GLColour(0, 0), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(0, -sh, -20), GLColour(0, 0), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(0, 0, -20), GLColour(0, 50), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(-sh, 0, -20), GLColour(0, 0), vector2df(0.5, 0.5), GLNormal()));
	AddQuad(GLVertex(vector3df(size.x, -sh, -20), GLColour(0, 0), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(size.x + sh, -sh, -20), GLColour(0, 0), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(size.x + sh, 0, -20), GLColour(0, 0), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(size.x, 0, -20), GLColour(0, 50), vector2df(0.5, 0.5), GLNormal()), true);
	AddQuad(GLVertex(vector3df(-sh, size.y, -20), GLColour(0, 0), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(0, size.y, -20), GLColour(0, 50), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(0, size.y + sh, -20), GLColour(0, 0), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(-sh, size.y + sh, -20), GLColour(0, 0), vector2df(0.5, 0.5), GLNormal()), true);
	AddQuad(GLVertex(vector3df(size.x, size.y, -20), GLColour(0, 50), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(size.x + sh, size.y, -20), GLColour(0, 0), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(size.x + sh, size.y + sh, -20), GLColour(0, 0), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(size.x, size.y + sh, -20), GLColour(0, 0), vector2df(0.5, 0.5), GLNormal()));
	CreateBufferObjects();

	// attach to the element
	m_Element->SetLabel(this);
}

void Label::SetElement(GUIElement* e)
{
	m_Element = e;
}

void Label::MouseDownFunction(GLuint button)
{

}

void Label::Update(bool bForce)
{
	// if we stop hovering over the GUIElement we're attached to, delete this label
	if (!m_Element || !m_Element->IsHovered())
	{
		GetGUIManager()->KillElement(this);
		if (m_Element)
			m_Element->SetLabel(NULL);
		return;
	}

	// update this widget
	GUIElement::Update(bForce);
}