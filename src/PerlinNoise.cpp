#include "PerlinNoise.h"
//#include <stdlib.h>
//#include <math.h>
//#include <iostream>
#include <numeric>
#include <random>
#include <algorithm>

PerlinNoise::PerlinNoise(unsigned int seed)
{
	SetSeed(seed);
}

void PerlinNoise::SetSeed(unsigned int seed)
{
	m_Permutations.resize(256);

	// fill p with values from 0 to 255
	std::iota(m_Permutations.begin(), m_Permutations.end(), 0);

	// initialize a random engine with seed
	std::default_random_engine engine(seed);

	// shuffle using the above random engine
	std::shuffle(m_Permutations.begin(), m_Permutations.end(), engine);

	// duplicate the permutation vector
	m_Permutations.insert(m_Permutations.end(), m_Permutations.begin(), m_Permutations.end());
}

double PerlinNoise::GetNoise(double x, double y, double z)
{
	// find the unit cube that contains the point
	int X = (int)floor(x) & 255;
	int Y = (int)floor(y) & 255;
	int Z = (int)floor(z) & 255;

	// find relative x, y, z of point in cube
	x -= floor(x);
	y -= floor(y);
	z -= floor(z);

	// compute fade curves for each of x, y, z
	double u = _Fade(x);
	double v = _Fade(y);
	double w = _Fade(z);

	// hash coordinates of the 8 cube corners
	int A = m_Permutations[X] + Y;
	int AA = m_Permutations[A] + Z;
	int AB = m_Permutations[A + 1] + Z;
	int B = m_Permutations[X + 1] + Y;
	int BA = m_Permutations[B] + Z;
	int BB = m_Permutations[B + 1] + Z;

	// add blended results from 8 corners of cube
	double res = _Lerp(w, _Lerp(v, _Lerp(u, _Grad(m_Permutations[AA], x, y, z), _Grad(m_Permutations[BA], x - 1, y, z)),
		_Lerp(u, _Grad(m_Permutations[AB], x, y - 1, z), _Grad(m_Permutations[BB], x - 1, y - 1, z))),
		_Lerp(v, _Lerp(u, _Grad(m_Permutations[AA + 1], x, y, z - 1), _Grad(m_Permutations[BA + 1], x - 1, y, z - 1)),
		_Lerp(u, _Grad(m_Permutations[AB + 1], x, y - 1, z - 1), _Grad(m_Permutations[BB + 1], x - 1, y - 1, z - 1))));
	return (res + 1.0) / 2.0;
}

double PerlinNoise::_Fade(double t)
{
	return (t * t * t * (t * (t * 6 - 15) + 10));
}

double PerlinNoise::_Lerp(double t, double a, double b)
{
	return (a + t * (b - a));
}

double PerlinNoise::_Grad(int hash, double x, double y, double z)
{
	int h = hash & 15;
	double u = (h < 8) ? x : y;
	double v = (h < 4) ? y : ((h == 12 || h == 14) ? x : z);
	return ((h & 1) == 0 ? u : -u) + ((h & 2) == 0 ? v : -v);
}

// OLD NON-WORKING CODE FOLLOWS, KEPT HERE BECAUSE SOME OF IT LOOKS PRETTY =D \\

/*void PerlinNoise::SetSeed(unsigned int seed)
{
	// generate some prime numbers to use in the _Noise() function
	srand(seed);

	// is_prime is a lambda function to let us know whether the supplied number is prime or not
	auto is_prime = [](unsigned int n) -> bool {
		if (n >= 1 && n <= 3)
			return true;
		else if (n % 2 == 0 || n % 3 == 0)
			return false;
		
		unsigned int i = 5;
		double s = sqrt(n);
		while (i * i <= s)
		{
			if (n % i == 0 || n % (i + 2) == 0)
				return false;

			i += 6;
		}

		return true;
	};

	// generate_prime is a lambda to generate a prime number between the given range
	auto generate_prime = [&](unsigned int& n, unsigned int lower, unsigned int range) {
		n = 4;
		while (!is_prime(n))
		{
			n = lower + rand() % range;
		}
	};

	// now to generate the random numbers used in each _Noise() function
	unsigned int i = 0;
	unsigned int n = 1;
	while (i < 32)
	{
		// first prime is between 15,000 - 18,000
		generate_prime(n, 15000, 3000);
		Primes[i] = n;

		// second prime is between 700,000 - 800,000
		generate_prime(n, 700000, 100000);
		Primes[i + 1] = n;

		// third prime is between 1,300,000,000 - 1,400,000,000
		generate_prime(n, 1300000000, 100000000);
		Primes[i + 2] = n;
		
		// fourth prime is between 1,200,000,000 - 1,300,000,000
		generate_prime(n, 1200000000, 100000000);
		Primes[i + 3] = n;

		// increment i
		i += 4;
	}
}

float PerlinNoise::GetNoise(float x, float y, float p, unsigned int o)
{
	// get the perlin noise at the given coordinates
	float noise = 0;

	for (unsigned int i = 0; i < o; i++) {
		float f = (float)pow(2, i);
		float a = (float)pow(p, i);

		noise += _InterpolatedNoise(x * f, y * f, i) * a;
	}

	return noise;
}

float PerlinNoise::_Noise(float x, float y, unsigned int n)
{
	// generate some random noise
	long i = (int)(x + y) * 57;
	i = pow((i << 13), i);

	unsigned int f = (n / 4);
	//std::cout << (1.0 - ((i * (i * i * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0) << std::endl;
	//return (1.0f - ((i * (i * i * Primes[f] + Primes[f + 1]) + Primes[f + 2]) & 0x7fffffff) / Primes[f + 3]);
	return (1.0 - ((i * (i * i * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0);
}

float PerlinNoise::_SmoothedNoise(float x, float y, unsigned int n)
{
	float corners = (_Noise(x - 1, y - 1, n) + _Noise(x + 1, y - 1, n) + _Noise(x - 1, y + 1, n) + _Noise(x + 1, y + 1, n)) / 16.0f;
	float sides = (_Noise(x - 1, y, n) + _Noise(x + 1, y, n) + _Noise(x, y - 1, n) + _Noise(x, y + 1, n)) / 8.0f;
	float center = _Noise(x, y, n) / 4.0f;

	return (corners + sides + center);
}

float PerlinNoise::_InterpolatedNoise(float x, float y, unsigned int n)
{
	// interpolate function
	auto interpolate = [](float x, float y, float n) -> float {
		return (x * (1 - n) + (y * n));
	};

	// interpolate the smoothed noise
	int X = (int)x;
	float fX = x - X;

	int Y = (int)y;
	float fY = y - Y;

	float v1 = _SmoothedNoise((float)X, (float)Y, n);
	float v2 = _SmoothedNoise((float)X + 1, (float)Y, n);
	float v3 = _SmoothedNoise((float)X, (float)Y + 1, n);
	float v4 = _SmoothedNoise((float)X + 1, (float)Y + 1, n);

	float i1 = interpolate(v1, v2, fX);
	float i2 = interpolate(v3, v4, fX);

	return interpolate(i1, i2, fY);
}*/