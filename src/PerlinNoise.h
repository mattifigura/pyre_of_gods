#ifndef _PERLIN_NOISE_H_
#define _PERLIN_NOISE_H_

#include <vector>

class PerlinNoise {
public:
	PerlinNoise(unsigned int seed);

	void SetSeed(unsigned int seed);
	double GetNoise(double x, double y, double z);

protected:

private:
	//float _Noise(float x, float y, unsigned int n);
	//float _SmoothedNoise(float x, float y, unsigned int n);
	//float _InterpolatedNoise(float x, float y, unsigned int n);

	//unsigned int Primes[32];
	double _Fade(double t);
	double _Lerp(double t, double a, double b);
	double _Grad(int hash, double x, double y, double z);

	std::vector<int> m_Permutations;
};

#endif