#include "XMLReader.h"
#include <cstring>

XMLReader::XMLReader(string file)
	: m_XMLFile(file, std::ios::in)
{
	
}

XMLReader::~XMLReader()
{
	if (m_XMLFile.is_open())
		m_XMLFile.close();
}

bool XMLReader::IsOpen()
{
	return m_XMLFile.is_open();
}

bool XMLReader::ReadLine()
{
	// if the file isn't even open return false
	if (!m_XMLFile.is_open())
		return false;

	// read a line of the XML file
	if (getline(m_XMLFile, m_strCurrentLine))
	{
		return true;
	}
	else
	{
		m_XMLFile.close();
		return false;
	}
}

bool XMLReader::IsNode(string node)
{
	// check if we're starting the given node
	unsigned int i = m_strCurrentLine.find('<');
	if (i < m_strCurrentLine.length())
	{
		if (strcmp(node.c_str(), m_strCurrentLine.substr(i + 1, node.length()).c_str()) == 0)
		{
			if (m_strCurrentLine[i + node.length() + 1] == '>')
				return true;
		}
	}

	return false;
}

bool XMLReader::IsEndOfNode(string node)
{
	// check if we're ending the given node
	unsigned int i = m_strCurrentLine.find("</");
	if (i < m_strCurrentLine.length())
	{
		return (strcmp(node.c_str(), m_strCurrentLine.substr(i + 2, node.length()).c_str()) == 0);
	}

	return false;
}

string XMLReader::GetNodeName()
{
	// returns the name of the node on the current line (if any)
	unsigned int i = m_strCurrentLine.find('<');
	if (i < m_strCurrentLine.length())
	{
		return m_strCurrentLine.substr(i + 1, m_strCurrentLine.find(' ', i) - (i + 1));
	}

	return "";
}

string XMLReader::GetAttributeValue(string attribute)
{
	// get the value of the given attribute name
	string str = "0";

	unsigned int i = m_strCurrentLine.find(attribute);
	if (i < m_strCurrentLine.length())
	{
		i = m_strCurrentLine.find('\"', i);

		if (i < m_strCurrentLine.length())
		{
			return m_strCurrentLine.substr(i + 1, m_strCurrentLine.find('\"', i + 1) - (i + 1));
		}
	}

	return str;
}
