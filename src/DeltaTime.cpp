#include "DeltaTime.h"

DeltaTime::DeltaTime()
	: m_Time(std::chrono::system_clock::now())
	, m_Delta(0)
{
	
}

void DeltaTime::CalculateDelta()
{
	DeltaTime& ins = _Instance();
	std::chrono::time_point<std::chrono::system_clock> time = std::chrono::system_clock::now();
	ins.m_Delta = time - ins.m_Time;
	ins.m_Time = time;
}

double DeltaTime::GetDelta()
{
	return _Instance().m_Delta.count();
}