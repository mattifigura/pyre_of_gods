#include "TerrainChunk.h"
#include "Terrain.h"
#include "SceneManager.h"

TerrainChunk::TerrainChunk(Manager* parent, Terrain* terrain, vector3di chunkPos)
	: GLObject(parent, vector3df(), vector3di(320, 320, 320))
	, m_pTerrain(terrain)
	, m_ChunkPos(chunkPos)
	, m_bMeshCreated(false)
	, m_bBuffersCreated(false)
	, m_bCreating(false)
	, m_bInitialised(false)
	, m_bWaterInitialised(false)
	, m_bBlockMeshesCreated(false)
	, m_WaterVBO(0)
	, m_WaterEBO(0)
	, m_WaterIndexCount(0)
	, m_bWaterSettled(true)
	, m_bHasWater(false)
	, m_fWaterTimer(0.f)
	, m_CreatingCounter(0)
{
	// set the texture file
	SetTexture("terrain_tiles.png");
	SetNormalTexture("terrain_tiles_normals.png");

	SetPosition(vector3df(chunkPos.x * CHUNK_SIZE * BLOCK_SIZE, chunkPos.y * CHUNK_SIZE * BLOCK_SIZE, chunkPos.z * CHUNK_SIZE * BLOCK_SIZE));
	SetAABB(vector3df(), vector3df(CHUNK_SIZE * BLOCK_SIZE));

	// make sure we are filled with bright blocks
	TerrainBlock space(0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 15);
	for (GLint i = 0; i < CHUNK_SIZE; ++i) {
		for (GLint j = 0; j < CHUNK_SIZE; ++j) {
			for (GLint k = 0; k < CHUNK_SIZE; ++k) {
				m_Terrain[i * CHUNK_SIZE * CHUNK_SIZE + j * CHUNK_SIZE + k] = space;
			}
		}
	}

	UpdateTransformationMatrix();
}

TerrainChunk::~TerrainChunk()
{
	// clear buffers
	if (m_WaterVBO > 0)
		glDeleteBuffers(1, &m_WaterVBO);
	if (m_WaterEBO > 0)
		glDeleteBuffers(1, &m_WaterEBO);
	m_WaterList.clear();

	// delete multi block lists
	map<vector3di, std::list<vector3di>*>::iterator it = m_MultiBlockLists.begin();
	std::list<std::list<vector3di>*> todelete;
	while (it != m_MultiBlockLists.end())
	{
		if (std::find(todelete.begin(), todelete.end(), it->second) == todelete.end())
		{
			todelete.push_back(it->second);
			it->second->clear();
			delete it->second;
		}
		++it;
	}
	m_MultiBlockLists.clear();
	todelete.clear();

	// delete block meshes
	map<GLuint, Mesh*>::iterator bit = m_BlockMeshes.begin();
	while (bit != m_BlockMeshes.end())
	{
		delete bit->second;

		++bit;
	}
	m_BlockMeshes.clear();

	// delete any animated objects
	map<vector3di, AnimatedBlock>::iterator ait = m_AnimatedBlocks.begin();
	while (ait != m_AnimatedBlocks.end())
	{
		delete ait->second.anim;

		++ait;
	}
	m_AnimatedBlocks.clear();

	// delete any particle emitters
	SceneManager* pSceneMan = GetSceneManager();
	map<vector3di, vector<ParticleEmitter*>>::iterator pit = m_BlockParticleEmitters.begin();
	while (pit != m_BlockParticleEmitters.end())
	{
		for (GLuint i = 0; i < pit->second.size(); ++i) {
			if (!pSceneMan->DeleteObject(pit->second[i]))
				delete pit->second[i];
		}
		pit->second.clear();

		++pit;
	}
	m_BlockParticleEmitters.clear();
}

void TerrainChunk::UnpackTerrain(Bytef* buf, uLongf len, bool bCreateVisibility)
{
	// as we don't generate the terrain in the client anymore, we will uncompress the terrain data and create ourself from that
	uLongf size = CHUNK_BUFFER_SIZE;
	Bytef* dest = new Bytef[size];
	uncompress(dest, &size, buf, len);

	// rather than memcpy, loop through and call setblock to do this properly
	TerrainBlock* blocks = reinterpret_cast<TerrainBlock*>(dest);
	for (GLuint i = 0; i < CHUNK_SIZE; ++i) {
		for (GLuint j = 0; j < CHUNK_SIZE; ++j) {
			for (GLuint k = 0; k < CHUNK_SIZE; ++k) {
				SetBlock(vector3di(i, k, j), blocks[i * CHUNK_SIZE * CHUNK_SIZE + j * CHUNK_SIZE + k]);

				// sort out particle emitters
				SortParticleEmitter(i, k, j, blocks[i * CHUNK_SIZE * CHUNK_SIZE + j * CHUNK_SIZE + k]);
			}
		}
	}

	delete[] dest;

	// we now need to create the visibility graph for this chunk
	if (bCreateVisibility)
		CalculateFaceVisibility();

	// set the initialised flag so we can create the mesh
	m_bInitialised = true;
}

void TerrainChunk::UnpackWater(Bytef* buf, uLongf len)
{
	// unpack the water and copy the data into our water & new water arrays
	uLongf size = CHUNK_BUFFER_SIZE;
	Bytef* dest = new Bytef[size];
	uncompress(dest, &size, buf, len);

	// go through and ensure the blocks have the water bit set
	GLubyte* water = reinterpret_cast<GLubyte*>(dest);
	for (GLuint x = 0; x < CHUNK_SIZE; ++x) {
		for (GLuint z = 0; z < CHUNK_SIZE; ++z) {
			for (GLuint y = 0; y < CHUNK_SIZE; ++y) {
				if (water[x * CHUNK_SIZE * CHUNK_SIZE + z * CHUNK_SIZE + y] > 0.f)
				{
					SetWater(vector3di(x, y, z), water[x * CHUNK_SIZE * CHUNK_SIZE + z * CHUNK_SIZE + y] * 0.004f);
					SetNewWater(vector3di(x, y, z), water[x * CHUNK_SIZE * CHUNK_SIZE + z * CHUNK_SIZE + y] * 0.004f);
					BlockAt(vector3di(x, y, z)).Water = 1;
					m_bHasWater = true;
				}
			}
		}
	}

	// clean up and set flags
	delete[] dest;
	m_bWaterInitialised = true;
	m_bWaterSettled = true;
}

bool TerrainChunk::CompressTerrain(Bytef** buf, uLongf& len)
{
	// if we're not yet initialised, don't do anything
	if (!m_bInitialised)
		return false;

	// compress this terrain to the buffer, used only to save to the network manager if we've updated chunks not in our memory
	*buf = new Bytef[COMPRESSED_CHUNK_BUFFER_SIZE];
	len = COMPRESSED_CHUNK_BUFFER_SIZE;
	Bytef* tdata = reinterpret_cast<Bytef*>(m_Terrain);

	if (compress(*buf, &len, tdata, CHUNK_BUFFER_SIZE) == 0)
		return true;

	delete[] * buf;
	*buf = NULL;
	return false;
}

bool TerrainChunk::CompressWater(Bytef** buf, uLongf& len)
{
	// if we're not yet initialised, don't do anything
	if (!m_bWaterInitialised)
		return false;

	// compress this terrain to the buffer, used only to save to the network manager if we've updated chunks not in our memory
	*buf = new Bytef[COMPRESSED_CHUNK_BUFFER_SIZE];
	len = COMPRESSED_CHUNK_BUFFER_SIZE;
	Bytef* tdata = reinterpret_cast<Bytef*>(m_CurrentWater);

	if (compress(*buf, &len, tdata, CHUNK_WATER_BUFFER_SIZE) == 0)
		return true;

	delete[] * buf;
	*buf = NULL;
	return false;
}

void TerrainChunk::CalculateFaceVisibility()
{
	// calculate this chunk's face visibility
	int iBlocksLeft = CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE;
	int iLastStart = 0;
	/*bool* state = new bool[iBlocksLeft];
	bool* added = new bool[iBlocksLeft];
	memset(state, 0, iBlocksLeft);
	memset(added, 0, iBlocksLeft);*/
	bool state[CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE] = { 0 };
	bool added[CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE] = { 0 };

	// clear our current visibility
	memset(&m_Visibility, 0, sizeof(FaceVisibility));

	// array to hold the visibility of each face for this flood fill
	// 0 = -x, 1 = +x, 2 = -z, 3 = +z, 4 = -y, 5 = +y
	bool visible[6] = { 0 };

	// queue for the flood fill
	std::queue<vector3di> checks;

	// function to add a block to the flood fill queue
	auto check_block = [&](const vector3di& pos) {
		if (pos.x >= 0 && pos.x < CHUNK_SIZE && pos.y >= 0 && pos.y < CHUNK_SIZE && pos.z >= 0 && pos.z < CHUNK_SIZE)
		{
			if (!state[pos.x * CHUNK_SIZE * CHUNK_SIZE + pos.z * CHUNK_SIZE + pos.y] && !added[pos.x * CHUNK_SIZE * CHUNK_SIZE + pos.z * CHUNK_SIZE + pos.y])
			{
				TerrainBlock& b = m_Terrain[pos.x * CHUNK_SIZE * CHUNK_SIZE + pos.z * CHUNK_SIZE + pos.y];
				if (!b.Solid || b.Ignored)
				{
					// add this block to the queue
					checks.push(pos);
					added[pos.x * CHUNK_SIZE * CHUNK_SIZE + pos.z * CHUNK_SIZE + pos.y] = true;
				}
				else
				{
					// this block is solid, remove it from the list to process
					state[pos.x * CHUNK_SIZE * CHUNK_SIZE + pos.z * CHUNK_SIZE + pos.y] = true;
					--iBlocksLeft;
				}
			}
		}
	};

	while (iBlocksLeft > 0)
	{
		vector3di startblock;
		bool bStartFound(false);

		// find the start block for the next flood fill
		for (int i = iLastStart; i < CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE && !bStartFound; ++i) {
			TerrainBlock& b = m_Terrain[i];
			if (!state[i] && (!b.Solid || b.Ignored))
			{
				int x = i / (CHUNK_SIZE * CHUNK_SIZE);
				int z = (i - (x * CHUNK_SIZE * CHUNK_SIZE)) / CHUNK_SIZE;
				int y = i % CHUNK_SIZE;
				startblock.Set(x, y, z);
				bStartFound = true;
				iLastStart = i + 1;
			}
			else
			{
				--iBlocksLeft;
				state[i] = true;
			}
		}

		while (!checks.empty())
			checks.pop();

		checks.push(startblock);

		for (int i = 0; i < 6; ++i) {
			visible[i] = false;
		}

		// perform the flood fill
		while (!checks.empty() && iBlocksLeft > 0)
		{
			// get the block here
			vector3di pos = checks.front();
			checks.pop();

			// set this block to processed
			state[pos.x * CHUNK_SIZE * CHUNK_SIZE + pos.z * CHUNK_SIZE + pos.y] = true;
			added[pos.x * CHUNK_SIZE * CHUNK_SIZE + pos.z * CHUNK_SIZE + pos.y] = true;
			--iBlocksLeft;

			// check if we have hit any faces
			if (pos.x == 0) visible[0] = true;
			else if (pos.x == CHUNK_SIZE - 1) visible[1] = true;
			if (pos.z == 0) visible[2] = true;
			else if (pos.z == CHUNK_SIZE - 1) visible[3] = true;
			if (pos.y == 0) visible[4] = true;
			else if (pos.y == CHUNK_SIZE - 1) visible[5] = true;

			// check blocks around us and add them to the queue if necessary
			check_block(pos + vector3di(-1, 0, 0));
			check_block(pos + vector3di(1, 0, 0));
			check_block(pos + vector3di(0, -1, 0));
			check_block(pos + vector3di(0, 1, 0));
			check_block(pos + vector3di(0, 0, -1));
			check_block(pos + vector3di(0, 0, 1));
		}

		// now use the visibility array to set this chunk's visibility
		if (visible[0])
		{
			if (visible[1]) m_Visibility.EastWest = 1;
			if (visible[2]) m_Visibility.SouthEast = 1;
			if (visible[3]) m_Visibility.NorthEast = 1;
			if (visible[4]) m_Visibility.EastDown = 1;
			if (visible[5]) m_Visibility.EastUp = 1;
		}
		if (visible[1])
		{
			if (visible[2]) m_Visibility.SouthWest = 1;
			if (visible[3]) m_Visibility.NorthWest = 1;
			if (visible[4]) m_Visibility.WestDown = 1;
			if (visible[5]) m_Visibility.WestUp = 1;
		}
		if (visible[2])
		{
			if (visible[3]) m_Visibility.NorthSouth = 1;
			if (visible[4]) m_Visibility.SouthDown = 1;
			if (visible[5]) m_Visibility.SouthUp = 1;
		}
		if (visible[3])
		{
			if (visible[4]) m_Visibility.NorthDown = 1;
			if (visible[5]) m_Visibility.NorthUp = 1;
		}
		if (visible[4])
		{
			if (visible[5]) m_Visibility.UpDown = 1;
		}

		// if we are all visible, no point in continuing
		bool bDone(true);
		for (int i = 0; i < 6; ++i) {
			if (!visible[i])
				bDone = false;
		}

		if (bDone)
			break;
	}

	// clean up
	//delete[] added;
	//delete[] state;

	// set the initialised flag to true
	m_Visibility.Initialised = 1;
}

bool TerrainChunk::CanFaceSeeFace(int face1, int face2)
{
	// if we aren't initialised, return false
	if (!m_Visibility.Initialised > 0)
		return false;
	
	// determine whether the given faces can see each other
	// 0 = -x, 1 = +x, 2 = -z, 3 = +z, 4 = -y, 5 = +y
	switch (face1)
	{
		default:
		case 0:
		{
			if (face2 == 1) return m_Visibility.EastWest > 0;
			if (face2 == 2) return m_Visibility.SouthEast > 0;
			if (face2 == 3) return m_Visibility.NorthEast > 0;
			if (face2 == 4) return m_Visibility.EastDown > 0;
			if (face2 == 5) return m_Visibility.EastUp > 0;
			break;
		}
		case 1:
		{
			if (face2 == 0) return m_Visibility.EastWest > 0;
			if (face2 == 2) return m_Visibility.SouthWest > 0;
			if (face2 == 3) return m_Visibility.NorthWest > 0;
			if (face2 == 4) return m_Visibility.WestDown > 0;
			if (face2 == 5) return m_Visibility.WestUp > 0;
			break;
		}
		case 2:
		{
			if (face2 == 0) return m_Visibility.SouthEast > 0;
			if (face2 == 1) return m_Visibility.SouthWest > 0;
			if (face2 == 3) return m_Visibility.NorthSouth > 0;
			if (face2 == 4) return m_Visibility.SouthDown > 0;
			if (face2 == 5) return m_Visibility.SouthUp > 0;
			break;
		}
		case 3:
		{
			if (face2 == 0) return m_Visibility.NorthEast > 0;
			if (face2 == 1) return m_Visibility.NorthWest > 0;
			if (face2 == 2) return m_Visibility.NorthSouth > 0;
			if (face2 == 4) return m_Visibility.NorthDown > 0;
			if (face2 == 5) return m_Visibility.NorthUp > 0;
			break;
		}
		case 4:
		{
			if (face2 == 0) return m_Visibility.EastDown > 0;
			if (face2 == 1) return m_Visibility.WestDown > 0;
			if (face2 == 2) return m_Visibility.SouthDown > 0;
			if (face2 == 3) return m_Visibility.NorthDown > 0;
			if (face2 == 5) return m_Visibility.UpDown > 0;
			break;
		}
		case 5:
		{
			if (face2 == 0) return m_Visibility.EastUp > 0;
			if (face2 == 1) return m_Visibility.WestUp > 0;
			if (face2 == 2) return m_Visibility.SouthUp > 0;
			if (face2 == 3) return m_Visibility.NorthUp > 0;
			if (face2 == 4) return m_Visibility.UpDown > 0;
			break;
		}
	}

	// if we have hit none of the above cases (ie face1 == face2), return true
	return true;
}

bool TerrainChunk::CanSeeFace(int face)
{
	// if we aren't initialised, return false
	if (!m_Visibility.Initialised > 0)
		return false;

	// switch on face
	// 0 = -x, 1 = +x, 2 = -z, 3 = +z, 4 = -y, 5 = +y
	switch (face)
	{
		default:
		case 0:
			return (m_Visibility.EastDown > 0 || m_Visibility.EastUp > 0 || m_Visibility.EastWest > 0 || m_Visibility.NorthEast > 0 || m_Visibility.SouthEast > 0);
			break;
		case 1:
			return (m_Visibility.WestDown > 0 || m_Visibility.WestUp > 0 || m_Visibility.EastWest > 0 || m_Visibility.NorthWest > 0 || m_Visibility.SouthWest > 0);
			break;
		case 2:
			return (m_Visibility.SouthDown > 0 || m_Visibility.SouthUp > 0 || m_Visibility.SouthEast > 0 || m_Visibility.SouthWest > 0 || m_Visibility.NorthSouth > 0);
			break;
		case 3:
			return (m_Visibility.NorthDown > 0 || m_Visibility.NorthEast > 0 || m_Visibility.NorthWest > 0 || m_Visibility.NorthUp > 0 || m_Visibility.NorthSouth > 0);
			break;
		case 4:
			return (m_Visibility.UpDown > 0 || m_Visibility.NorthDown > 0 || m_Visibility.SouthDown > 0 || m_Visibility.WestDown > 0 || m_Visibility.EastDown > 0);
			break;
		case 5:
			return (m_Visibility.UpDown > 0 || m_Visibility.NorthUp > 0 || m_Visibility.SouthUp > 0 || m_Visibility.EastUp > 0 || m_Visibility.WestUp > 0);
			break;
	}

	// if we have hit none of the above cases (ie face1 == face2), return false
	return false;
}

void TerrainChunk::_GenerateTree(GLint x, GLint y, GLint z)
{
	// decide on a height
	++y;

	TerrainBlock tree(Item::GetBlockFromName("Tree"));
	tree.SkyLight = 15;
	tree.RotY = (x + y + z) % 4;
	tree.Ignored = 1;
	m_Terrain[x * CHUNK_SIZE * CHUNK_SIZE + z * CHUNK_SIZE + y] = tree;

	std::list<vector3di>* list = new std::list<vector3di>;

	list->push_back(vector3di(x, y, z));
	AddToMultiBlockList(vector3di(x, y, z), list);

	tree.Multi = 1;
	for (GLuint k = 1; k < 5; ++k) {
		if (y + k >= CHUNK_SIZE)
			break;

		tree.SkyLight = _GetBlock(x, y + k + 1, z).SkyLight;
		m_Terrain[x * CHUNK_SIZE * CHUNK_SIZE + z * CHUNK_SIZE + y + k] = tree;

		list->push_back(vector3di(x, y + k, z));
		AddToMultiBlockList(vector3di(x, y + k, z), list);
	}
}

GLfloat RotateBlockMesh(TerrainBlock block)
{
	// use this block's rotation values to rotate the block
	int x = block.RotX;
	int y = block.RotY;
	int z = block.RotZ;

	GLfloat rot = 0;

	// 0 : +y : top
	// 1 : -y : bottom
	// 2 : -x : front
	// 3 : +x : back
	// 4 : -z : right
	// 5 : +z : left

	// if z is 0, this is normal (upright), sorting UVs shall be simple
	if (z == 0)
	{
		if (x == 1)
		{
			if (y == 1)
			{
				// if x and y are 1, rotate the block -90 degrees
				rot = -PI_OVER_2;
			}
			else
			{
				// if x is 1 and y is 0, rotate the block 180 degrees
				rot = PI;
			}
		}
		else if (y == 1)
		{
			// if y is 1, rotate the block 90 degrees
			rot = PI_OVER_2;
		}
	}
	else
	{

	}

	return rot;
}

void TerrainChunk::_ApplyLightingToMesh(Mesh* mesh, matrix4f transform, TerrainBlock block) const
{
	// light up the mesh based off of the light values of adjacent blocks
	std::vector<GLVertex>* verts = mesh->GetVertexPointer();
	std::vector<GLuint>* inds = mesh->GetIndexPointer();

	static vector3df normals[] = { vector3df(1, 0, 0), vector3df(-1, 0, 0), vector3df(0, 1, 0), vector3df(0, -1, 0), vector3df(0, 0, 1), vector3df(0, 0, -1) };

	auto get_side = [&](vector3df& n) -> GLuint {
		// 0 : +x, 1 : -x
		// 2 : +y, 3 : -y
		// 4 : +z, 5 : -z
		GLuint side = 0;
		GLfloat val = 0;
		GLfloat tmp = 0;

		val = n.Dot(normals[0]);

		for (GLuint i = 1; i < 6; ++i) {
			tmp = n.Dot(normals[i]);

			if (tmp >= 0.99f)
				return i;

			if (tmp > val)
			{
				val = tmp;
				side = i;
			}
		}

		return side;
	};

	// get the inverse transpose of mat for normal calculations
	matrix4f vMat(transform);
	if (vMat.Inverse())
		vMat.Transpose();

	for (GLuint i = 0; i < verts->size(); ++i) {
		GLVertex& vert = verts->at(i);
		vector3df n;

		vector4df _n(vert.n.ToVector());
		_n = vMat.Multiply(_n);
		n = vector3df(_n).Normal();

		vector4df _p(vert.pos);
		_p.w = 1;
		vector4df p(transform.Multiply(_p));

		GLuint side = get_side(n);
		TerrainBlock b;
		GLuint j = 1;
		do
		{
			// get the next empty block to take lighting values from
			b = m_pTerrain->GetBlock(p + (normals[side] * 10 * j));
			++j;
		} while (b.BlockType == block.BlockType && b.Multi == 1);

		if (b.Solid || b.SkyLight == 0)
			b = m_pTerrain->GetBlock(p);

		vert.light.Set(b.LightRed * 17, b.LightGreen * 17, b.LightBlue * 17, b.SkyLight * 17);
	}
}

void TerrainChunk::CreateMesh()
{
	if (m_bMeshCreated)
	{
		m_bCreating = false;
		return;
	}

	// function to help smooth lighting
	auto smooth_lighting = [](GLuint l1, GLuint l2, GLuint l3, GLuint l4) -> GLubyte {
		GLubyte total = l1;
		GLuint n = 1;
		GLuint t = l1 - 1 >= 0 ? l1 - 1 : 0;

		if (l2 >= t)
		{
			total += l2;
			++n;
		}
		if (l3 >= t)
		{
			total += l3;
			++n;
		}
		if (l4 >= t)
		{
			total += l4;
			++n;
		}

		return (GLuint)(total * 17 * OneOver(n));
	};

	// clear block meshes
	map<GLuint, Mesh*>::iterator it = m_BlockMeshes.begin();
	while (it != m_BlockMeshes.end())
	{
		it->second->ClearVectors();

		++it;
	}
	m_bBlockMeshesCreated = false;

	// function for adding a mesh to the terrain
	auto add_mesh = [&](Mesh* m, Mesh* meshToAdd, const TerrainBlock& block, const vector3di& pos, const vector3di& offset, GLfloat rot) {
		matrix4f transform;
		transform.SetRotation(vector3df(0, rot, 0));
		transform.SetTranslation(vector3df(m_ChunkPos.x * CHUNK_SIZE * BLOCK_SIZE + pos.x * BLOCK_SIZE + (offset.x * BLOCK_SIZE) / 2,
			m_ChunkPos.y * CHUNK_SIZE * BLOCK_SIZE + pos.y * BLOCK_SIZE,
			m_ChunkPos.z * CHUNK_SIZE * BLOCK_SIZE + pos.z * BLOCK_SIZE + (offset.z * BLOCK_SIZE) / 2));

		Mesh* mesh = new Mesh();
		mesh->CopyFrom(meshToAdd);

		_ApplyLightingToMesh(mesh, transform, block);

		m->AddFrom(mesh, transform);
		delete mesh;
	};

	// function to join connected meshes
	auto join_mesh = [&](Mesh* m, Mesh* meshToAdd, const TerrainBlock& block, const vector3di& pos, const vector3di& offset, const vector3di& _where) {
		vector3di check(pos.x + _where.x, pos.y + _where.y, pos.z + _where.z);
		TerrainBlock b = _GetBlock(check.x, check.y, check.z);
		if (b.BlockType == block.BlockType)
		{
			// figure out the rotation
			vector3di diff = check - vector3di(pos);
			GLfloat rot = 0.f;
			if (diff.z < 0) rot = PI;
			else if (diff.x < 0) rot = -PI_OVER_2;
			else if (diff.x > 0) rot = PI_OVER_2;

			// add the mesh!
			add_mesh(m, meshToAdd, block, pos, offset, rot);
		}
	};

	// create each face of the terrain
	TerrainBlock block;
	vector4df uv, correctedUV[6], leakedUV[6];
	GLColour col(255);
	for (int k = 0; k < CHUNK_SIZE; ++k) {
		for (int j = 0; j < CHUNK_SIZE; ++j) {
			for (int i = 0; i < CHUNK_SIZE; ++i) {
				block = GetBlock(i, k, j);

				if (block.BlockType == 0)
				{
					continue;
				}

				if (block.Multi)
					continue;

				const ItemClass* ic = Item::GetItemClass(block);

				if (!ic || ic->bAnimated)
					continue;

				if (ic->bBlockHasMesh)
				{
					GLuint tex = GetSceneManager()->GetTextureManager()->GetTexture(ic->strTexturePath).gltex;
					Mesh* m;
					if (m_BlockMeshes.find(tex) == m_BlockMeshes.end())
					{
						// create a new mesh for this texture
						m = new Mesh();
						m->SetClearData(true);
						m_BlockMeshes.emplace(tex, m);
					}
					else
						m = m_BlockMeshes.at(tex);

					GLfloat rot = RotateBlockMesh(block);
					vector3di offset = Item::GetItemClass(block)->BlockSize;
					if (rot == 0 || rot == PI)
					{
						GLint tmp = offset.x;
						offset.x = offset.z;
						offset.z = tmp;
					}

					add_mesh(m, ic->Meshes[0], block, vector3di(i, k, j), offset, rot);

					// if this mesh connects, we need to use a second mesh to join it to adjacent blocks of the same type
					if (ic->sValues[7] > 0 && ic->Meshes.size() > 1)
					{
						join_mesh(m, ic->Meshes[1], block, vector3di(i, k, j), offset, vector3di(1, 0, 0));
						join_mesh(m, ic->Meshes[1], block, vector3di(i, k, j), offset, vector3di(-1, 0, 0));
						join_mesh(m, ic->Meshes[1], block, vector3di(i, k, j), offset, vector3di(0, 0, 1));
						join_mesh(m, ic->Meshes[1], block, vector3di(i, k, j), offset, vector3di(0, 0, -1));
					}

					continue;
				}

				_CorrectUV(ic->TerrainUV, correctedUV, block);

				// leaked blocks
				bool bLeakedPositiveX(false);
				bool bLeakedNegativeX(false);
				bool bLeakedPositiveZ(false);
				bool bLeakedNegativeZ(false);
				if (block.Leaked)
				{
					TerrainBlock above = _GetBlock(i, k + 1, j);
					const ItemClass* aic = Item::GetItemClass(above);
					if (aic)
					{
						_CorrectUV(aic->TerrainUV, leakedUV, block);

						if (above.XX && above.YX)
							bLeakedNegativeX = true;
						if (above.XX && above.XY)
							bLeakedNegativeZ = true;
						if (above.YX && above.YY)
							bLeakedPositiveZ = true;
						if (above.XY && above.YY)
							bLeakedPositiveX = true;
					}
				}

				TerrainBlock blocks[] = {
					_GetBlock(i - 1, k + 1, j - 1), _GetBlock(i, k + 1, j - 1), _GetBlock(i + 1, k + 1, j - 1),
					_GetBlock(i - 1, k + 1, j), _GetBlock(i, k + 1, j), _GetBlock(i + 1, k + 1, j),
					_GetBlock(i - 1, k + 1, j + 1), _GetBlock(i, k + 1, j + 1), _GetBlock(i + 1, k + 1, j + 1),

					_GetBlock(i - 1, k, j - 1), _GetBlock(i, k, j - 1), _GetBlock(i + 1, k, j - 1),
					_GetBlock(i - 1, k, j), _GetBlock(i, k, j), _GetBlock(i + 1, k, j),
					_GetBlock(i - 1, k, j + 1), _GetBlock(i, k, j + 1), _GetBlock(i + 1, k, j + 1),

					_GetBlock(i - 1, k - 1, j - 1), _GetBlock(i, k - 1, j - 1), _GetBlock(i + 1, k - 1, j - 1),
					_GetBlock(i - 1, k - 1, j), _GetBlock(i, k - 1, j), _GetBlock(i + 1, k - 1, j),
					_GetBlock(i - 1, k - 1, j + 1), _GetBlock(i, k - 1, j + 1), _GetBlock(i + 1, k - 1, j + 1),
				};

				// x
				TerrainBlock oblock = _GetBlock(i - 1, k, j);
				bool bSpace = oblock.Ignored || !oblock.Solid;
				if (bSpace || oblock.XY || oblock.YY)
				{
					// get the light
					//_MergeLighting(i - 1, k, j, col);

					bool ao1 = ((blocks[21].Solid) || (blocks[18].Solid) || (blocks[9].Solid) || (blocks[12].Solid));
					bool ao2 = ((blocks[21].Solid) || (blocks[24].Solid) || (blocks[15].Solid) || (blocks[12].Solid));
					bool ao3 = ((blocks[3].Solid) || (blocks[6].Solid) || (blocks[15].Solid) || (blocks[12].Solid));
					bool ao4 = ((blocks[3].Solid) || (blocks[0].Solid) || (blocks[9].Solid) || (blocks[12].Solid));
					GLint s1 = 255 - ao1 * 40;
					GLint s2 = 255 - ao2 * 40;
					GLint s3 = 255 - ao3 * 40;
					GLint s4 = 255 - ao4 * 40;

					GLubyte r1 = smooth_lighting(blocks[12].LightRed, blocks[21].LightRed, blocks[18].LightRed, blocks[9].LightRed);
					GLubyte r2 = smooth_lighting(blocks[12].LightRed, blocks[21].LightRed, blocks[24].LightRed, blocks[15].LightRed);
					GLubyte r3 = smooth_lighting(blocks[12].LightRed, blocks[3].LightRed, blocks[6].LightRed, blocks[15].LightRed);
					GLubyte r4 = smooth_lighting(blocks[12].LightRed, blocks[3].LightRed, blocks[0].LightRed, blocks[9].LightRed);
					GLubyte g1 = smooth_lighting(blocks[12].LightGreen, blocks[21].LightGreen, blocks[18].LightGreen, blocks[9].LightGreen);
					GLubyte g2 = smooth_lighting(blocks[12].LightGreen, blocks[21].LightGreen, blocks[24].LightGreen, blocks[15].LightGreen);
					GLubyte g3 = smooth_lighting(blocks[12].LightGreen, blocks[3].LightGreen, blocks[6].LightGreen, blocks[15].LightGreen);
					GLubyte g4 = smooth_lighting(blocks[12].LightGreen, blocks[3].LightGreen, blocks[0].LightGreen, blocks[9].LightGreen);
					GLubyte b1 = smooth_lighting(blocks[12].LightBlue, blocks[21].LightBlue, blocks[18].LightBlue, blocks[9].LightBlue);
					GLubyte b2 = smooth_lighting(blocks[12].LightBlue, blocks[21].LightBlue, blocks[24].LightBlue, blocks[15].LightBlue);
					GLubyte b3 = smooth_lighting(blocks[12].LightBlue, blocks[3].LightBlue, blocks[6].LightBlue, blocks[15].LightBlue);
					GLubyte b4 = smooth_lighting(blocks[12].LightBlue, blocks[3].LightBlue, blocks[0].LightBlue, blocks[9].LightBlue);
					GLubyte sky1 = smooth_lighting(blocks[12].SkyLight, blocks[21].SkyLight, blocks[18].SkyLight, blocks[9].SkyLight);
					GLubyte sky2 = smooth_lighting(blocks[12].SkyLight, blocks[21].SkyLight, blocks[24].SkyLight, blocks[15].SkyLight);
					GLubyte sky3 = smooth_lighting(blocks[12].SkyLight, blocks[3].SkyLight, blocks[6].SkyLight, blocks[15].SkyLight);
					GLubyte sky4 = smooth_lighting(blocks[12].SkyLight, blocks[3].SkyLight, blocks[0].SkyLight, blocks[9].SkyLight);

					// create the face
					uv = bLeakedNegativeX ? leakedUV[2] : correctedUV[2];

					if ((bSpace || oblock.XY || oblock.YY) && (!block.XX && block.YX))
					{
						//_MergeLighting(i - 1, k + !bSpace, j, col);

						AddTriangle(GLVertex(vector3df((i + 0) * BLOCK_SIZE, (k + 1) * BLOCK_SIZE, (j + 0) * BLOCK_SIZE), GLColour(s4), vector2df(uv.x, uv.y), GLNormal(-1, 0, 0), GLColour(r4, g4, b4, sky4)),
							GLVertex(vector3df((i + 0) * BLOCK_SIZE, (k + 0) * BLOCK_SIZE, (j + 0) * BLOCK_SIZE), GLColour(s1), vector2df(uv.x, uv.w), GLNormal(-1, 0, 0), GLColour(r1, g1, b1, sky1)),
							GLVertex(vector3df((i + 0) * BLOCK_SIZE, (k + 0) * BLOCK_SIZE, (j + 1) * BLOCK_SIZE), GLColour(s2), vector2df(uv.z, uv.y), GLNormal(-1, 0, 0), GLColour(r2, g2, b2, sky2)));
					}
					else if ((bSpace || oblock.XY || oblock.YY) && (block.XX && !block.YX))
					{
						//_MergeLighting(i - 1, k + !bSpace, j, col);

						AddTriangle(GLVertex(vector3df((i + 0) * BLOCK_SIZE, (k + 0) * BLOCK_SIZE, (j + 0) * BLOCK_SIZE), GLColour(s1), vector2df(uv.x, uv.y), GLNormal(-1, 0, 0), GLColour(r1, g1, b1, sky1)),
							GLVertex(vector3df((i + 0) * BLOCK_SIZE, (k + 0) * BLOCK_SIZE, (j + 1) * BLOCK_SIZE), GLColour(s2), vector2df(uv.z, uv.w), GLNormal(-1, 0, 0), GLColour(r2, g2, b2, sky2)),
							GLVertex(vector3df((i + 0) * BLOCK_SIZE, (k + 1) * BLOCK_SIZE, (j + 1) * BLOCK_SIZE), GLColour(s3), vector2df(uv.z, uv.y), GLNormal(-1, 0, 0), GLColour(r3, g3, b3, sky3)));
					}
					else if ((bSpace || (!bSpace && oblock.XY && oblock.YY)) && !block.XX && !block.YX)
					{
						//if (oblock.XY && oblock.YY)
						//	_MergeLighting(i - 1, k + 1, j, col);

						AddQuad(GLVertex(vector3df((i + 0) * BLOCK_SIZE, (k + 0) * BLOCK_SIZE, (j + 0) * BLOCK_SIZE), GLColour(s1), vector2df(uv.x, uv.y), GLNormal(-1, 0, 0), GLColour(r1, g1, b1, sky1)),
							GLVertex(vector3df((i + 0) * BLOCK_SIZE, (k + 0) * BLOCK_SIZE, (j + 1) * BLOCK_SIZE), GLColour(s2), vector2df(uv.z, uv.y), GLNormal(-1, 0, 0), GLColour(r2, g2, b2, sky2)),
							GLVertex(vector3df((i + 0) * BLOCK_SIZE, (k + 1) * BLOCK_SIZE, (j + 1) * BLOCK_SIZE), GLColour(s3), vector2df(uv.z, uv.w), GLNormal(-1, 0, 0), GLColour(r3, g3, b3, sky3)),
							GLVertex(vector3df((i + 0) * BLOCK_SIZE, (k + 1) * BLOCK_SIZE, (j + 0) * BLOCK_SIZE), GLColour(s4), vector2df(uv.x, uv.w), GLNormal(-1, 0, 0), GLColour(r4, g4, b4, sky4)));
					}
					else if (!block.YX && !oblock.XY && oblock.YY)
					{
						//_MergeLighting(i - 1, k + 1, j, col);

						AddTriangle(GLVertex(vector3df((i + 0) * BLOCK_SIZE, (k + 1) * BLOCK_SIZE, (j + 0) * BLOCK_SIZE), GLColour(s4), vector2df(uv.x, uv.y), GLNormal(-1, 0, 0), GLColour(r4, g4, b4, sky4)),
							GLVertex(vector3df((i + 0) * BLOCK_SIZE, (k + 0) * BLOCK_SIZE, (j + 1) * BLOCK_SIZE), GLColour(s2), vector2df(uv.z, uv.w), GLNormal(-1, 0, 0), GLColour(r2, g2, b2, sky2)),
							GLVertex(vector3df((i + 0) * BLOCK_SIZE, (k + 1) * BLOCK_SIZE, (j + 1) * BLOCK_SIZE), GLColour(s3), vector2df(uv.z, uv.y), GLNormal(-1, 0, 0), GLColour(r3, g3, b3, sky3)));
					}
					else if (!block.XX && oblock.XY && !oblock.YY)
					{
						//_MergeLighting(i - 1, k + 1, j, col);

						AddTriangle(GLVertex(vector3df((i + 0) * BLOCK_SIZE, (k + 1) * BLOCK_SIZE, (j + 0) * BLOCK_SIZE), GLColour(s4), vector2df(uv.x, uv.y), GLNormal(-1, 0, 0), GLColour(r4, g4, b4, sky4)),
							GLVertex(vector3df((i + 0) * BLOCK_SIZE, (k + 0) * BLOCK_SIZE, (j + 0) * BLOCK_SIZE), GLColour(s1), vector2df(uv.x, uv.w), GLNormal(-1, 0, 0), GLColour(r1, g1, b1, sky1)),
							GLVertex(vector3df((i + 0) * BLOCK_SIZE, (k + 1) * BLOCK_SIZE, (j + 1) * BLOCK_SIZE), GLColour(s3), vector2df(uv.z, uv.y), GLNormal(-1, 0, 0), GLColour(r3, g3, b3, sky3)));
					}
				}
				oblock = _GetBlock(i + 1, k, j);
				bSpace = oblock.Ignored || !oblock.Solid;
				if (bSpace || oblock.YX || oblock.XX)
				{
					// get the light
					//_MergeLighting(i + 1, k, j, col);

					bool ao1 = ((blocks[23].Solid) || (blocks[20].Solid) || (blocks[11].Solid) || (blocks[14].Solid));
					bool ao2 = ((blocks[23].Solid) || (blocks[26].Solid) || (blocks[17].Solid) || (blocks[14].Solid));
					bool ao3 = ((blocks[5].Solid) || (blocks[8].Solid) || (blocks[17].Solid) || (blocks[14].Solid));
					bool ao4 = ((blocks[5].Solid) || (blocks[2].Solid) || (blocks[11].Solid) || (blocks[14].Solid));
					GLint s1 = 255 - ao1 * 40;
					GLint s2 = 255 - ao2 * 40;
					GLint s3 = 255 - ao3 * 40;
					GLint s4 = 255 - ao4 * 40;

					GLubyte r1 = smooth_lighting(blocks[14].LightRed, blocks[23].LightRed, blocks[20].LightRed, blocks[11].LightRed);
					GLubyte r2 = smooth_lighting(blocks[14].LightRed, blocks[23].LightRed, blocks[26].LightRed, blocks[17].LightRed);
					GLubyte r3 = smooth_lighting(blocks[14].LightRed, blocks[5].LightRed, blocks[8].LightRed, blocks[17].LightRed);
					GLubyte r4 = smooth_lighting(blocks[14].LightRed, blocks[5].LightRed, blocks[2].LightRed, blocks[11].LightRed);
					GLubyte g1 = smooth_lighting(blocks[14].LightGreen, blocks[23].LightGreen, blocks[20].LightGreen, blocks[11].LightGreen);
					GLubyte g2 = smooth_lighting(blocks[14].LightGreen, blocks[23].LightGreen, blocks[26].LightGreen, blocks[17].LightGreen);
					GLubyte g3 = smooth_lighting(blocks[14].LightGreen, blocks[5].LightGreen, blocks[8].LightGreen, blocks[17].LightGreen);
					GLubyte g4 = smooth_lighting(blocks[14].LightGreen, blocks[5].LightGreen, blocks[2].LightGreen, blocks[11].LightGreen);
					GLubyte b1 = smooth_lighting(blocks[14].LightBlue, blocks[23].LightBlue, blocks[20].LightBlue, blocks[11].LightBlue);
					GLubyte b2 = smooth_lighting(blocks[14].LightBlue, blocks[23].LightBlue, blocks[26].LightBlue, blocks[17].LightBlue);
					GLubyte b3 = smooth_lighting(blocks[14].LightBlue, blocks[5].LightBlue, blocks[8].LightBlue, blocks[17].LightBlue);
					GLubyte b4 = smooth_lighting(blocks[14].LightBlue, blocks[5].LightBlue, blocks[2].LightBlue, blocks[11].LightBlue);
					GLubyte sky1 = smooth_lighting(blocks[14].SkyLight, blocks[23].SkyLight, blocks[20].SkyLight, blocks[11].SkyLight);
					GLubyte sky2 = smooth_lighting(blocks[14].SkyLight, blocks[23].SkyLight, blocks[26].SkyLight, blocks[17].SkyLight);
					GLubyte sky3 = smooth_lighting(blocks[14].SkyLight, blocks[5].SkyLight, blocks[8].SkyLight, blocks[17].SkyLight);
					GLubyte sky4 = smooth_lighting(blocks[14].SkyLight, blocks[5].SkyLight, blocks[2].SkyLight, blocks[11].SkyLight);

					// create the face
					uv = bLeakedPositiveX ? leakedUV[3] : correctedUV[3];

					if ((bSpace || oblock.YX || oblock.XX) && (!block.XY && block.YY))
					{
						//_MergeLighting(i + 1, k + !bSpace, j, col);

						AddTriangle(GLVertex(vector3df((i + 1) * BLOCK_SIZE, (k + 1) * BLOCK_SIZE, (j + 0) * BLOCK_SIZE), GLColour(s4), vector2df(uv.z, uv.y), GLNormal(1, 0, 0), GLColour(r4, g4, b4, sky4)),
							GLVertex(vector3df((i + 1) * BLOCK_SIZE, (k + 0) * BLOCK_SIZE, (j + 1) * BLOCK_SIZE), GLColour(s2), vector2df(uv.x, uv.y), GLNormal(1, 0, 0), GLColour(r2, g2, b2, sky2)),
							GLVertex(vector3df((i + 1) * BLOCK_SIZE, (k + 0) * BLOCK_SIZE, (j + 0) * BLOCK_SIZE), GLColour(s1), vector2df(uv.z, uv.w), GLNormal(1, 0, 0), GLColour(r1, g1, b1, sky1)));
					}
					else if ((bSpace || oblock.YX || oblock.XX) && (block.XY && !block.YY))
					{
						//_MergeLighting(i + 1, k + !bSpace, j, col);

						AddTriangle(GLVertex(vector3df((i + 1) * BLOCK_SIZE, (k + 0) * BLOCK_SIZE, (j + 0) * BLOCK_SIZE), GLColour(s1), vector2df(uv.z, uv.y), GLNormal(1, 0, 0), GLColour(r1, g1, b1, sky1)),
							GLVertex(vector3df((i + 1) * BLOCK_SIZE, (k + 1) * BLOCK_SIZE, (j + 1) * BLOCK_SIZE), GLColour(s3), vector2df(uv.x, uv.y), GLNormal(1, 0, 0), GLColour(r3, g3, b3, sky3)),
							GLVertex(vector3df((i + 1) * BLOCK_SIZE, (k + 0) * BLOCK_SIZE, (j + 1) * BLOCK_SIZE), GLColour(s2), vector2df(uv.x, uv.w), GLNormal(1, 0, 0), GLColour(r2, g2, b2, sky2)));
					}
					else if ((bSpace || (!bSpace && oblock.XX && oblock.YX)) && !block.XY && !block.YY)
					{
						//if (oblock.XX && oblock.YX)
						//	_MergeLighting(i + 1, k + 1, j, col);

						AddQuad(GLVertex(vector3df((i + 1) * BLOCK_SIZE, (k + 0) * BLOCK_SIZE, (j + 1) * BLOCK_SIZE), GLColour(s2), vector2df(uv.x, uv.y), GLNormal(1, 0, 0), GLColour(r2, g2, b2, sky2)),
							GLVertex(vector3df((i + 1) * BLOCK_SIZE, (k + 0) * BLOCK_SIZE, (j + 0) * BLOCK_SIZE), GLColour(s1), vector2df(uv.z, uv.y), GLNormal(1, 0, 0), GLColour(r1, g1, b1, sky1)),
							GLVertex(vector3df((i + 1) * BLOCK_SIZE, (k + 1) * BLOCK_SIZE, (j + 0) * BLOCK_SIZE), GLColour(s4), vector2df(uv.z, uv.w), GLNormal(1, 0, 0), GLColour(r4, g4, b4, sky4)),
							GLVertex(vector3df((i + 1) * BLOCK_SIZE, (k + 1) * BLOCK_SIZE, (j + 1) * BLOCK_SIZE), GLColour(s3), vector2df(uv.x, uv.w), GLNormal(1, 0, 0), GLColour(r3, g3, b3, sky3)));
					}
					else if (!block.YY && !oblock.XX && oblock.YX)
					{
						//_MergeLighting(i + 1, k + 1, j, col);

						AddTriangle(GLVertex(vector3df((i + 1) * BLOCK_SIZE, (k + 1) * BLOCK_SIZE, (j + 0) * BLOCK_SIZE), GLColour(s4), vector2df(uv.z, uv.y), GLNormal(1, 0, 0), GLColour(r4, g4, b4, sky4)),
							GLVertex(vector3df((i + 1) * BLOCK_SIZE, (k + 1) * BLOCK_SIZE, (j + 1) * BLOCK_SIZE), GLColour(s3), vector2df(uv.x, uv.y), GLNormal(1, 0, 0), GLColour(r3, g3, b3, sky3)),
							GLVertex(vector3df((i + 1) * BLOCK_SIZE, (k + 0) * BLOCK_SIZE, (j + 1) * BLOCK_SIZE), GLColour(s2), vector2df(uv.x, uv.w), GLNormal(1, 0, 0), GLColour(r2, g2, b2, sky2)));
					}
					else if (!block.XY && oblock.XX && !oblock.YX)
					{
						//_MergeLighting(i + 1, k + 1, j, col);

						AddTriangle(GLVertex(vector3df((i + 1) * BLOCK_SIZE, (k + 1) * BLOCK_SIZE, (j + 0) * BLOCK_SIZE), GLColour(s4), vector2df(uv.z, uv.y), GLNormal(1, 0, 0), GLColour(r4, g4, b4, sky4)),
							GLVertex(vector3df((i + 1) * BLOCK_SIZE, (k + 1) * BLOCK_SIZE, (j + 1) * BLOCK_SIZE), GLColour(s3), vector2df(uv.x, uv.y), GLNormal(1, 0, 0), GLColour(r3, g3, b3, sky3)),
							GLVertex(vector3df((i + 1) * BLOCK_SIZE, (k + 0) * BLOCK_SIZE, (j + 0) * BLOCK_SIZE), GLColour(s1), vector2df(uv.z, uv.w), GLNormal(1, 0, 0), GLColour(r1, g1, b1, sky1)));
					}
				}

				// y
				if (_BlockIsEmpty(i, k - 1, j))
				{
					// get the light
					//_MergeLighting(i, k - 1, j, col);

					bool ao1 = ((blocks[18].Solid) || (blocks[19].Solid) || (blocks[21].Solid));
					bool ao2 = ((blocks[19].Solid) || (blocks[20].Solid) || (blocks[23].Solid));
					bool ao3 = ((blocks[23].Solid) || (blocks[25].Solid) || (blocks[26].Solid));
					bool ao4 = ((blocks[21].Solid) || (blocks[24].Solid) || (blocks[25].Solid));
					GLint s1 = 255 - ao1 * 40;
					GLint s2 = 255 - ao2 * 40;
					GLint s3 = 255 - ao3 * 40;
					GLint s4 = 255 - ao4 * 40;

					GLubyte r1 = smooth_lighting(blocks[22].LightRed, blocks[18].LightRed, blocks[19].LightRed, blocks[21].LightRed);
					GLubyte r2 = smooth_lighting(blocks[22].LightRed, blocks[19].LightRed, blocks[20].LightRed, blocks[23].LightRed);
					GLubyte r3 = smooth_lighting(blocks[22].LightRed, blocks[23].LightRed, blocks[25].LightRed, blocks[26].LightRed);
					GLubyte r4 = smooth_lighting(blocks[22].LightRed, blocks[21].LightRed, blocks[24].LightRed, blocks[25].LightRed);
					GLubyte g1 = smooth_lighting(blocks[22].LightGreen, blocks[18].LightGreen, blocks[19].LightGreen, blocks[21].LightGreen);
					GLubyte g2 = smooth_lighting(blocks[22].LightGreen, blocks[19].LightGreen, blocks[20].LightGreen, blocks[23].LightGreen);
					GLubyte g3 = smooth_lighting(blocks[22].LightGreen, blocks[23].LightGreen, blocks[25].LightGreen, blocks[26].LightGreen);
					GLubyte g4 = smooth_lighting(blocks[22].LightGreen, blocks[21].LightGreen, blocks[24].LightGreen, blocks[25].LightGreen);
					GLubyte b1 = smooth_lighting(blocks[22].LightBlue, blocks[18].LightBlue, blocks[19].LightBlue, blocks[21].LightBlue);
					GLubyte b2 = smooth_lighting(blocks[22].LightBlue, blocks[19].LightBlue, blocks[20].LightBlue, blocks[23].LightBlue);
					GLubyte b3 = smooth_lighting(blocks[22].LightBlue, blocks[23].LightBlue, blocks[25].LightBlue, blocks[26].LightBlue);
					GLubyte b4 = smooth_lighting(blocks[22].LightBlue, blocks[21].LightBlue, blocks[24].LightBlue, blocks[25].LightBlue);
					GLubyte sky1 = smooth_lighting(blocks[22].SkyLight, blocks[18].SkyLight, blocks[19].SkyLight, blocks[21].SkyLight);
					GLubyte sky2 = smooth_lighting(blocks[22].SkyLight, blocks[19].SkyLight, blocks[20].SkyLight, blocks[23].SkyLight);
					GLubyte sky3 = smooth_lighting(blocks[22].SkyLight, blocks[23].SkyLight, blocks[25].SkyLight, blocks[26].SkyLight);
					GLubyte sky4 = smooth_lighting(blocks[22].SkyLight, blocks[21].SkyLight, blocks[24].SkyLight, blocks[25].SkyLight);

					// create the face
					uv = correctedUV[1];
					bool bFlipped = false;

					if (block.Half)
					{
						if (block.XX && block.YY && (block.XY || block.YX))
							bFlipped = true;
						else if (block.XY && block.YX && (block.XX || block.YY))
							bFlipped = false;
					}

					vector3df p1((i + 1) * BLOCK_SIZE, (k + 0) * BLOCK_SIZE, (j + 0) * BLOCK_SIZE);
					vector3df p2((i + 0) * BLOCK_SIZE, (k + 0) * BLOCK_SIZE, (j + 0) * BLOCK_SIZE);
					vector3df p3((i + 0) * BLOCK_SIZE, (k + 0) * BLOCK_SIZE, (j + 1) * BLOCK_SIZE);
					vector3df p4((i + 1) * BLOCK_SIZE, (k + 0) * BLOCK_SIZE, (j + 1) * BLOCK_SIZE);

					GLNormal n1, n2;
					if (bFlipped)
					{
						n1 = vector3df(0, -1, 0);
						n2 = n1;
						int iHide = 0;

						if (block.Half)
						{
							if (block.XX && block.YY && (!block.XY && block.YX))
								iHide = 1;
							else if (block.XX && block.YY && (block.XY && !block.YX))
								iHide = 2;
						}

						if (iHide != 1)
							AddTriangle(GLVertex(p4, GLColour(s3), vector2df(uv.x, uv.w), n1, GLColour(r3, g3, b3, sky3)),
								GLVertex(p3, GLColour(s4), vector2df(uv.z, uv.w), n1, GLColour(r4, g4, b4, sky4)),
								GLVertex(p2, GLColour(s1), vector2df(uv.z, uv.y), n1, GLColour(r1, g1, b1, sky1)));

						if (iHide != 2)
							AddTriangle(GLVertex(p2, GLColour(s1), vector2df(uv.z, uv.y), n2, GLColour(r1, g1, b1, sky1)),
								GLVertex(p1, GLColour(s2), vector2df(uv.x, uv.y), n2, GLColour(r2, g2, b2, sky2)),
								GLVertex(p4, GLColour(s3), vector2df(uv.x, uv.w), n2, GLColour(r3, g3, b3, sky3)));
					}
					else
					{
						n1 = vector3df(0, -1, 0);
						n2 = n1;
						int iHide = 0;

						if (block.Half)
						{
							if (block.XY && block.YX && (block.XX && !block.YY))
								iHide = 1;
							else if (block.XY && block.YX && (!block.XX && block.YY))
								iHide = 2;
						}

						if (iHide != 1)
							AddTriangle(GLVertex(p3, GLColour(s4), vector2df(uv.z, uv.w), n1, GLColour(r4, g4, b4, sky4)),
								GLVertex(p2, GLColour(s1), vector2df(uv.z, uv.y), n1, GLColour(r1, g1, b1, sky1)),
								GLVertex(p1, GLColour(s2), vector2df(uv.x, uv.y), n1, GLColour(r2, g2, b2, sky2)));

						if (iHide != 2)
							AddTriangle(GLVertex(p1, GLColour(s2), vector2df(uv.x, uv.y), n2, GLColour(r2, g2, b2, sky2)),
								GLVertex(p4, GLColour(s3), vector2df(uv.x, uv.w), n2, GLColour(r3, g3, b3, sky3)),
								GLVertex(p3, GLColour(s4), vector2df(uv.z, uv.w), n2, GLColour(r4, g4, b4, sky4)));
					}
					/*AddQuad(GLVertex(vector3df((i + 1) * BLOCK_SIZE, (k + 0) * BLOCK_SIZE, (j + 1) * BLOCK_SIZE), GLColour(s3), vector2df(uv.x, uv.y), GLNormal(0, -1, 0), GLColour(r3, g3, b3, sky3)),
					GLVertex(vector3df((i + 0) * BLOCK_SIZE, (k + 0) * BLOCK_SIZE, (j + 1) * BLOCK_SIZE), GLColour(s4), vector2df(uv.z, uv.y), GLNormal(0, -1, 0), GLColour(r4, g4, b4, sky4)),
					GLVertex(vector3df((i + 0) * BLOCK_SIZE, (k + 0) * BLOCK_SIZE, (j + 0) * BLOCK_SIZE), GLColour(s1), vector2df(uv.z, uv.w), GLNormal(0, -1, 0), GLColour(r1, g1, b1, sky1)),
					GLVertex(vector3df((i + 1) * BLOCK_SIZE, (k + 0) * BLOCK_SIZE, (j + 0) * BLOCK_SIZE), GLColour(s2), vector2df(uv.x, uv.w), GLNormal(0, -1, 0), GLColour(r2, g2, b2, sky2)));*/
				}
				oblock = _GetBlock(i, k + 1, j);
				if (_BlockIsEmpty(i, k + 1, j) || (block.XX || block.XY || block.YX || block.YY) || oblock.Half)
				{
					// get the light
					//_MergeLighting(i, k + 1, j, col);

					bool ao00, ao10, ao11, ao01;
					ao00 = (!block.XX && ((blocks[4].Solid && !blocks[4].Half) || (blocks[3].Solid && !blocks[3].XY) || (blocks[1].Solid && !blocks[1].YX) || (blocks[0].Solid && !blocks[0].YY))) || (block.XX && ((blocks[12].Solid && !blocks[12].XY) || (blocks[10].Solid && !blocks[10].YX) || (blocks[9].Solid && !blocks[9].YY)));
					ao10 = (!block.XY && ((blocks[4].Solid && !blocks[4].Half) || (blocks[5].Solid && !blocks[5].XX) || (blocks[1].Solid && !blocks[1].YY) || (blocks[2].Solid && !blocks[2].YX))) || (block.XY && ((blocks[14].Solid && !blocks[14].XX) || (blocks[10].Solid && !blocks[10].YY) || (blocks[11].Solid && !blocks[11].YX)));
					ao11 = (!block.YY && ((blocks[4].Solid && !blocks[4].Half) || (blocks[5].Solid && !blocks[5].YX) || (blocks[7].Solid && !blocks[7].XY) || (blocks[8].Solid && !blocks[8].XX))) || (block.YY && ((blocks[14].Solid && !blocks[14].YX) || (blocks[16].Solid && !blocks[16].XY) || (blocks[17].Solid && !blocks[17].XX)));
					ao01 = (!block.YX && ((blocks[4].Solid && !blocks[4].Half) || (blocks[3].Solid && !blocks[3].YY) || (blocks[7].Solid && !blocks[7].XX) || (blocks[6].Solid && !blocks[6].XY))) || (block.YX && ((blocks[12].Solid && !blocks[12].YY) || (blocks[16].Solid && !blocks[16].XX) || (blocks[15].Solid && !blocks[15].XY)));
					GLint s1 = 255 - ao00 * 40;
					GLint s2 = 255 - ao10 * 40;
					GLint s3 = 255 - ao11 * 40;
					GLint s4 = 255 - ao01 * 40;

					GLubyte r1 = smooth_lighting(blocks[4 + block.XX * 9].LightRed, blocks[0 + block.XX * 9].LightRed, blocks[1 + block.XX * 9].LightRed, blocks[3 + block.XX * 9].LightRed);
					GLubyte r2 = smooth_lighting(blocks[4 + block.XY * 9].LightRed, blocks[1 + block.XY * 9].LightRed, blocks[2 + block.XY * 9].LightRed, blocks[5 + block.XY * 9].LightRed);
					GLubyte r3 = smooth_lighting(blocks[4 + block.YY * 9].LightRed, blocks[5 + block.YY * 9].LightRed, blocks[7 + block.YY * 9].LightRed, blocks[8 + block.YY * 9].LightRed);
					GLubyte r4 = smooth_lighting(blocks[4 + block.YX * 9].LightRed, blocks[3 + block.YX * 9].LightRed, blocks[6 + block.YX * 9].LightRed, blocks[7 + block.YX * 9].LightRed);
					GLubyte g1 = smooth_lighting(blocks[4 + block.XX * 9].LightGreen, blocks[0 + block.XX * 9].LightGreen, blocks[1 + block.XX * 9].LightGreen, blocks[3 + block.XX * 9].LightGreen);
					GLubyte g2 = smooth_lighting(blocks[4 + block.XY * 9].LightGreen, blocks[1 + block.XY * 9].LightGreen, blocks[2 + block.XY * 9].LightGreen, blocks[5 + block.XY * 9].LightGreen);
					GLubyte g3 = smooth_lighting(blocks[4 + block.YY * 9].LightGreen, blocks[5 + block.YY * 9].LightGreen, blocks[7 + block.YY * 9].LightGreen, blocks[8 + block.YY * 9].LightGreen);
					GLubyte g4 = smooth_lighting(blocks[4 + block.YX * 9].LightGreen, blocks[3 + block.YX * 9].LightGreen, blocks[6 + block.YX * 9].LightGreen, blocks[7 + block.YX * 9].LightGreen);
					GLubyte b1 = smooth_lighting(blocks[4 + block.XX * 9].LightBlue, blocks[0 + block.XX * 9].LightBlue, blocks[1 + block.XX * 9].LightBlue, blocks[3 + block.XX * 9].LightBlue);
					GLubyte b2 = smooth_lighting(blocks[4 + block.XY * 9].LightBlue, blocks[1 + block.XY * 9].LightBlue, blocks[2 + block.XY * 9].LightBlue, blocks[5 + block.XY * 9].LightBlue);
					GLubyte b3 = smooth_lighting(blocks[4 + block.YY * 9].LightBlue, blocks[5 + block.YY * 9].LightBlue, blocks[7 + block.YY * 9].LightBlue, blocks[8 + block.YY * 9].LightBlue);
					GLubyte b4 = smooth_lighting(blocks[4 + block.YX * 9].LightBlue, blocks[3 + block.YX * 9].LightBlue, blocks[6 + block.YX * 9].LightBlue, blocks[7 + block.YX * 9].LightBlue);
					GLubyte sky1 = smooth_lighting(blocks[4 + block.XX * 9].SkyLight, blocks[0 + block.XX * 9].SkyLight, blocks[1 + block.XX * 9].SkyLight, blocks[3 + block.XX * 9].SkyLight);
					GLubyte sky2 = smooth_lighting(blocks[4 + block.XY * 9].SkyLight, blocks[1 + block.XY * 9].SkyLight, blocks[2 + block.XY * 9].SkyLight, blocks[5 + block.XY * 9].SkyLight);
					GLubyte sky3 = smooth_lighting(blocks[4 + block.YY * 9].SkyLight, blocks[5 + block.YY * 9].SkyLight, blocks[7 + block.YY * 9].SkyLight, blocks[8 + block.YY * 9].SkyLight);
					GLubyte sky4 = smooth_lighting(blocks[4 + block.YX * 9].SkyLight, blocks[3 + block.YX * 9].SkyLight, blocks[6 + block.YX * 9].SkyLight, blocks[7 + block.YX * 9].SkyLight);

					// calculate the points
					vector3df p1((i + 1) * BLOCK_SIZE, (k + 1) * BLOCK_SIZE - block.XY * BLOCK_SIZE, (j + 0) * BLOCK_SIZE);
					vector3df p2((i + 0) * BLOCK_SIZE, (k + 1) * BLOCK_SIZE - block.XX * BLOCK_SIZE, (j + 0) * BLOCK_SIZE);
					vector3df p3((i + 0) * BLOCK_SIZE, (k + 1) * BLOCK_SIZE - block.YX * BLOCK_SIZE, (j + 1) * BLOCK_SIZE);
					vector3df p4((i + 1) * BLOCK_SIZE, (k + 1) * BLOCK_SIZE - block.YY * BLOCK_SIZE, (j + 1) * BLOCK_SIZE);

					// calculate the normals
					bool bFlipped = (block.XY || block.YX) || ((block.XX == block.XY == block.YX == block.YY) && ao00 + ao11 < ao10 + ao01);

					if (block.Half)
					{
						if (block.XX && block.YY && (block.XY || block.YX))
							bFlipped = true;
						else if (block.XY && block.YX && (block.XX || block.YY))
							bFlipped = false;
					}

					GLNormal n1, n2;
					uv = correctedUV[0];
					if (bFlipped)
					{
						n1 = -CalculateNormal(triangle3df(p2, p3, p4));
						n2 = -CalculateNormal(triangle3df(p4, p1, p2));
						int iHide = 0;

						if (block.Half)
						{
							if (block.XX && block.YY && (!block.XY && block.YX))
								iHide = 1;
							else if (block.XX && block.YY && (block.XY && !block.YX))
								iHide = 2;
						}

						if (iHide != 1)
							AddTriangle(GLVertex(p2, GLColour(s1), vector2df(uv.z, uv.y), n1, GLColour(r1, g1, b1, sky1)),
								GLVertex(p3, GLColour(s4), vector2df(uv.z, uv.w), n1, GLColour(r4, g4, b4, sky4)),
								GLVertex(p4, GLColour(s3), vector2df(uv.x, uv.w), n1, GLColour(r3, g3, b3, sky3)));

						if (iHide != 2)
							AddTriangle(GLVertex(p4, GLColour(s3), vector2df(uv.x, uv.w), n2, GLColour(r3, g3, b3, sky3)),
								GLVertex(p1, GLColour(s2), vector2df(uv.x, uv.y), n2, GLColour(r2, g2, b2, sky2)),
								GLVertex(p2, GLColour(s1), vector2df(uv.z, uv.y), n2, GLColour(r1, g1, b1, sky1)));
					}
					else
					{
						n1 = -CalculateNormal(triangle3df(p1, p2, p3));
						n2 = -CalculateNormal(triangle3df(p3, p4, p1));
						int iHide = 0;

						if (block.Half)
						{
							if (block.XY && block.YX && (block.XX && !block.YY))
								iHide = 1;
							else if (block.XY && block.YX && (!block.XX && block.YY))
								iHide = 2;
						}

						if (iHide != 1)
							AddTriangle(GLVertex(p1, GLColour(s2), vector2df(uv.x, uv.y), n1, GLColour(r2, g2, b2, sky2)),
								GLVertex(p2, GLColour(s1), vector2df(uv.z, uv.y), n1, GLColour(r1, g1, b1, sky1)),
								GLVertex(p3, GLColour(s4), vector2df(uv.z, uv.w), n1, GLColour(r4, g4, b4, sky4)));

						if (iHide != 2)
							AddTriangle(GLVertex(p3, GLColour(s4), vector2df(uv.z, uv.w), n2, GLColour(r4, g4, b4, sky4)),
								GLVertex(p4, GLColour(s3), vector2df(uv.x, uv.w), n2, GLColour(r3, g3, b3, sky3)),
								GLVertex(p1, GLColour(s2), vector2df(uv.x, uv.y), n2, GLColour(r2, g2, b2, sky2)));
					}

					// create the face
					/*AddQuad(GLVertex(p1, GLColour(s2), vector2df(uv.x, uv.y), n1, GLColour(r2, g2, b2, blocks[4].SkyLight * 17)),
					GLVertex(p2, GLColour(s1), vector2df(uv.z, uv.y), n2, GLColour(r1, g1, b1, blocks[4].SkyLight * 17)),
					GLVertex(p3, GLColour(s4), vector2df(uv.z, uv.w), n3, GLColour(r4, g4, b4, blocks[4].SkyLight * 17)),
					GLVertex(p4, GLColour(s3), vector2df(uv.x, uv.w), n4, GLColour(r3, g3, b3, blocks[4].SkyLight * 17)), bFlipped);*/

					// add grass on top
					if (block.BlockType == Item::GetBlockFromName("Grass") && (block.XX + block.XY + block.YX + block.YY == 0))
					{
						GLfloat _uv = 32.f / 1024.f;
						GLfloat height = 1.5f;
						AddQuad(GLVertex(vector3df((i + 1) * BLOCK_SIZE, (k + 1) * BLOCK_SIZE + block.XY * BLOCK_SIZE, (j + 1) * BLOCK_SIZE), GLColour(s3), vector2df(2.f * _uv, 3.f * _uv), GLNormal(0, 1, 0), GLColour(r3, g3, b3, sky3)),
							GLVertex(vector3df((i + 1) * BLOCK_SIZE, (k + height) * BLOCK_SIZE + block.XY * BLOCK_SIZE, (j + 1) * BLOCK_SIZE), GLColour(s3), vector2df(2.f * _uv, 2.f * _uv), GLNormal(0, 1, 0), GLColour(r3, g3, b3, sky3)),
							GLVertex(vector3df((i + 0) * BLOCK_SIZE, (k + height) * BLOCK_SIZE + block.XY * BLOCK_SIZE, (j + 0) * BLOCK_SIZE), GLColour(s1), vector2df(3.f * _uv, 2.f * _uv), GLNormal(0, 1, 0), GLColour(r1, g1, b1, sky1)),
							GLVertex(vector3df((i + 0) * BLOCK_SIZE, (k + 1) * BLOCK_SIZE + block.XY * BLOCK_SIZE, (j + 0) * BLOCK_SIZE), GLColour(s1), vector2df(3.f * _uv, 3.f * _uv), GLNormal(0, 1, 0), GLColour(r1, g1, b1, sky1)));
						AddQuad(GLVertex(vector3df((i + 1) * BLOCK_SIZE, (k + height) * BLOCK_SIZE + block.XY * BLOCK_SIZE, (j + 1) * BLOCK_SIZE), GLColour(s3), vector2df(2.f * _uv, 2.f * _uv), GLNormal(0, 1, 0), GLColour(r3, g3, b3, sky3)),
							GLVertex(vector3df((i + 1) * BLOCK_SIZE, (k + 1) * BLOCK_SIZE + block.XY * BLOCK_SIZE, (j + 1) * BLOCK_SIZE), GLColour(s3), vector2df(2.f * _uv, 3.f * _uv), GLNormal(0, 1, 0), GLColour(r3, g3, b3, sky3)),
							GLVertex(vector3df((i + 0) * BLOCK_SIZE, (k + 1) * BLOCK_SIZE + block.XY * BLOCK_SIZE, (j + 0) * BLOCK_SIZE), GLColour(s1), vector2df(3.f * _uv, 3.f * _uv), GLNormal(0, 1, 0), GLColour(r1, g1, b1, sky1)),
							GLVertex(vector3df((i + 0) * BLOCK_SIZE, (k + height) * BLOCK_SIZE + block.XY * BLOCK_SIZE, (j + 0) * BLOCK_SIZE), GLColour(s1), vector2df(3.f * _uv, 2.f * _uv), GLNormal(0, 1, 0), GLColour(r1, g1, b1, sky1)));

						AddQuad(GLVertex(vector3df((i + 0) * BLOCK_SIZE, (k + 1) * BLOCK_SIZE + block.XY * BLOCK_SIZE, (j + 1) * BLOCK_SIZE), GLColour(s4), vector2df(2.f * _uv, 3.f * _uv), GLNormal(0, 1, 0), GLColour(r4, g4, b4, sky4)),
							GLVertex(vector3df((i + 0) * BLOCK_SIZE, (k + height) * BLOCK_SIZE + block.XY * BLOCK_SIZE, (j + 1) * BLOCK_SIZE), GLColour(s4), vector2df(2.f * _uv, 2.f * _uv), GLNormal(0, 1, 0), GLColour(r4, g4, b4, sky4)),
							GLVertex(vector3df((i + 1) * BLOCK_SIZE, (k + height) * BLOCK_SIZE + block.XY * BLOCK_SIZE, (j + 0) * BLOCK_SIZE), GLColour(s2), vector2df(3.f * _uv, 2.f * _uv), GLNormal(0, 1, 0), GLColour(r2, g2, b2, sky2)),
							GLVertex(vector3df((i + 1) * BLOCK_SIZE, (k + 1) * BLOCK_SIZE + block.XY * BLOCK_SIZE, (j + 0) * BLOCK_SIZE), GLColour(s2), vector2df(3.f * _uv, 3.f * _uv), GLNormal(0, 1, 0), GLColour(r2, g2, b2, sky2)));
						AddQuad(GLVertex(vector3df((i + 0) * BLOCK_SIZE, (k + height) * BLOCK_SIZE + block.XY * BLOCK_SIZE, (j + 1) * BLOCK_SIZE), GLColour(s4), vector2df(2.f * _uv, 2.f * _uv), GLNormal(0, 1, 0), GLColour(r4, g4, b4, sky4)),
							GLVertex(vector3df((i + 0) * BLOCK_SIZE, (k + 1) * BLOCK_SIZE + block.XY * BLOCK_SIZE, (j + 1) * BLOCK_SIZE), GLColour(s4), vector2df(2.f * _uv, 3.f * _uv), GLNormal(0, 1, 0), GLColour(r4, g4, b4, sky4)),
							GLVertex(vector3df((i + 1) * BLOCK_SIZE, (k + 1) * BLOCK_SIZE + block.XY * BLOCK_SIZE, (j + 0) * BLOCK_SIZE), GLColour(s2), vector2df(3.f * _uv, 3.f * _uv), GLNormal(0, 1, 0), GLColour(r2, g2, b2, sky2)),
							GLVertex(vector3df((i + 1) * BLOCK_SIZE, (k + height) * BLOCK_SIZE + block.XY * BLOCK_SIZE, (j + 0) * BLOCK_SIZE), GLColour(s2), vector2df(3.f * _uv, 2.f * _uv), GLNormal(0, 1, 0), GLColour(r2, g2, b2, sky2)));
					}
				}

				// z
				oblock = _GetBlock(i, k, j - 1);
				bSpace = oblock.Ignored || !oblock.Solid;
				if (bSpace || oblock.YX || oblock.YY)
				{
					// get the light
					//_MergeLighting(i, k, j - 1, col);

					bool ao1 = ((blocks[19].Solid) || (blocks[20].Solid) || (blocks[11].Solid) || (blocks[10].Solid));
					bool ao2 = ((blocks[19].Solid) || (blocks[18].Solid) || (blocks[9].Solid) || (blocks[10].Solid));
					bool ao3 = ((blocks[1].Solid) || (blocks[0].Solid) || (blocks[9].Solid) || (blocks[10].Solid));
					bool ao4 = ((blocks[1].Solid) || (blocks[2].Solid) || (blocks[11].Solid) || (blocks[10].Solid));
					GLint s1 = 255 - ao1 * 40;
					GLint s2 = 255 - ao2 * 40;
					GLint s3 = 255 - ao3 * 40;
					GLint s4 = 255 - ao4 * 40;

					GLubyte r1 = smooth_lighting(blocks[10].LightRed, blocks[19].LightRed, blocks[20].LightRed, blocks[11].LightRed);
					GLubyte r2 = smooth_lighting(blocks[10].LightRed, blocks[19].LightRed, blocks[18].LightRed, blocks[9].LightRed);
					GLubyte r3 = smooth_lighting(blocks[10].LightRed, blocks[1].LightRed, blocks[0].LightRed, blocks[9].LightRed);
					GLubyte r4 = smooth_lighting(blocks[10].LightRed, blocks[1].LightRed, blocks[2].LightRed, blocks[11].LightRed);
					GLubyte g1 = smooth_lighting(blocks[10].LightGreen, blocks[19].LightGreen, blocks[20].LightGreen, blocks[11].LightGreen);
					GLubyte g2 = smooth_lighting(blocks[10].LightGreen, blocks[19].LightGreen, blocks[18].LightGreen, blocks[9].LightGreen);
					GLubyte g3 = smooth_lighting(blocks[10].LightGreen, blocks[1].LightGreen, blocks[0].LightGreen, blocks[9].LightGreen);
					GLubyte g4 = smooth_lighting(blocks[10].LightGreen, blocks[1].LightGreen, blocks[2].LightGreen, blocks[11].LightGreen);
					GLubyte b1 = smooth_lighting(blocks[10].LightBlue, blocks[19].LightBlue, blocks[20].LightBlue, blocks[11].LightBlue);
					GLubyte b2 = smooth_lighting(blocks[10].LightBlue, blocks[19].LightBlue, blocks[18].LightBlue, blocks[9].LightBlue);
					GLubyte b3 = smooth_lighting(blocks[10].LightBlue, blocks[1].LightBlue, blocks[0].LightBlue, blocks[9].LightBlue);
					GLubyte b4 = smooth_lighting(blocks[10].LightBlue, blocks[1].LightBlue, blocks[2].LightBlue, blocks[11].LightBlue);
					GLubyte sky1 = smooth_lighting(blocks[10].SkyLight, blocks[19].SkyLight, blocks[20].SkyLight, blocks[11].SkyLight);
					GLubyte sky2 = smooth_lighting(blocks[10].SkyLight, blocks[19].SkyLight, blocks[18].SkyLight, blocks[9].SkyLight);
					GLubyte sky3 = smooth_lighting(blocks[10].SkyLight, blocks[1].SkyLight, blocks[0].SkyLight, blocks[9].SkyLight);
					GLubyte sky4 = smooth_lighting(blocks[10].SkyLight, blocks[1].SkyLight, blocks[2].SkyLight, blocks[11].SkyLight);

					// create the face
					uv = bLeakedNegativeZ ? leakedUV[4] : correctedUV[4];

					if ((bSpace || oblock.YX || oblock.YY) && (!block.XX && block.XY))
					{
						//_MergeLighting(i, k + !bSpace, j - 1, col);

						AddTriangle(GLVertex(vector3df((i + 0) * BLOCK_SIZE, (k + 0) * BLOCK_SIZE, (j + 0) * BLOCK_SIZE), GLColour(s2), vector2df(uv.z, uv.w), GLNormal(0, 0, -1), GLColour(r2, g2, b2, sky2)),
							GLVertex(vector3df((i + 0) * BLOCK_SIZE, (k + 1) * BLOCK_SIZE, (j + 0) * BLOCK_SIZE), GLColour(s3), vector2df(uv.z, uv.y), GLNormal(0, 0, -1), GLColour(r3, g3, b3, sky3)),
							GLVertex(vector3df((i + 1) * BLOCK_SIZE, (k + 0) * BLOCK_SIZE, (j + 0) * BLOCK_SIZE), GLColour(s1), vector2df(uv.x, uv.y), GLNormal(0, 0, -1), GLColour(r1, g1, b1, sky1)));
					}
					else if ((bSpace || oblock.YX || oblock.YY) && (block.XX && !block.XY))
					{
						//_MergeLighting(i, k + !bSpace, j - 1, col);

						AddTriangle(GLVertex(vector3df((i + 1) * BLOCK_SIZE, (k + 0) * BLOCK_SIZE, (j + 0) * BLOCK_SIZE), GLColour(s1), vector2df(uv.x, uv.w), GLNormal(0, 0, -1), GLColour(r1, g1, b1, sky1)),
							GLVertex(vector3df((i + 0) * BLOCK_SIZE, (k + 0) * BLOCK_SIZE, (j + 0) * BLOCK_SIZE), GLColour(s2), vector2df(uv.z, uv.y), GLNormal(0, 0, -1), GLColour(r2, g2, b2, sky2)),
							GLVertex(vector3df((i + 1) * BLOCK_SIZE, (k + 1) * BLOCK_SIZE, (j + 0) * BLOCK_SIZE), GLColour(s4), vector2df(uv.x, uv.y), GLNormal(0, 0, -1), GLColour(r4, g4, b4, sky4)));
					}
					else if ((bSpace || (!bSpace && oblock.YX && oblock.YY)) && !block.XX && !block.XY)
					{
						//if (oblock.YX && oblock.YY)
						//	_MergeLighting(i, k + 1, j - 1, col);

						AddQuad(GLVertex(vector3df((i + 1) * BLOCK_SIZE, (k + 0) * BLOCK_SIZE, (j + 0) * BLOCK_SIZE), GLColour(s1), vector2df(uv.x, uv.y), GLNormal(0, 0, -1), GLColour(r1, g1, b1, sky1)),
							GLVertex(vector3df((i + 0) * BLOCK_SIZE, (k + 0) * BLOCK_SIZE, (j + 0) * BLOCK_SIZE), GLColour(s2), vector2df(uv.z, uv.y), GLNormal(0, 0, -1), GLColour(r2, g2, b2, sky2)),
							GLVertex(vector3df((i + 0) * BLOCK_SIZE, (k + 1) * BLOCK_SIZE, (j + 0) * BLOCK_SIZE), GLColour(s3), vector2df(uv.z, uv.w), GLNormal(0, 0, -1), GLColour(r3, g3, b3, sky3)),
							GLVertex(vector3df((i + 1) * BLOCK_SIZE, (k + 1) * BLOCK_SIZE, (j + 0) * BLOCK_SIZE), GLColour(s4), vector2df(uv.x, uv.w), GLNormal(0, 0, -1), GLColour(r4, g4, b4, sky4)));
					}
					else if (!block.XY && !oblock.YX && oblock.YY)
					{
						//_MergeLighting(i, k + 1, j - 1, col);

						AddTriangle(GLVertex(vector3df((i + 1) * BLOCK_SIZE, (k + 0) * BLOCK_SIZE, (j + 0) * BLOCK_SIZE), GLColour(s1), vector2df(uv.x, uv.w), GLNormal(0, 0, -1), GLColour(r1, g1, b1, sky1)),
							GLVertex(vector3df((i + 0) * BLOCK_SIZE, (k + 1) * BLOCK_SIZE, (j + 0) * BLOCK_SIZE), GLColour(s3), vector2df(uv.z, uv.y), GLNormal(0, 0, -1), GLColour(r3, g3, b3, sky3)),
							GLVertex(vector3df((i + 1) * BLOCK_SIZE, (k + 1) * BLOCK_SIZE, (j + 0) * BLOCK_SIZE), GLColour(s4), vector2df(uv.x, uv.y), GLNormal(0, 0, -1), GLColour(r4, g4, b4, sky4)));
					}
					else if (!block.XX && oblock.YX && !oblock.YY)
					{
						//_MergeLighting(i, k + 1, j - 1, col);

						AddTriangle(GLVertex(vector3df((i + 0) * BLOCK_SIZE, (k + 0) * BLOCK_SIZE, (j + 0) * BLOCK_SIZE), GLColour(s2), vector2df(uv.z, uv.w), GLNormal(0, 0, -1), GLColour(r2, g2, b2, sky2)),
							GLVertex(vector3df((i + 0) * BLOCK_SIZE, (k + 1) * BLOCK_SIZE, (j + 0) * BLOCK_SIZE), GLColour(s3), vector2df(uv.z, uv.y), GLNormal(0, 0, -1), GLColour(r3, g3, b3, sky3)),
							GLVertex(vector3df((i + 1) * BLOCK_SIZE, (k + 1) * BLOCK_SIZE, (j + 0) * BLOCK_SIZE), GLColour(s4), vector2df(uv.x, uv.y), GLNormal(0, 0, -1), GLColour(r4, g4, b4, sky4)));
					}
				}
				oblock = _GetBlock(i, k, j + 1);
				bSpace = oblock.Ignored || !oblock.Solid;
				if (bSpace || oblock.XX || oblock.XY)
				{
					// get the light
					//_MergeLighting(i, k, j + 1, col);

					bool ao1 = ((blocks[25].Solid) || (blocks[26].Solid) || (blocks[17].Solid) || (blocks[16].Solid));
					bool ao2 = ((blocks[25].Solid) || (blocks[24].Solid) || (blocks[15].Solid) || (blocks[16].Solid));
					bool ao3 = ((blocks[7].Solid) || (blocks[6].Solid) || (blocks[15].Solid) || (blocks[16].Solid));
					bool ao4 = ((blocks[7].Solid) || (blocks[8].Solid) || (blocks[17].Solid) || (blocks[16].Solid));
					GLint s1 = 255 - ao1 * 40;
					GLint s2 = 255 - ao2 * 40;
					GLint s3 = 255 - ao3 * 40;
					GLint s4 = 255 - ao4 * 40;

					GLubyte r1 = smooth_lighting(blocks[16].LightRed, blocks[25].LightRed, blocks[26].LightRed, blocks[17].LightRed);
					GLubyte r2 = smooth_lighting(blocks[16].LightRed, blocks[25].LightRed, blocks[24].LightRed, blocks[15].LightRed);
					GLubyte r3 = smooth_lighting(blocks[16].LightRed, blocks[7].LightRed, blocks[6].LightRed, blocks[15].LightRed);
					GLubyte r4 = smooth_lighting(blocks[16].LightRed, blocks[7].LightRed, blocks[8].LightRed, blocks[17].LightRed);
					GLubyte g1 = smooth_lighting(blocks[16].LightGreen, blocks[25].LightGreen, blocks[26].LightGreen, blocks[17].LightGreen);
					GLubyte g2 = smooth_lighting(blocks[16].LightGreen, blocks[25].LightGreen, blocks[24].LightGreen, blocks[15].LightGreen);
					GLubyte g3 = smooth_lighting(blocks[16].LightGreen, blocks[7].LightGreen, blocks[6].LightGreen, blocks[15].LightGreen);
					GLubyte g4 = smooth_lighting(blocks[16].LightGreen, blocks[7].LightGreen, blocks[8].LightGreen, blocks[17].LightGreen);
					GLubyte b1 = smooth_lighting(blocks[16].LightBlue, blocks[25].LightBlue, blocks[26].LightBlue, blocks[17].LightBlue);
					GLubyte b2 = smooth_lighting(blocks[16].LightBlue, blocks[25].LightBlue, blocks[24].LightBlue, blocks[15].LightBlue);
					GLubyte b3 = smooth_lighting(blocks[16].LightBlue, blocks[7].LightBlue, blocks[6].LightBlue, blocks[15].LightBlue);
					GLubyte b4 = smooth_lighting(blocks[16].LightBlue, blocks[7].LightBlue, blocks[8].LightBlue, blocks[17].LightBlue);
					GLubyte sky1 = smooth_lighting(blocks[16].SkyLight, blocks[25].SkyLight, blocks[26].SkyLight, blocks[17].SkyLight);
					GLubyte sky2 = smooth_lighting(blocks[16].SkyLight, blocks[25].SkyLight, blocks[24].SkyLight, blocks[15].SkyLight);
					GLubyte sky3 = smooth_lighting(blocks[16].SkyLight, blocks[7].SkyLight, blocks[6].SkyLight, blocks[15].SkyLight);
					GLubyte sky4 = smooth_lighting(blocks[16].SkyLight, blocks[7].SkyLight, blocks[8].SkyLight, blocks[17].SkyLight);

					// create the face
					uv = bLeakedPositiveZ ? leakedUV[5] : correctedUV[5];

					if ((bSpace || oblock.XX || oblock.XY) && (!block.YX && block.YY))
					{
						//_MergeLighting(i, k + !bSpace, j + 1, col);

						AddTriangle(GLVertex(vector3df((i + 1) * BLOCK_SIZE, (k + 0) * BLOCK_SIZE, (j + 1) * BLOCK_SIZE), GLColour(s1), vector2df(uv.z, uv.y), GLNormal(0, 0, 1), GLColour(r1, g1, b1, sky1)),
							GLVertex(vector3df((i + 0) * BLOCK_SIZE, (k + 1) * BLOCK_SIZE, (j + 1) * BLOCK_SIZE), GLColour(s3), vector2df(uv.x, uv.y), GLNormal(0, 0, 1), GLColour(r3, g3, b3, sky3)),
							GLVertex(vector3df((i + 0) * BLOCK_SIZE, (k + 0) * BLOCK_SIZE, (j + 1) * BLOCK_SIZE), GLColour(s2), vector2df(uv.x, uv.w), GLNormal(0, 0, 1), GLColour(r2, g2, b2, sky2)));
					}
					else if ((bSpace || oblock.XX || oblock.XY) && (block.YX && !block.YY))
					{
						//_MergeLighting(i, k + !bSpace, j + 1, col);

						AddTriangle(GLVertex(vector3df((i + 1) * BLOCK_SIZE, (k + 0) * BLOCK_SIZE, (j + 1) * BLOCK_SIZE), GLColour(s1), vector2df(uv.z, uv.w), GLNormal(0, 0, 1), GLColour(r1, g1, b1, sky1)),
							GLVertex(vector3df((i + 1) * BLOCK_SIZE, (k + 1) * BLOCK_SIZE, (j + 1) * BLOCK_SIZE), GLColour(s4), vector2df(uv.z, uv.y), GLNormal(0, 0, 1), GLColour(r4, g4, b4, sky4)),
							GLVertex(vector3df((i + 0) * BLOCK_SIZE, (k + 0) * BLOCK_SIZE, (j + 1) * BLOCK_SIZE), GLColour(s2), vector2df(uv.x, uv.y), GLNormal(0, 0, 1), GLColour(r2, g2, b2, sky2)));
					}
					else if ((bSpace || (!bSpace && oblock.XX && oblock.XY)) && !block.YX && !block.YY)
					{
						//if (oblock.XX && oblock.XY)
						//	_MergeLighting(i, k + 1, j + 1, col);

						AddQuad(GLVertex(vector3df((i + 0) * BLOCK_SIZE, (k + 0) * BLOCK_SIZE, (j + 1) * BLOCK_SIZE), GLColour(s2), vector2df(uv.x, uv.y), GLNormal(0, 0, 1), GLColour(r2, g2, b2, sky2)),
							GLVertex(vector3df((i + 1) * BLOCK_SIZE, (k + 0) * BLOCK_SIZE, (j + 1) * BLOCK_SIZE), GLColour(s1), vector2df(uv.z, uv.y), GLNormal(0, 0, 1), GLColour(r1, g1, b1, sky1)),
							GLVertex(vector3df((i + 1) * BLOCK_SIZE, (k + 1) * BLOCK_SIZE, (j + 1) * BLOCK_SIZE), GLColour(s4), vector2df(uv.z, uv.w), GLNormal(0, 0, 1), GLColour(r4, g4, b4, sky4)),
							GLVertex(vector3df((i + 0) * BLOCK_SIZE, (k + 1) * BLOCK_SIZE, (j + 1) * BLOCK_SIZE), GLColour(s3), vector2df(uv.x, uv.w), GLNormal(0, 0, 1), GLColour(r3, g3, b3, sky3)));
					}
					else if (!block.YY && !oblock.XX && oblock.XY)
					{
						//_MergeLighting(i, k + 1, j + 1, col);

						AddTriangle(GLVertex(vector3df((i + 1) * BLOCK_SIZE, (k + 0) * BLOCK_SIZE, (j + 1) * BLOCK_SIZE), GLColour(s1), vector2df(uv.z, uv.w), GLNormal(0, 0, 1), GLColour(r1, g1, b1, sky1)),
							GLVertex(vector3df((i + 1) * BLOCK_SIZE, (k + 1) * BLOCK_SIZE, (j + 1) * BLOCK_SIZE), GLColour(s4), vector2df(uv.z, uv.y), GLNormal(0, 0, 1), GLColour(r4, g4, b4, sky4)),
							GLVertex(vector3df((i + 0) * BLOCK_SIZE, (k + 1) * BLOCK_SIZE, (j + 1) * BLOCK_SIZE), GLColour(s3), vector2df(uv.x, uv.y), GLNormal(0, 0, 1), GLColour(r3, g3, b3, sky3)));
					}
					else if (!block.YX && oblock.XX && !oblock.XY)
					{
						//_MergeLighting(i, k + 1, j + 1, col);

						AddTriangle(GLVertex(vector3df((i + 1) * BLOCK_SIZE, (k + 1) * BLOCK_SIZE, (j + 1) * BLOCK_SIZE), GLColour(s4), vector2df(uv.z, uv.y), GLNormal(0, 0, 1), GLColour(r4, g4, b4, sky4)),
							GLVertex(vector3df((i + 0) * BLOCK_SIZE, (k + 1) * BLOCK_SIZE, (j + 1) * BLOCK_SIZE), GLColour(s3), vector2df(uv.x, uv.y), GLNormal(0, 0, 1), GLColour(r3, g3, b3, sky3)),
							GLVertex(vector3df((i + 0) * BLOCK_SIZE, (k + 0) * BLOCK_SIZE, (j + 1) * BLOCK_SIZE), GLColour(s2), vector2df(uv.x, uv.w), GLNormal(0, 0, 1), GLColour(r2, g2, b2, sky2)));
					}
				}
			}
		}
	}

	// mark meshes as dirty
	it = m_BlockMeshes.begin();
	while (it != m_BlockMeshes.end())
	{
		it->second->MarkAsUncreated();

		++it;
	}
	m_Mutex.lock();
	m_bBlockMeshesCreated = true;

	m_bMeshCreated = true;
	m_bBuffersCreated = false;
	m_Mutex.unlock();

	//cout << "Created chunk mesh at " << m_ChunkPos.x << ", " << m_ChunkPos.y << ", " << m_ChunkPos.z << endl;
}

void TerrainChunk::ClearMesh()
{
	if (!m_bMeshCreated)
		return;

	m_Mutex.lock();
	m_bMeshCreated = false;
	ClearVectors();
	m_Mutex.unlock();
}

bool TerrainChunk::DoesRayCollide(line3df& l, vector3df& oo, GLfloat& out)
{
	// firstly check whether the ray collides with our big bounding box
	if (RayCollisionAABB(l, GetPosition(), GetPosition() + vector3df(BLOCK_SIZE * CHUNK_SIZE), oo, out))
	{
		// loop through each block and see if the ray collides with it
		for (GLuint i = 0; i < CHUNK_SIZE; ++i) {
			for (GLuint j = 0; j < CHUNK_SIZE; ++j) {
				for (GLuint k = CHUNK_SIZE - 1; k > 0; --k) {
					// we start at the highest block as the camera is facing downwards towards the terrain
					if (GetBlock(i, k, j) == 0)
						continue;

					if (RayCollisionAABB(l, GetPosition() + vector3df(i * BLOCK_SIZE, k * BLOCK_SIZE, j * BLOCK_SIZE),
						GetPosition() + vector3df((i + 1) * BLOCK_SIZE, (k + 1) * BLOCK_SIZE, (j + 1) * BLOCK_SIZE), oo, out))
						return true;
				}
			}
		}
	}

	return false;
}

bool TerrainChunk::IsCreated()
{
	return m_bMeshCreated;
}

bool TerrainChunk::IsCreating()
{
	return m_bCreating;
}

void TerrainChunk::SetCreating(bool set)
{
	m_Mutex.lock();
	m_bCreating = set;
	m_Mutex.unlock();
}

bool TerrainChunk::IsInitialised()
{
	return m_bInitialised;
}

bool TerrainChunk::IsWaterInitialised()
{
	return m_bWaterInitialised;
}

vector3di TerrainChunk::GetChunkPos()
{
	return m_ChunkPos;
}

TerrainBlock* TerrainChunk::ArrayPointer()
{
	return m_Terrain;
}

TerrainBlock TerrainChunk::GetBlock(GLint x, GLint y, GLint z) const
{
	// if we are out of range for whatever reason
	if (x < 0 || x >= CHUNK_SIZE || y < 0 || y >= CHUNK_SIZE || z < 0 || z >= CHUNK_SIZE)
	{
		static TerrainBlock block(0, 0, 0, 0, 0, m_ChunkPos.y >= 0, 0, 0, 0, 0, 0, 0, 0, 15);
		return block;
	}

	// return the block
	return m_Terrain[x * CHUNK_SIZE * CHUNK_SIZE + z * CHUNK_SIZE + y];
}

TerrainBlock TerrainChunk::GetBlock(const vector3di& pos) const
{
	return GetBlock(pos.x, pos.y, pos.z);
}

TerrainBlock& TerrainChunk::BlockAt(const vector3di& pos)
{
	return m_Terrain[pos.x * CHUNK_SIZE * CHUNK_SIZE + pos.z * CHUNK_SIZE + pos.y];
}

void TerrainChunk::SetBlock(GLint x, GLint y, GLint z, TerrainBlock type)
{
	// if for whatever reason we're out of our range, quit
	if (x < 0 || x >= CHUNK_SIZE || y < 0 || y >= CHUNK_SIZE || z < 0 || z >= CHUNK_SIZE)
		return;

	// check whether this block can be ignored
	const ItemClass* ic = Item::GetItemClass(type);
	if (type.BlockType == 0 || (ic && (ic->bBlockHasMesh || ic->sValues[3] || ic->BlockSize != vector3di(1, 1, 1))))
		type.Ignored = 1;
	else
		type.Ignored = 0;

	m_Terrain[x * CHUNK_SIZE * CHUNK_SIZE + z * CHUNK_SIZE + y] = type;

	// if we are removing water, remove water
	if (type.Water == 0)
	{
		vector3di pos(x, y, z);

		SetWater(pos, 0.f);
		SetNewWater(pos, 0.f);

		/*std::list<vector3di>::iterator it = std::find(m_WaterList.begin(), m_WaterList.end(), pos);

		if (it != m_WaterList.end())
		m_WaterList.erase(it);*/
	}

	// set the water settled flag to false so we can see if we need to update water
	m_bWaterSettled = false;

	// if there is an animated object here, delete it
	map<vector3di, AnimatedBlock>::iterator it = m_AnimatedBlocks.find(vector3di(x, y, z));
	if (it != m_AnimatedBlocks.end())
	{
		if (!GetSceneManager()->DeleteObject(it->second.obj))
			delete it->second.obj;
		delete it->second.anim;

		//cout << "Deleted " << it->second.obj << " at " << x << ", " << y << ", " << z << endl;

		m_AnimatedBlocks.erase(it);
	}

	if (type.BlockType && ic && ic->bAnimated && !type.Multi)
	{
		GLObject* o = new GLObject(GetSceneManager(), vector3df(m_ChunkPos.x * CHUNK_SIZE * BLOCK_SIZE, m_ChunkPos.y * CHUNK_SIZE * BLOCK_SIZE, m_ChunkPos.z * CHUNK_SIZE * BLOCK_SIZE) +
			vector3df(x * BLOCK_SIZE, y * BLOCK_SIZE, z * BLOCK_SIZE) +
			vector3df(BLOCK_SIZE * 0.5f, 0, BLOCK_SIZE * 0.5f), vector3di(1));
		o->SetAABB(vector3df(), vector3df(ic->BlockSize.x * BLOCK_SIZE, ic->BlockSize.y * BLOCK_SIZE, ic->BlockSize.z * BLOCK_SIZE));
		o->SetMesh(ic->Meshes[0]);
		o->SetRotation(vector3df(0, RotateBlockMesh(type), 0), 1);
		o->SetTexture(ic->strTexturePath.c_str());
		Animator* a = new Animator(ic->Meshes[0]);
		GetSceneManager()->AddGLObject(o);

		AnimatedBlock block;
		block.obj = o;
		block.anim = a;
		block.opening = false;
		m_AnimatedBlocks.emplace(vector3di(x, y, z), block);

		//cout << m_AnimatedBlocks.size() << " -> " << o << " at " << x << ", " << y << ", " << z << endl;

		// based on what type of animated object this is, set up the animation
		if (ic->sValues[5] > 0)
		{
			block.anim->SetAnimation("Open");
			block.anim->SetAnimationLoop(false);
			block.anim->SetAnimationSpeed(2);
			block.obj->SetFrameAtTime(block.anim, 0);

			// if this block is not solid, open it
			if (!type.Solid)
				block.anim->Start();
		}
	}
}

void TerrainChunk::SetBlock(vector3di pos, TerrainBlock type)
{
	SetBlock(pos.x, pos.y, pos.z, type);
}

void TerrainChunk::DestroyBlock(GLint x, GLint y, GLint z)
{
	// make sure the values passed to us are in range
	Limit(x, 0, CHUNK_SIZE - 1);
	Limit(y, 0, CHUNK_SIZE - 1);
	Limit(z, 0, CHUNK_SIZE - 1);

	m_Terrain[x * CHUNK_SIZE * CHUNK_SIZE + z * CHUNK_SIZE + y] = 0;
}

void TerrainChunk::Interact(vector3di pos, bool open)
{
	// if there's no animated block here, return
	if (m_AnimatedBlocks.find(pos) == m_AnimatedBlocks.end())
		return;

	AnimatedBlock& b = m_AnimatedBlocks.at(pos);
	if (open)
	{
		b.opening = true;
		b.anim->SetAnimation("Open");
		b.anim->Start();
	}
	else
	{
		b.opening = false;
		b.anim->SetAnimation("Close");
		b.anim->Start();
	}
}

void TerrainChunk::SortParticleEmitter(GLint x, GLint y, GLint z, TerrainBlock b)
{
	// check if there are any particle emitters to created
	const ItemClass *ic = Item::GetItemClass(b);
	if (ic && ic->Particles.empty())
		return;

	// if there is no particle emitter array at this point and we are removing the block we can safely exit
	vector3di pos(x, y, z);
	vector<ParticleEmitter*>* p = NULL;
	map<vector3di, vector<ParticleEmitter*>>::iterator it = m_BlockParticleEmitters.find(pos);
	if (!ic && it == m_BlockParticleEmitters.end())
		return;

	// if there is no particle emitter array at this point and we're adding to it, create one here
	if (it == m_BlockParticleEmitters.end())
	{
		// ic will NOT be null at this point, it is impossible
		m_BlockParticleEmitters.emplace(pos, vector<ParticleEmitter*>());
	}

	// get the particle emitter at this point
	p = &m_BlockParticleEmitters.at(pos);

	SceneManager* pSceneMan = GetSceneManager();
	if (ic == NULL)
	{
		// if ic is null then we are deleting this block, see if we need to delete any particles
		for (GLuint i = 0; i < p->size(); ++i) {
			if (!pSceneMan->DeleteObject(p->at(i)))
				delete p->at(i); // this check really shouldn't fail but just to be sure, we have this catch
		}
		p->clear();
	}
	else
	{
		// if ic is not null then we are placing a block, see if we need to create any particles
		vector3df pos((GetChunkPos().x * CHUNK_SIZE + x) * BLOCK_SIZE, (GetChunkPos().y * CHUNK_SIZE + y) * BLOCK_SIZE, (GetChunkPos().z * CHUNK_SIZE + z) * BLOCK_SIZE);
		for (GLuint i = 0; i < ic->Particles.size(); ++i) {
			ParticleEmitter* pe = pSceneMan->CreateParticleEmitter(ic->Particles[i]);
			//pe->SetTexture(ic->Particles[i].texture.c_str());
			pe->SetTextureToLoad(ic->Particles[i].texture);
			pe->TranslateEmitterPosition(pos);

			for (GLuint j = 0; j < ic->Particles[i].anims.size(); ++j) {
				pe->AddAnimation(ic->Particles[i].anims[j].first, ic->Particles[i].anims[j].second);
			}

			p->push_back(pe);
		}
	}
}

void TerrainChunk::SetBlockLight(const vector3di& pos, GLubyte red, GLuint green, GLuint blue)
{
	m_Terrain[pos.x * CHUNK_SIZE * CHUNK_SIZE + pos.z * CHUNK_SIZE + pos.y].LightRed = red;
	m_Terrain[pos.x * CHUNK_SIZE * CHUNK_SIZE + pos.z * CHUNK_SIZE + pos.y].LightGreen = green;
	m_Terrain[pos.x * CHUNK_SIZE * CHUNK_SIZE + pos.z * CHUNK_SIZE + pos.y].LightBlue = blue;
}

void TerrainChunk::SetSunLight(const vector3di& pos, GLubyte val)
{
	m_Terrain[pos.x * CHUNK_SIZE * CHUNK_SIZE + pos.z * CHUNK_SIZE + pos.y].SkyLight = val;
}

void TerrainChunk::AddToMultiBlockList(vector3di block, std::list<vector3di>* list)
{
	m_MultiBlockLists[block] = list;
}

void TerrainChunk::RemoveFromMultiBlockList(vector3di block)
{
	map<vector3di, std::list<vector3di>*>::iterator it = m_MultiBlockLists.find(block);

	if (it != m_MultiBlockLists.end())
	{
		m_MultiBlockLists.erase(it);
	}
}

std::list<vector3di>* TerrainChunk::GetMultiBlockList(vector3di block)
{
	if (m_MultiBlockLists.find(block) != m_MultiBlockLists.end())
		return m_MultiBlockLists[block];

	return NULL;
}

void TerrainChunk::DoSunlight(vector<GLfloat> noise)
{
	m_bSunlightCreated = true;

	auto noise_height = [&](int x, int z) -> GLfloat {
		return noise[CHUNK_SIZE + 3 + (x * (CHUNK_SIZE + 2) + z)];
	};

	GLint y = GetChunkPos().y * CHUNK_SIZE;
	GLfloat x = GetChunkPos().x * CHUNK_SIZE * BLOCK_SIZE;
	GLfloat z = GetChunkPos().z * CHUNK_SIZE * BLOCK_SIZE;
	for (GLuint i = 0; i < CHUNK_SIZE; ++i) {
		for (GLuint j = 0; j < CHUNK_SIZE; ++j) {
			if (y + CHUNK_SIZE - 1 > noise_height(i, j))
				m_pTerrain->SetSunLight(vector3df(x + i * BLOCK_SIZE, (y + CHUNK_SIZE - 1) * BLOCK_SIZE, z + j * BLOCK_SIZE), 15, false);
		}
	}
	m_pTerrain->DoSunlightUpdate();
}

bool TerrainChunk::IsSunlightDone()
{
	return m_bSunlightCreated;
}

void TerrainChunk::SetWater(const vector3di& pos, GLfloat water)
{
	Limit(water, 0.f, 1.f);
	m_CurrentWater[pos.x * CHUNK_SIZE * CHUNK_SIZE + pos.z * CHUNK_SIZE + pos.y] = water * 250.f;
}

void TerrainChunk::SetNewWater(const vector3di& pos, GLfloat water)
{
	Limit(water, 0.f, 1.f);
	m_NewWater[pos.x * CHUNK_SIZE * CHUNK_SIZE + pos.z * CHUNK_SIZE + pos.y] = water * 250.f;
}

GLfloat TerrainChunk::GetWater(const vector3di& pos) const
{
	return m_Terrain[pos.x * CHUNK_SIZE * CHUNK_SIZE + pos.z * CHUNK_SIZE + pos.y].Water > 0 ? m_CurrentWater[pos.x * CHUNK_SIZE * CHUNK_SIZE + pos.z * CHUNK_SIZE + pos.y] * 0.004f: 0.f;
}

GLfloat TerrainChunk::GetNewWater(const vector3di& pos) const
{
	return m_NewWater[pos.x * CHUNK_SIZE * CHUNK_SIZE + pos.z * CHUNK_SIZE + pos.y] * 0.004f;
}

void TerrainChunk::AddWater(const vector3di& pos, GLfloat water)
{
	/*NewWater node;
	node.water = water;
	node.pos = pos;

	m_WaterToAdd.push_back(node);*/
	BlockAt(pos).Water = 1;

	SetWater(pos, water);
	SetNewWater(pos, water);

	m_bWaterSettled = false;
	m_bHasWater = true;
	m_bWaterInitialised = true;
}

/*void TerrainChunk::SortIndices(GLubyte dir)
{
// sort the indices of this terrain based on camera direction
vector<GLuint> tmp;
vector<GLuint> inds = GetIndices();
vector<GLVertex> verts = GetVertices();
bool(*cmp)(vector3df, vector3df) = [](vector3df one, vector3df two) -> bool {
return (one.z < two.z && one.y < two.y);
};

// switch on dir
{

}

// sort the indices
for (GLuint i = 0; i < inds.size(); i += 6) {
vector3df vs[4] = { vector3df(), vector3df(), vector3df(), vector3df() };

if (tmp.size() == 0)
{
tmp.push_back(inds[i + 0]); tmp.push_back(inds[i + 1]); tmp.push_back(inds[i + 2]);
tmp.push_back(inds[i + 3]); tmp.push_back(inds[i + 4]); tmp.push_back(inds[i + 5]);
}
else
{
vs[0] = verts[inds[i]].pos;
for (int j = 1; j < 6; j++) {
bool bAdd = true;
for (int k = min(j, 3); k > 0; k--) {
if (vs[k] == verts[inds[i + j]].pos)
bAdd = false;
}

if (bAdd)
vs[min(j, 3)] = verts[inds[j]].pos;
}

vector3df point = vector3df((min(vs[0].x, vs[1].x, vs[2].x, vs[3].x) + max(vs[0].x, vs[1].x, vs[2].x, vs[3].x)) / 2,
(min(vs[0].y, vs[1].y, vs[2].y, vs[3].y) + max(vs[0].y, vs[1].y, vs[2].y, vs[3].y)) / 2,
(min(vs[0].z, vs[1].z, vs[2].z, vs[3].z) + max(vs[0].z, vs[1].z, vs[2].z, vs[3].z)) / 2);

// this code is horrible :(
bool bAdded = false;
for (GLuint j = 0; j < tmp.size(); j += 6) {
vs[0] = verts[inds[j]].pos;
for (int k = 1; k < 6; k++) {
bool bAdd = true;
for (int h = min(k, 3); h > 0; h--) {
if (vs[h] == verts[inds[j + k]].pos)
bAdd = false;
}

if (bAdd)
vs[min(k, 3)] = verts[inds[j]].pos;
}

vector3df two = vector3df((min(vs[0].x, vs[1].x, vs[2].x, vs[3].x) + max(vs[0].x, vs[1].x, vs[2].x, vs[3].x)) / 2,
(min(vs[0].y, vs[1].y, vs[2].y, vs[3].y) + max(vs[0].y, vs[1].y, vs[2].y, vs[3].y)) / 2,
(min(vs[0].z, vs[1].z, vs[2].z, vs[3].z) + max(vs[0].z, vs[1].z, vs[2].z, vs[3].z)) / 2);

if (cmp(point, two))
{
bAdded = true;

tmp.insert(std::find(tmp.begin(), tmp.end(), inds[j]), inds[i], 6);

break;
}
}
if (!bAdded)
{
tmp.push_back(inds[i + 0]); tmp.push_back(inds[i + 1]); tmp.push_back(inds[i + 2]);
tmp.push_back(inds[i + 3]); tmp.push_back(inds[i + 4]); tmp.push_back(inds[i + 5]);
}
}
}

SetIndices(tmp);
SetIndicesUnsorted();
}*/

TerrainBlock TerrainChunk::_GetBlock(GLint x, GLint y, GLint z) const
{
	vector3di pos = m_ChunkPos;
	bool bThis = true;

	if (x < 0)
	{
		x += CHUNK_SIZE;
		--pos.x;
		bThis = false;
	}
	else if (x >= CHUNK_SIZE)
	{
		x = 0;
		++pos.x;
		bThis = false;
	}

	if (y < 0)
	{
		y += CHUNK_SIZE;
		--pos.y;
		bThis = false;
	}
	else if (y >= CHUNK_SIZE)
	{
		y = 0;
		++pos.y;
		bThis = false;
	}

	if (z < 0)
	{
		z += CHUNK_SIZE;
		--pos.z;
		bThis = false;
	}
	else if (z >= CHUNK_SIZE)
	{
		z = 0;
		++pos.z;
		bThis = false;
	}

	if (bThis)
		return m_Terrain[x * CHUNK_SIZE * CHUNK_SIZE + z * CHUNK_SIZE + y];
	else
		return m_pTerrain->_GetBlock(x, y, z, pos);
}

GLfloat TerrainChunk::_GetWater(GLint x, GLint y, GLint z) const
{
	vector3di pos = m_ChunkPos;
	bool bThis = true;

	if (x < 0)
	{
		x += CHUNK_SIZE;
		--pos.x;
		bThis = false;
	}
	else if (x >= CHUNK_SIZE)
	{
		x = 0;
		++pos.x;
		bThis = false;
	}

	if (y < 0)
	{
		y += CHUNK_SIZE;
		--pos.y;
		bThis = false;
	}
	else if (y >= CHUNK_SIZE)
	{
		y = 0;
		++pos.y;
		bThis = false;
	}

	if (z < 0)
	{
		z += CHUNK_SIZE;
		--pos.z;
		bThis = false;
	}
	else if (z >= CHUNK_SIZE)
	{
		z = 0;
		++pos.z;
		bThis = false;
	}

	if (bThis)
		return m_Terrain[x * CHUNK_SIZE * CHUNK_SIZE + z * CHUNK_SIZE + y].Water > 0 ? m_CurrentWater[x * CHUNK_SIZE * CHUNK_SIZE + z * CHUNK_SIZE + y] : 0.f;
	else
		return m_pTerrain->_GetWater(x, y, z, pos);
}

bool TerrainChunk::_BlockIsEmpty(GLint x, GLint y, GLint z) const
{
	TerrainBlock block = _GetBlock(x, y, z);
	//const ItemClass* ic = Item::GetItemClass(block);
	return (block.Ignored || !block.Solid);// || ic->bBlockHasMesh || ic->sValues[3] || ic->BlockSize != vector3di(1, 1, 1));
}

void TerrainChunk::Update(bool bForce)
{
	// if we have any animated objects to update, do so now
	map<vector3di, AnimatedBlock>::iterator it = m_AnimatedBlocks.begin();
	while (it != m_AnimatedBlocks.end())
	{
		//it->second.obj->Update(bForce);
		it->second.obj->Animate(it->second.anim);

		++it;
	}

	//GLObject::Update(bForce);
	//CalculateInView();

	CreateWater();
}

void TerrainChunk::UpdateWater()
{
	// check for water updates
	if (!m_bWaterSettled && m_fWaterTimer <= 0.f)
	{
		// set the flag to true - it will get set to false again later in the code if we need to update again
		m_bWaterSettled = true;

		// variables needed to update
		GLfloat flow = 0.f;
		GLfloat waterLeft = 0.f;
		vector3di blockPos, chunkPos(GetChunkPos());
		TerrainChunk* chunk = this;
		TerrainBlock* block = NULL;
		GLint x, y, z;

		// function to update water
		auto update_water = [&](const vector3di& offset, bool force = false) -> bool {
			blockPos.Set(x + offset.x, y + offset.y, z + offset.z);
			chunkPos.Set(GetChunkPos());

			block = m_pTerrain->_BlockAt(blockPos, chunkPos, &chunk);
			if (block && block->BlockType == 0 && chunk->IsWaterInitialised() && chunk->GetNewWater(blockPos) < 1.f)
			{
				// there is a free space in this block, let us flow into it
				vector3di here(x, y, z);
				if (force)
				{
					// we are forcing (ie flowing down), try and dump more water beneath us
					flow = (1.f - chunk->GetNewWater(blockPos)) * 0.5f;
					Limit(flow, 0.f, GetNewWater(here));
				}
				else
				{
					// we aren't forcing (ie we are spreading to the sides), use a normal calculation
					flow = (GetWater(here) - chunk->GetNewWater(blockPos)) * 0.5f;
					if (flow > 0.1f)
						flow *= 0.5f;
				}

				bool bEven = round(abs(flow * 250.f)) <= 1.f;

				Limit(flow, 0.f, waterLeft);
				if (bEven && !force)
				{
					chunk->SetNewWater(blockPos, chunk->GetNewWater(here));
				}
				else
				{
					SetNewWater(here, GetNewWater(here) - flow);
					chunk->SetNewWater(blockPos, chunk->GetNewWater(blockPos) + flow);
				}
				waterLeft -= flow;

				// set the settled flags to false
				m_bWaterSettled = false;
				chunk->m_bWaterSettled = false;
				m_bHasWater = true;
				chunk->m_bHasWater = true;

				// we have updated water
				return flow > 0.02f;
			}

			// we haven't updated any water
			return false;
		};

		// go through and update the water!
		for (x = 0; x < CHUNK_SIZE; ++x) {
			for (z = 0; z < CHUNK_SIZE; ++z) {
				for (y = 0; y < CHUNK_SIZE; ++y) {
					// if there isn't water here, continue
					if (m_Terrain[x * CHUNK_SIZE * CHUNK_SIZE + z * CHUNK_SIZE + y].Water == 0)
						continue;

					// calculate water spread
					flow = 0.f;
					waterLeft = GetWater(vector3di(x, y, z));
					if (waterLeft <= 0.f)
						continue;

					// below
					if (!update_water(vector3di(0, -1, 0), true) && waterLeft > 0.f)
					{
						// up
						update_water(vector3di(0, 0, 1));
						if (waterLeft <= 0.f)
							continue;

						// down
						update_water(vector3di(0, 0, -1));
						if (waterLeft <= 0.f)
							continue;

						// left
						update_water(vector3di(1, 0, 0));
						if (waterLeft <= 0.f)
							continue;

						// right
						update_water(vector3di(-1, 0, 0));
					}
				}
			}
		}
	}

	if (m_bHasWater && m_fWaterTimer <= 0.f)
	{
		// clear vectors
		m_Mutex.lock();
		m_WaterVertices.clear();
		m_WaterIndices.clear();

		// function to smooth out corners
		auto smooth_corner = [](GLfloat level, GLfloat o1, GLfloat o2, GLfloat o3) -> GLfloat {
			GLfloat out = level;
			GLuint n = 1;

			if (o1 > 0.f)
			{
				out += o1;
				++n;
			}

			if (o2 > 0.f)
			{
				out += o2;
				++n;
			}

			if (o3 > 0.f)
			{
				out += o3;
				++n;
			}

			return (out * OneOver(n));
		};

		// update the current water status
		for (GLint x = 0; x < CHUNK_SIZE; ++x) {
			for (GLint z = 0; z < CHUNK_SIZE; ++z) {
				for (GLint y = 0; y < CHUNK_SIZE; ++y) {
					// if there isn't water here, continue
					if (m_Terrain[x * CHUNK_SIZE * CHUNK_SIZE + z * CHUNK_SIZE + y].BlockType > 0)
						continue;

					// sort out water list
					vector3di here(x, y, z);
					GLfloat current = GetWater(here);
					GLfloat next = GetNewWater(here);
					if (next > 0.01f)
					{
						// there is now water in this block, add it to the list
						BlockAt(here).Water = 1;

						// now we have updated this block, add data to the array to render (this block COULD be updated by another block later, but it doesn't really matter)
						if (!(y < CHUNK_SIZE - 1 && m_Terrain[x * CHUNK_SIZE * CHUNK_SIZE + z * CHUNK_SIZE + y + 1].Water == 1) && !(y == CHUNK_SIZE - 1 && _GetBlock(x, y + 1, z).Water == 1))
						{
							int iIndex = m_WaterVertices.size();
							GLfloat level = next;//ceil(next * 20.f) * 0.05f;

							//GLfloat level1 = smooth_corner(level, ceil(_GetWater(x + 1, y, z) * 20.f) * 0.05f, ceil(_GetWater(x + 1, y, z - 1) * 20.f) * 0.05f, ceil(_GetWater(x, y, z - 1) * 20.f) * 0.05f);
							//GLfloat level2 = smooth_corner(level, ceil(_GetWater(x - 1, y, z) * 20.f) * 0.05f, ceil(_GetWater(x - 1, y, z - 1) * 20.f) * 0.05f, ceil(_GetWater(x, y, z - 1) * 20.f) * 0.05f);
							//GLfloat level3 = smooth_corner(level, ceil(_GetWater(x - 1, y, z) * 20.f) * 0.05f, ceil(_GetWater(x - 1, y, z + 1) * 20.f) * 0.05f, ceil(_GetWater(x, y, z + 1) * 20.f) * 0.05f);
							//GLfloat level4 = smooth_corner(level, ceil(_GetWater(x + 1, y, z) * 20.f) * 0.05f, ceil(_GetWater(x + 1, y, z + 1) * 20.f) * 0.05f, ceil(_GetWater(x, y, z + 1) * 20.f) * 0.05f);

							m_WaterVertices.push_back(GLVertex(vector3df((x + 1) * BLOCK_SIZE, (y + level) * BLOCK_SIZE, (z + 0) * BLOCK_SIZE), GLColour(100, 150, 220, 190), vector2df(0.f, 0.f), GLNormal(0, 1, 0)));
							m_WaterVertices.push_back(GLVertex(vector3df((x + 0) * BLOCK_SIZE, (y + level) * BLOCK_SIZE, (z + 0) * BLOCK_SIZE), GLColour(100, 150, 220, 190), vector2df(0.f, 0.f), GLNormal(0, 1, 0)));
							m_WaterVertices.push_back(GLVertex(vector3df((x + 0) * BLOCK_SIZE, (y + level) * BLOCK_SIZE, (z + 1) * BLOCK_SIZE), GLColour(100, 150, 220, 190), vector2df(0.f, 0.f), GLNormal(0, 1, 0)));
							m_WaterVertices.push_back(GLVertex(vector3df((x + 1) * BLOCK_SIZE, (y + level) * BLOCK_SIZE, (z + 1) * BLOCK_SIZE), GLColour(100, 150, 220, 190), vector2df(0.f, 0.f), GLNormal(0, 1, 0)));

							m_WaterIndices.push_back(iIndex + 0); m_WaterIndices.push_back(iIndex + 1); m_WaterIndices.push_back(iIndex + 2);
							m_WaterIndices.push_back(iIndex + 2); m_WaterIndices.push_back(iIndex + 3); m_WaterIndices.push_back(iIndex + 0);
						}
					}

					SetWater(here, next);

					if (next <= 0.01f)
					{
						// there used to be water here but now there isn't, remove us from the list
						BlockAt(here).Water = 0;
					}
				}
			}
		}

		m_WaterIndexCount = m_WaterIndices.size();
		m_Mutex.unlock();

		// set the water timer back
		m_fWaterTimer = 1.f;
	}
}

void TerrainChunk::CreateWater()
{
	// water timer
	if (m_fWaterTimer > 0.f)
	{
		double delta = DeltaTime::GetDelta();
		m_fWaterTimer -= 20.f * delta;
	}

	// clear vectors
	//m_WaterVertices.clear();
	//m_WaterIndices.clear();

	// update the current water status
	/*for (GLint x = 0; x < CHUNK_SIZE; x++) {
	for (GLint z = 0; z < CHUNK_SIZE; z++) {
	for (GLint y = 0; y < CHUNK_SIZE; y++) {
	// if there isn't water here, continue
	if (m_Terrain[x * CHUNK_SIZE * CHUNK_SIZE + z * CHUNK_SIZE + y].BlockType > 0)
	continue;

	// sort out water list
	vector3di here(x, y, z);
	GLfloat current = GetWater(here);
	GLfloat next = GetNewWater(here);
	if (next > 0.01f)
	{
	// there is now water in this block, add it to the list
	BlockAt(here).Water = 1;

	// now we have updated this block, add data to the array to render (this block COULD be updated by another block later, but it doesn't really matter)
	if (!(y < CHUNK_SIZE - 1 && m_Terrain[x * CHUNK_SIZE * CHUNK_SIZE + z * CHUNK_SIZE + y + 1].Water == 1))
	{
	int iIndex = m_WaterVertices.size();
	GLfloat level = ceil(next * 20.f) * 0.05f;

	m_WaterVertices.push_back(GLVertex(vector3df((x + 1) * BLOCK_SIZE, (y + level) * BLOCK_SIZE, (z + 0) * BLOCK_SIZE), GLColour(100, 150, 220, 190), vector2df(0.f, 0.f), GLNormal(0, 1, 0)));
	m_WaterVertices.push_back(GLVertex(vector3df((x + 0) * BLOCK_SIZE, (y + level) * BLOCK_SIZE, (z + 0) * BLOCK_SIZE), GLColour(100, 150, 220, 190), vector2df(0.f, 0.f), GLNormal(0, 1, 0)));
	m_WaterVertices.push_back(GLVertex(vector3df((x + 0) * BLOCK_SIZE, (y + level) * BLOCK_SIZE, (z + 1) * BLOCK_SIZE), GLColour(100, 150, 220, 190), vector2df(0.f, 0.f), GLNormal(0, 1, 0)));
	m_WaterVertices.push_back(GLVertex(vector3df((x + 1) * BLOCK_SIZE, (y + level) * BLOCK_SIZE, (z + 1) * BLOCK_SIZE), GLColour(100, 150, 220, 190), vector2df(0.f, 0.f), GLNormal(0, 1, 0)));

	m_WaterIndices.push_back(iIndex + 0); m_WaterIndices.push_back(iIndex + 1); m_WaterIndices.push_back(iIndex + 2);
	m_WaterIndices.push_back(iIndex + 2); m_WaterIndices.push_back(iIndex + 3); m_WaterIndices.push_back(iIndex + 0);
	}
	}

	SetWater(here, next);

	if (next <= 0.01f)
	{
	// there used to be water here but now there isn't, remove us from the list
	BlockAt(here).Water = 0;
	}
	}
	}
	}*/

	// water indices size
	m_Mutex.lock();
	if (!m_WaterVertices.empty() && !m_WaterIndices.empty())
	{
		// create water buffer objects
		if (m_WaterEBO == 0 || m_WaterVBO == 0)
		{
			glGenBuffers(1, &m_WaterVBO);
			glGenBuffers(1, &m_WaterEBO);
		}

		// check we have a water texture
		if (m_WaterTex.gltex == 0)
			m_WaterTex = GetSceneManager()->GetTextureManager()->GetTexture("all_white.png");

		// now buffer the data into the buffers
		glBindBuffer(GL_ARRAY_BUFFER, m_WaterVBO);
		glBufferData(GL_ARRAY_BUFFER, m_WaterVertices.size() * sizeof(GLVertex), NULL, GL_STREAM_DRAW);
		glBufferSubData(GL_ARRAY_BUFFER, 0, m_WaterVertices.size() * sizeof(GLVertex), &m_WaterVertices[0]);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_WaterEBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_WaterIndices.size() * sizeof(GLuint), NULL, GL_STREAM_DRAW);
		glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, m_WaterIndices.size() * sizeof(GLuint), &m_WaterIndices[0]);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	m_Mutex.unlock();
}

void TerrainChunk::Render(bool& bCreate)
{
	if (bCreate && m_bMeshCreated && !m_bBuffersCreated)
	{
		CreateBufferObjects();
		m_bBuffersCreated = true;
		m_bCreating = false;
		bCreate = false;
	}
	else if (!GetVBO())
	{
		if (!m_bMeshCreated && m_bCreating)
		{
			if (++m_CreatingCounter > 50)
			{
				m_CreatingCounter = 0;
				m_bCreating = false;
			}
		}

		return;
	}

	GLObject::Render();

	// if we have any water to render, do so now
	/*if (m_WaterIndices.size() > 0 && m_WaterVBO > 0 && m_WaterEBO > 0)
	{
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_WaterTex.gltex);
	glBindBuffer(GL_ARRAY_BUFFER, m_WaterVBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_WaterEBO);

	Bind();

	glDrawElements(GL_TRIANGLES, m_WaterIndices.size(), GL_UNSIGNED_INT, 0);

	Unbind();

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);
	}*/

	// loop through the block meshes map and draw any blocks that have custom meshes
	if (InViewFrustum() && !m_BlockMeshes.empty())
	{
		map<GLuint, Mesh*>::iterator it = m_BlockMeshes.begin();
		matrix4f m;
		while (it != m_BlockMeshes.end())
		{
			RenderMesh(it->second, it->first, m);

			++it;
		}
	}

	// if we have any animated objects to render, do so now
	/*if (!m_AnimatedBlocks.empty())
	{
	map<vector3di, AnimatedBlock>::iterator it = m_AnimatedBlocks.begin();
	while (it != m_AnimatedBlocks.end())
	{
	it->second.obj->Render();

	++it;
	}
	}*/
}

void TerrainChunk::RenderWater()
{
	// if we have any water to render, do so now
	if (InViewFrustum() && m_WaterIndices.size() > 0 && m_WaterVBO > 0 && m_WaterEBO > 0)
	{
		SetUniforms();
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, m_WaterTex.gltex);
		glBindBuffer(GL_ARRAY_BUFFER, m_WaterVBO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_WaterEBO);

		Bind();

		glDisable(GL_CULL_FACE);
		glDrawElements(GL_TRIANGLES, m_WaterIndices.size(), GL_UNSIGNED_INT, 0);
		glEnable(GL_CULL_FACE);

		Unbind();

		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, 0);
	}
}

void TerrainChunk::RenderShadows()
{
	if (m_bMeshCreated && !m_bBuffersCreated)
	{
		CreateBufferObjects();
		m_bBuffersCreated = true;
		m_bCreating = false;
	}
	else if (!GetVBO())
		return;

	GLObject::RenderShadows();

	// loop through the block meshes map and draw any blocks that have custom meshes
	if (m_BlockMeshes.size())
	{
		map<GLuint, Mesh*>::iterator it = m_BlockMeshes.begin();
		matrix4f m;
		while (it != m_BlockMeshes.end())
		{
			RenderShadowMesh(it->second, it->first, m);

			++it;
		}

		/*Mesh *mesh;
		map<vector3di, BlockMesh>::iterator it = m_BlockMeshes.begin();
		while (it != m_BlockMeshes.end())
		{
		if (it == m_BlockMeshes.end())
		break;

		TerrainBlock tb = it->second.type;
		matrix4f m = it->second.transform;
		mesh = &it->second.mesh;

		RenderShadowMesh(mesh, it->second.texture.gltex, m);

		if (it != m_BlockMeshes.end())
		it++;
		}*/
	}

	// if we have any animated objects to render, do so now
	/*if (!m_AnimatedBlocks.empty())
	{
	map<vector3di, AnimatedBlock>::iterator it = m_AnimatedBlocks.begin();
	while (it != m_AnimatedBlocks.end())
	{
	it->second.obj->RenderShadows();

	++it;
	}
	}*/
}

void TerrainChunk::_CorrectUV(const vector4df from[], vector4df to[], TerrainBlock block) const
{
	// use this block's rotation values to correct the UVs
	int x = block.RotX;
	int y = block.RotY;
	int z = block.RotZ;

	for (int i = 0; i < 6; ++i) {
		to[i] = from[i];
	}

	// 0 : +y : top
	// 1 : -y : bottom
	// 2 : -x : front
	// 3 : +x : back
	// 4 : -z : right
	// 5 : +z : left

	// if z is 0, this is normal (upright), sorting UVs shall be simple
	if (z == 0)
	{
		if (x == 1)
		{
			if (y == 1)
			{
				// if x and y are 1, rotate the block -90 degrees
				to[4] = from[2];
				to[3] = from[4];
				to[5] = from[3];
				to[2] = from[5];
			}
			else
			{
				// if x is 1 and y is 0, rotate the block 180 degrees
				to[2] = from[3];
				to[3] = from[2];
				to[4] = from[5];
				to[5] = from[4];
			}
		}
		else if (y == 1)
		{
			// if y is 1, rotate the block 90 degrees
			to[5] = from[2];
			to[3] = from[5];
			to[4] = from[3];
			to[2] = from[4];
		}
	}
	else
	{

	}
}

void TerrainChunk::_MergeLighting(GLint x, GLint y, GLint z, GLColour& col) const
{
	TerrainBlock tb = _GetBlock(x, y, z);
	GLuint r = tb.LightRed * 17;
	GLuint g = tb.LightGreen * 17;
	GLuint b = tb.LightBlue * 17;
	GLuint a = tb.SkyLight * 17;

	col.r = r; col.g = g; col.b = b; col.a = a;
}