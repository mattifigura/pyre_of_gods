#include "MeshView.h"

MeshView::MeshView(Mesh* mesh, string texture, Manager* parent, GUIElement* guiParent, vector3df pos, vector3di size)
	: GUIElement(parent, guiParent, pos, size)
	, m_DestScale(1.f)
	, m_Scale(1.f)
{
	// set the mesh, texture and rotation
	SetClickable(false);
	if (mesh != NULL && !texture.empty())
		SetMeshView(mesh, texture);
	//SetUseBones(false);

	// figure out the scaling and translation
	SetDefaultModelView();
}

void MeshView::SetMeshView(Mesh* mesh, string texture)
{
	SetMesh(mesh, false, true);
	if (!texture.empty())
		SetTexture(texture.c_str());
}

void MeshView::SetModelOffset(const vector2df& offset)
{
	m_ModelOffset.Set(offset);
}

void MeshView::SetDefaultModelView()
{
	vector3di size = GetSize();
	GLfloat scaleX = size.x / 7.0f;
	GLfloat scaleY = size.y / 7.0f;
	SetScale(vector3df(scaleX, scaleY, 0.2f));
	m_ModelOffset.Set((GLfloat)(size.x - 6), (GLfloat)(size.y - 6));
	SetRotation(vector3df(0, 1, 1.5f), PI_OVER_2);
}

void MeshView::SetBlockModelView(const vector3di& blockSize)
{
	vector2di size = GetSize();
	GLfloat m = max(blockSize.x, blockSize.y, blockSize.z);
	GLfloat scale = ((GLfloat)size.x / 48.0f) * 3.0f;
	GLfloat off = max(blockSize.x, blockSize.z) != blockSize.y ? -4 : 0;
	SetModelOffset(vector2df((GLfloat)(size.x / 2), (GLfloat)(size.y - 10) + off));
	SetScale(vector3df(scale / m, scale / m, 0.2f));
	SetRotation(vector3df(0.3f, 0.5f, 2), PI_OVER_2);
}

void MeshView::SetIngotModelView()
{
	vector2di size = GetSize();
	GLfloat scale = ((GLfloat)size.x / 48.0f) * 3.0f;
	SetModelOffset(vector2df((GLfloat)(size.x / 2), (GLfloat)(size.y * 0.5f + size.y * 0.05f)));
	SetScale(vector3df(scale * 4, scale * 4, 1));
	SetRotation(vector3df(0.3f, 0.85f, 2), PI_OVER_2);
}

void MeshView::ScaleTo(const vector3df& scale)
{
	m_DestScale.Set(scale);
}

void MeshView::Update(bool bForce)
{
	// if we aren't visible then don't update anything
	if (!bForce && !IsVisible())
		return;

	// offset
	if (GetParent())
	{
		GLObject::SetPosition(GetParent()->GetPosition() + m_Offset + vector3df(0, 0, m_Depth));
	}
	else
		GLObject::TranslatePosition(vector3df(0, 0, m_Depth));

	// scaling
	if (m_Scale != m_DestScale)
	{
		Interpolate(m_Scale.x, m_DestScale.x, 8.f, 0.001f);
		Interpolate(m_Scale.y, m_DestScale.y, 8.f, 0.001f);
		Interpolate(m_Scale.y, m_DestScale.y, 8.f, 0.001f);
	}

	// update this yo
	GLObject::UpdateTransformationMatrix(m_ModelOffset, matrix4f(0.f), m_Scale);

	if (!GetParent())
		GLObject::TranslatePosition(vector3df(0, 0, -m_Depth));
}

void MeshView::Render()
{
	// as meshview cannot have any children, just call GLObject::Render()
	GLObject::Render();
}

void MeshView::MouseDownFunction(GLuint button)
{
	// clicking on a mesh doesn't do anything at the moment
}
