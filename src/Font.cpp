#include "Font.h"
#include <vector>
#include <fstream>
#include <sstream>

short ReadShort(char* data)
{
	// read a short from the character data
	return ((data[0] << 7) | (data[1] & 0x00FF));
}

Font::Font(string fileName, TextureManager* texMan)
{
	// load the font
	std::stringstream texName;
	texName << fileName << ".png";
	texture = texMan->GetTexture(texName.str().c_str());
	
	// load the font details
	std::stringstream fontFile;
	fontFile << "media/" << fileName << ".fnd";
	
	char* data;
	std::streampos size;
	std::ifstream file(fontFile.str().c_str(), std::ios::in | std::ios::binary | std::ios::ate);
	if (file.is_open())
	{
		// get the size of the file so we can allocate data
		size = file.tellg();
		data = new char[(unsigned int)size];
		file.seekg(0, std::ios::beg);
		file.read(data, size);
		file.close();
		
		// read the width, height and character height
		details.x = ReadShort(&data[0]);
		details.y = ReadShort(&data[2]);
		details.z = ReadShort(&data[4]);
		
		// loop through and read in each character's position details
		int i = 6;
		while (i + 7 <= size)
		{
			char c = data[i];
			CharacterDetail deet;
			
			deet.charWidth = ReadShort(&data[i + 1]);
			deet.charX = ReadShort(&data[i + 3]);
			deet.charY = ReadShort(&data[i + 5]);
			
			chars[c] = deet;
			
			i += 7;
		}
		
		// free up memory which is no longer needed
		delete[] data;
		
		// we have loaded the font
		cout << "Loading font " << fileName << endl;
	}
}

Font::~Font()
{
	chars.clear();
}

Texture Font::GetTexture()
{
	return texture;
}

const CharacterDetail& Font::GetChar(const char c)
{
	return chars[c];
}

GLuint Font::GetStringWidth(string text)
{
	// get the width of the given string
	GLuint width = 0;
	
	int i = -1;
	while (true)
	{
		const char c = text[++i];

		if (c == '\0')
		{
			break;
		}

		width += chars[c].charWidth;
	}
	
	return width;
}

const vector3ds& Font::GetDetails()
{
	return details;
}
