#include "RecipeBook.h"
#include "XMLReader.h"
#include "DropDownMenu.h"

RecipeBook::RecipeBook()
{

}

RecipeBook::~RecipeBook()
{
	for (map<string, Recipe*>::iterator it = m_Recipes.begin(); it != m_Recipes.end(); ++it) {
		delete it->second;
	}
	m_Recipes.clear();
}

bool RecipeBook::LoadRecipes(string file)
{
	// create an XML file reader
	XMLReader XML(file);
	if (!XML.IsOpen())
		return false;

	// read the file
	const string strRecipesTag("recipes");
	const string strNodeTag("recipe");
	string strCurrentSection, strNodeName;
	Recipe* recipe = NULL;
	bool bLoaded = false;

	while (XML.ReadLine())
	{
		if (XML.IsNode(strRecipesTag))
			strCurrentSection = strRecipesTag;
		else if (strCurrentSection == strRecipesTag && XML.IsNode(strNodeTag))
			strCurrentSection = strNodeTag;
		else if (strCurrentSection == strNodeTag)
		{
			// check if we have reached the end of the item
			if (XML.IsEndOfNode(strCurrentSection))
			{
				strCurrentSection = strRecipesTag;

				continue;
			}

			// look for what we need
			strNodeName = XML.GetNodeName();
			if (strNodeName == "name")
			{
				recipe = new Recipe();
				recipe->name = XML.GetAttributeValue("value");

				if (m_Recipes[recipe->name] != NULL)
				{
					delete recipe;
					recipe = NULL;

					strCurrentSection = strRecipesTag;
				}
				else
				{
					m_Recipes[recipe->name] = recipe;
					bLoaded = true; // we have loaded at least one so return true
				}
			}
			else if (strNodeName == "produce")
			{
				recipe->produce = Item::GetItemClass(XML.GetAttributeValue("item"));
				recipe->produceAmount = atoi(XML.GetAttributeValue("amount").c_str());
			}
			else if (strNodeName == "ingredient")
			{
				int n = atoi(XML.GetAttributeValue("amount").c_str());
				recipe->ingredients[Item::GetItemClass(XML.GetAttributeValue("item"))] = n;
			}
			else if (strNodeName == "crafted_at")
			{
				recipe->craftingStation = (GLushort)Item::GetBlockFromName(XML.GetAttributeValue("block"));
			}
		}
	}

	return bLoaded;
}

bool RecipeBook::LoadRecipePages(string file, DropDownMenu* menu)
{
	// create an XML file reader
	XMLReader XML(file);
	if (!XML.IsOpen())
		return false;

	// read the file
	const string strRecipesTag("recipebook");
	const string strNodeTag("page");
	string strCurrentSection, strNodeName, strPageName;
	bool bLoaded = false;

	while (XML.ReadLine())
	{
		if (XML.IsNode(strRecipesTag))
			strCurrentSection = strRecipesTag;
		else if (strCurrentSection == strRecipesTag && XML.IsNode(strNodeTag))
			strCurrentSection = strNodeTag;
		else if (strCurrentSection == strNodeTag)
		{
			// check if we have reached the end of the item
			if (XML.IsEndOfNode(strCurrentSection))
			{
				strCurrentSection = strRecipesTag;

				continue;
			}

			// look for what we need
			strNodeName = XML.GetNodeName();
			if (strNodeName == "name")
			{
				strPageName = XML.GetAttributeValue("value");
				menu->AddItem(strPageName);
			}
			else if (strNodeName == "recipe")
			{
				m_RecipePages[strPageName][XML.GetAttributeValue("name")] = true;
				m_RecipePages["All"][XML.GetAttributeValue("name")] = true; // add to the "All" tab so that it is in alphabetical order
			}
		}
	}

	return bLoaded;
}

void RecipeBook::AddRecipesToListBox(string page, ListBox* lb)
{
	// add recipes from the given page into the given listbox
	lb->ClearList();
	map<string, bool>::iterator it = m_RecipePages[page].begin();

	while (it != m_RecipePages[page].end())
	{
		lb->AddItem(it->first, false);

		++it;
	}

	lb->Create();
}

Recipe* RecipeBook::GetRecipe(string name)
{
	return m_Recipes[name];
}