#include "CheckBox.h"
#include "Text.h"
#include "GUIManager.h"

CheckBox::CheckBox(Manager* parent, GUIElement* guiParent, vector2df pos, vector3di size, string label)
	: GUIElement(parent, guiParent, pos, size)
	, m_bChecked(false)
{
	// add the quads
	vector2df boxPos = vector2df((GLfloat)size.x - 21, (GLfloat)(GLint)(size.y / 2.f) - 10);
	AddQuad(GLVertex(vector3df(boxPos.x + 2, boxPos.y + 2, -20), GLColour(0), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(boxPos.x + 19, boxPos.y + 2, -20), GLColour(0), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(boxPos.x + 19, boxPos.y + 19, -20), GLColour(15), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(boxPos.x + 2, boxPos.y + 19, -20), GLColour(15), vector2df(0.5, 0.5), GLNormal()));
	AddQuad(boxPos, vector2df(48.f / 256.f, 235.f / 256.f), boxPos + vector2df(21, 21), vector2df(69.f / 256.f, 1), GLColour(0, 0));

	AddQuad(boxPos, vector2df(0.5, 0.5), vector2df(boxPos.x + 21, boxPos.y + 1), vector2df(0.5, 0.5), GLColour(61));
	AddQuad(vector2df(boxPos.x, boxPos.y + 20), vector2df(0.5, 0.5), vector2df(boxPos.x + 21, boxPos.y + 21), vector2df(0.5, 0.5), GLColour(61));
	AddQuad(vector2df(boxPos.x, boxPos.y + 1), vector2df(0.5, 0.5), vector2df(boxPos.x + 1, boxPos.y + 20), vector2df(0.5, 0.5), GLColour(61));
	AddQuad(vector2df(boxPos.x + 20, boxPos.y + 1), vector2df(0.5, 0.5), vector2df(boxPos.x + 21, boxPos.y + 20), vector2df(0.5, 0.5), GLColour(61));
	CreateBufferObjects();

	// add the text
	GetGUIManager()->CreateText(label.c_str(), vector2df(0, (GLfloat)(GLint)(size.y / 2.f) - 7), vector2di(size.x, -1), NULL, this);

	// set the default function
	SetFunction(left_mouse, CheckBox::CheckBoxClicked);
}

void CheckBox::MouseDownFunction(GLuint button)
{

}

void CheckBox::SetChecked(bool checked)
{
	m_bChecked = checked;

	if (m_bChecked)
		ChangeQuadColour(1, GLColour(255));
	else
		ChangeQuadColour(1, GLColour(0, 0));
}

bool CheckBox::GetChecked()
{
	return m_bChecked;
}

void CheckBox::CheckBoxClicked(GUIElement* e)
{
	CheckBox* cb = dynamic_cast<CheckBox*>(e);

	if (cb)
		cb->SetChecked(!cb->GetChecked());
}