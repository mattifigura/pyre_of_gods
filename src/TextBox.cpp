#include "TextBox.h"
#include "GUIManager.h"

TextBox::TextBox(string text, Manager* parent, GUIElement* guiParent, vector3df pos, vector3di size)
	: GUIElement(parent, guiParent, pos, size)
	, m_CharFunction(NULL)
	, m_bNumbersOnly(false)
	, m_CharLimit(0)
{
	// create the quads
	//AddQuad(vector2df(), vector2df(0.5, 0.5), vector2df((GLfloat)size.x, (GLfloat)size.y), vector2df(0.5, 0.5), GLColour(0, 0, 0, 200));
	AddQuad(GLVertex(vector3df(0, 0, -20), GLColour(0), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df((GLfloat)size.x, 0, -20), GLColour(0), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df((GLfloat)size.x, (GLfloat)size.y, -20), GLColour(30), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(0, (GLfloat)size.y, -20), GLColour(30), vector2df(0.5, 0.5), GLNormal()));
	AddQuad(vector2df(), vector2df(0.5, 0.5), vector2df(), vector2df(0.5, 0.5), GLColour(150, 150, 150, 200));

	AddQuad(vector2df(), vector2df(0.5, 0.5), vector2df((GLfloat)size.x, 1), vector2df(0.5, 0.5), GLColour(51, 51, 51, 255));
	AddQuad(vector2df(0, (GLfloat)size.y - 1), vector2df(0.5, 0.5), vector2df((GLfloat)size.x, (GLfloat)size.y), vector2df(0.5, 0.5), GLColour(51, 51, 51, 255));
	AddQuad(vector2df(0, 1), vector2df(0.5, 0.5), vector2df(1, (GLfloat)size.y - 1), vector2df(0.5, 0.5), GLColour(51, 51, 51, 255));
	AddQuad(vector2df((GLfloat)size.x - 1, 1), vector2df(0.5, 0.5), vector2df((GLfloat)size.x, (GLfloat)size.y - 1), vector2df(0.5, 0.5), GLColour(51, 51, 51, 255));
	CreateBufferObjects();

	// create the text
	m_Text = GetGUIManager()->CreateText(text.c_str(), vector2df(5, 2), vector2di(1000000, 1), NULL, this);
	m_Hint = GetGUIManager()->CreateText("", vector2df(5, 2), vector2di(size.x, 1), NULL, this);
	m_Hint->SetVisible(false);

	// set the default function
	SetFunction(left_mouse, TextBox::DefaultFunction);
}

void TextBox::SetCharLimit(GLuint limit)
{
	m_CharLimit = limit;
}

void TextBox::SetNumeric(bool numbers)
{
	m_bNumbersOnly = true;
}

void TextBox::SetHint(string hint)
{
	// set the hint
	m_Hint->SetText(string("^9").append(hint), true);
	m_Hint->SetVisible(m_Text->GetTextLength() < 1);
}

void TextBox::UnFocus()
{
	if (!IsHovered())
	{
		if (GetGUIManager()->GetCharCallback() == this)
			GetGUIManager()->SetCharCallback(NULL);
		ChangeQuadColour(0, GLColour(0), GLColour(0), GLColour(30), GLColour(30));
		ChangeQuadCoords(1, vector2df(), vector2df());
		if (m_Text->GetText() == "" && m_Hint->GetText() != "")
			m_Hint->SetVisible(true);
	}
}

void TextBox::TextEntered(GLuint codepoint)
{
	// if we are the char callback, handle text input
	//cout << (char)codepoint << endl;
	if ((!m_bNumbersOnly || (isdigit(codepoint) || (char)codepoint == '-' || (char)codepoint == '.')) && (m_CharLimit == 0 || m_Text->GetText().length() < m_CharLimit))
	{
		m_Text->AppendText(string(1, (char)codepoint));
		m_Hint->SetVisible(false);
		SetCursor();

		if (m_CharFunction)
			m_CharFunction(this);
	}
}

void TextBox::SetText(string text)
{
	m_Text->SetText(text, true);
	m_Hint->SetVisible(text == "" && m_Hint->GetText() != "");
}

string TextBox::GetText()
{
	return m_Text->GetText();
}

void TextBox::SetCharFunction(void(*fn)(GUIElement*))
{
	m_CharFunction = fn;
}

void TextBox::Update(bool bForce)
{
	// if we aren't visible then don't update anything
	if (!bForce && !IsVisible())
		return;

	// backspace
	if (GetGUIManager()->GetKeyState(GLFW_KEY_BACKSPACE) && GetGUIManager()->GetCharCallback() == this)
	{
		if (m_Counter == 0 || m_Counter > 20)
		{
			string s = m_Text->GetText();
			if (s.length())
			{
				m_Text->SetText(s.substr(0, s.length() - 1), s.length() == 1);
				SetCursor();

				if (m_CharFunction)
					m_CharFunction(this);
			}
			++m_Counter;
		}
		else
			++m_Counter;
	}
	else
		m_Counter = 0;

	// call GUIElement::Update()
	GUIElement::Update(bForce);
}

void TextBox::MouseDownFunction(GLuint button)
{
	// do nothing
}

void TextBox::Release(GLuint button)
{
	// call base class release
	GUIElement::Release(button);

	// if we click outside of the textbox and we are the char callback, remove us from being the callback
	UnFocus();
}

void TextBox::DefaultFunction(GUIElement* e)
{
	// set us as the char callback
	TextBox* tb = dynamic_cast<TextBox*>(e);
	e->GetGUIManager()->SetCharCallback(tb);
	tb->ChangeQuadColour(0, GLColour(50), GLColour(50), GLColour(90), GLColour(90));
	tb->m_Hint->SetVisible(false);
	tb->SetCursor();
}

void TextBox::SetCursor()
{
	GLfloat l = (GLfloat)m_Text->GetFont()->GetStringWidth(m_Text->GetReadableText());
	ChangeQuadCoords(1, vector2df(l + 5, 2), vector2df(l + 7, (GLfloat)GetSize().y - 2));
}