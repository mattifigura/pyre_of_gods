#ifndef _CLIENT_H_
#define _CLIENT_H_

#include "GLObject.h"
#include "Animator.h"

class Item;
struct ItemClass;
struct Stats;

class Client : public GLObject {
	public:
		Client(Manager* parent, vector3df pos);
		~Client();

		void SetDestPosition(vector3df& pos);
		void SetLookRotation(const vector2df& rot);
		void SetSwinging(bool swing);
		void SetSelectedItem(int item);

		void LoadInventory(int type, unsigned char* buf, int len);
		void ClearHotbar();

		string GetName();
		void SetName(string name);

		Stats GetStats() const;
		void SetStats(const Stats& stats);

		virtual void Update(bool bForce = false);
		virtual void Render();
		virtual void RenderShadows();

	protected:

	private:
		vector3df					m_DestPos;
		Animator					m_Animation;
		Animator					m_ToolAnimation;
		vector2df					m_LookRot;
		bool						m_bSwinging;
		vector<Item*>				m_Hotbar;
		vector<const ItemClass*>	m_HotbarItemClass;
		vector<Item*>				m_Equipment;
		vector<const ItemClass*>	m_EquipmentItemClass;
		int							m_SelectedItem;
		matrix4f					m_SelectedItemMatrix;
		string						m_Name;
		Stats						m_Stats;
};

#endif