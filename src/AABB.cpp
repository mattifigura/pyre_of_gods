#include "AABB.h"
#include "ViewFrustum.h"

void AABB::SetAABB(const vector3df& bl, const vector3df& tr)
{
	bottomLeft.Set(bl);
	topRight.Set(tr);
	midPoint = (topRight + bottomLeft) * 0.5f;

	size = bottomLeft.Distance(topRight);
	halfsize = size * 0.5f;
}

bool AABB::InViewFrustum(const ViewFrustum& frustum) const
{
	// check bottom left
	if (frustum.PointInFrustum(bottomLeft))
		return true;

	// check top right
	if (frustum.PointInFrustum(topRight))
		return true;

	// check other points
	if (frustum.PointInFrustum(vector3df(bottomLeft.x, bottomLeft.y, topRight.z)))
		return true;
	if (frustum.PointInFrustum(vector3df(bottomLeft.x, topRight.y, topRight.z)))
		return true;
	if (frustum.PointInFrustum(vector3df(topRight.x, bottomLeft.y, topRight.z)))
		return true;
	if (frustum.PointInFrustum(vector3df(topRight.x, bottomLeft.y, bottomLeft.z)))
		return true;
	if (frustum.PointInFrustum(vector3df(bottomLeft.x, topRight.y, bottomLeft.z)))
		return true;
	if (frustum.PointInFrustum(vector3df(topRight.x, topRight.y, bottomLeft.z)))
		return true;

	// we are not in the frustum
	return false;
}

bool AABB::QuickInViewFrustum(const ViewFrustum& frustum, const vector3df& pos) const
{
	// check the middle point with the halfsize
	return frustum.QuickPointInFrustum(midPoint + pos, halfsize);
}

// AABB2D class
AABB2D::AABB2D(const vector2df& _where, const float _size)
	: m_Position(_where)
	, m_HalfSize(_size)
{
	// Nothing to do here
}

AABB2D::AABB2D(const AABB2D& other)
	: m_Position(other.m_Position)
	, m_HalfSize(other.m_HalfSize)
{
	// Nothing to do here
}

bool AABB2D::PointInside(const vector2df& _where) const
{
	// Is this point inside us?
	return ((_where.x >= m_Position.x - m_HalfSize && _where.x <= m_Position.x + m_HalfSize) &&
		(_where.y >= m_Position.y - m_HalfSize && _where.y <= m_Position.y + m_HalfSize));
}

bool AABB2D::Intersects(const AABB2D& other) const
{
	// Does this AABB intersect us?
	return ((m_Position.x + m_HalfSize >= other.m_Position.x - other.m_HalfSize || m_Position.x - m_HalfSize <= other.m_Position.x + other.m_HalfSize) &&
		(m_Position.y + m_HalfSize >= other.m_Position.y - other.m_HalfSize || m_Position.y - m_HalfSize <= other.m_Position.y + other.m_HalfSize));
}

float AABB2D::GetHalfSize() const
{
	// Get our half size
	return m_HalfSize;
}

vector2df AABB2D::GetPosition() const
{
	// Get our position
	return m_Position;
}