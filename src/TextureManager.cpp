#include <iostream>
#include <cstdlib>
#include "TextureManager.h"

using std::cout;
using std::endl;

TextureManager::TextureManager()
{

}

TextureManager::~TextureManager()
{
	// clean up
	CleanUp();
}

void TextureManager::CleanUp()
{
	// call glDeleteTextures to clean up
	map<string, Texture>::iterator it = m_Textures.begin();
	while (it != m_Textures.end())
	{
		cout << "Deleting " << it->first << endl;
		glDeleteTextures(1, &(it->second.gltex));
		
		++it;
	}
	
	// clear the textures map
	m_Textures.clear();
}

void TextureManager::DeleteTexture(string name)
{
	if (m_Textures.find(name) == m_Textures.end())
		return;
	
	glDeleteTextures(1, &m_Textures[name].gltex);
	m_Textures.erase(name);
}

Texture TextureManager::GetTexture(string fileName)
{
	// firstly check if this file is already loaded as a texture
	if (m_Textures[fileName].gltex)
	{
		//cout << "Texture " << fileName << " already loaded" << endl;
		return m_Textures[fileName];
	}

	// check if we've been given a blank string
	if (fileName.empty())
		return Texture();
	
	// this texture hasn't yet been loaded, let us load it
	cout << "Loading texture " << fileName << endl;
	
	// load the texture from a png file and bind it to a gl texture
#ifdef _WIN32
	FILE *file = new FILE;
	fopen_s(&file, string("media/").append(fileName).c_str(), "rb");
#else
	FILE* file = fopen(string("media/").append(fileName).c_str(), "rb");
#endif
	if (!file)
	{
		cout << "ERROR: Cannot open texture file " << fileName << endl;
		return Texture();
	}
	
	// check if this is a png file or not
	png_byte pngHeader[8];
	fread(pngHeader, 1, 8, file);
	
	if (png_sig_cmp(pngHeader, 0, 8))
	{
		cout << "ERROR: " << fileName << " is not a valid PNG file!" << endl;
		fclose(file);
		return Texture();
	}
	
	// create png read struct
	png_structp pngPtr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (!pngPtr)
	{
		cout << "ERROR: PNG file loading error #1066" << endl;
		fclose(file);
		return Texture();
	}
	
	// create png info structs
	png_infop infoPtr = png_create_info_struct(pngPtr);
	if (!infoPtr)
	{
		cout << "ERROR: PNG file loading error #1166" << endl;
		png_destroy_read_struct(&pngPtr, NULL, NULL);
		fclose(file);
		return Texture();
	}
	
	png_infop endPtr = png_create_info_struct(pngPtr);
	if (!endPtr)
	{
		cout << "ERROR: PNG file loading error #1266" << endl;
		png_destroy_read_struct(&pngPtr, &infoPtr, NULL);
		fclose(file);
		return Texture();
	}
	
	// we use setjmp to see if libpng has encountered an error
	if (setjmp(png_jmpbuf(pngPtr)))
	{
		cout << "ERROR: PNG file loading error #1366" << endl;
		png_destroy_read_struct(&pngPtr, &infoPtr, &endPtr);
		fclose(file);
		return Texture();
	}
	
	// start reading the png file
	png_init_io(pngPtr, file);
	png_set_sig_bytes(pngPtr, 8);
	png_read_info(pngPtr, infoPtr);
	
	int iBitDepth, iColourType;
	png_uint_32 iWidth, iHeight;
	
	png_get_IHDR(pngPtr, infoPtr, &iWidth, &iHeight, &iBitDepth, &iColourType, NULL, NULL, NULL);
	
	if (iBitDepth != 8)
	{
		cout << "ERROR: PNG file loading error #1466" << endl;
		png_destroy_read_struct(&pngPtr, &infoPtr, &endPtr);
		fclose(file);
		return Texture();
	}
	
	// get the colour format of this png file
	GLint format = GL_RGBA;
	if (iColourType == PNG_COLOR_TYPE_RGB)
	{
		format = GL_RGB;
	}
	else if (iColourType != PNG_COLOR_TYPE_RGB_ALPHA)
	{
		cout << "ERROR: PNG file loading error #1566" << endl;
		png_destroy_read_struct(&pngPtr, &infoPtr, &endPtr);
		fclose(file);
		return Texture();
	}
	
	png_read_update_info(pngPtr, infoPtr);
	
	int iRowBytes = png_get_rowbytes(pngPtr, infoPtr);
	iRowBytes += 3 - ((iRowBytes - 1) % 4);
	
	png_byte* imageData = (png_byte*)malloc(iRowBytes * iHeight * sizeof(png_byte) + 15);
	if (!imageData)
	{
		cout << "ERROR: PNG file loading error #1666" << endl;
		png_destroy_read_struct(&pngPtr, &infoPtr, &endPtr);
		fclose(file);
		return Texture();
	}
	
	png_byte** rowPtrs = (png_byte**)malloc(iHeight * sizeof(png_byte*));
	if (!rowPtrs)
	{
		cout << "ERROR: PNG file loading error #1666" << endl;
		png_destroy_read_struct(&pngPtr, &infoPtr, &endPtr);
		free(imageData);
		fclose(file);
		return Texture();
	}
	
	for (unsigned int i = 0; i < iHeight; ++i) {
		rowPtrs[iHeight - 1 - i] = imageData + i * iRowBytes;
	}
	
	// read the data from the png
	png_read_image(pngPtr, rowPtrs);
	png_read_end(pngPtr, endPtr);
	
	// now we can generate an opengl texture :)
	GLuint texture;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	//glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 0);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, format, iWidth, iHeight, 0, format, GL_UNSIGNED_BYTE, imageData);
	glBindTexture(GL_TEXTURE_2D, 0);
	//int texture = 0;
	
	// clean up
	png_destroy_read_struct(&pngPtr, &infoPtr, &endPtr);
	free(imageData);
	free(rowPtrs);
	fclose(file);
	
	// add the texture to the map and return it
	Texture tex(texture, iWidth, iHeight);
	m_Textures[fileName] = tex;
	return tex;
}

Texture TextureManager::CreateTexture(string name, GLint width, GLint height, GLubyte* data)
{
	// if a texture exists with this name, return that
	if (m_Textures[name].gltex)
		return m_Textures[name];
	
	// create a texture
	GLuint texture;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	//glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 0);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
	glBindTexture(GL_TEXTURE_2D, 0);

	// add the texture to the map and return it
	Texture tex(texture, width, height);
	m_Textures[name] = tex;
	return tex;
}
