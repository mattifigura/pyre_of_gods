#include "AnimatedObject.h"

AnimatedObject::AnimatedObject(Manager* parent, vector3df pos, vector3di size)
	: GLObject(parent, pos, size)
	, m_fAnimSpeed(10)
	, m_fFrameWidth((float)size.x)
	, m_fFrameHeight((float)size.y)
{
	m_iXFrames = 1; // temporary
}

AnimatedObject::~AnimatedObject()
{
	Clear();
}

void AnimatedObject::Update(bool bForce)
{
	// get delta time
	double delta = DeltaTime::GetDelta();
	
	// update the current animation
	m_fCurrentFrame += m_fAnimSpeed * delta;
	
	if (m_fCurrentFrame >= m_Animations[m_iCurrentAnim].size())
		m_fCurrentFrame = 0;
	if (m_fCurrentFrame < 0)
		m_fCurrentFrame = m_Animations[m_iCurrentAnim].size() - 0.01f;
	
	vector3df frame = m_Animations[m_iCurrentAnim][(int)m_fCurrentFrame];
	//ChangeQuadUV(0, frame, vector3df(frame.x + m_fFrameWidth, frame.y + m_fFrameHeight));
	
	// call GLObject's Update() method
	GLObject::Update(bForce);
}

void AnimatedObject::SetAnimationSpeed(float speed)
{
	m_fAnimSpeed = speed;
}

float AnimatedObject::GetAnimationSpeed()
{
	return m_fAnimSpeed;
}

int AnimatedObject::AddAnimation(vector3di start, vector3di end)
{
	// add an animation to this animated object
	vector<vector2df> anim;
	
	int iX = start.x;
	int iY = start.y;
	int length = (end.y * m_iXFrames) - (start.y * m_iXFrames) + (end.x + 1) - start.x;
	
	for (int i = 0; i < length; ++i) {
		anim.push_back(vector2df(m_fFrameWidth * iX, m_fFrameHeight * iY));
		++iX;
		
		if (iX >= m_iXFrames)
		{
			iX = 0;
			++iY;
		}
	}
	
	m_Animations.push_back(anim);
	return m_Animations.size() - 1;
}

void AnimatedObject::SetAnimation(int anim)
{
	m_iCurrentAnim = anim;
}

int AnimatedObject::GetAnimation()
{
	return m_iCurrentAnim;
}

void AnimatedObject::SetCurrentFrame(float frame)
{
	m_fCurrentFrame = frame;
}
