#include "Mesh.h"
#include <fstream>

Mesh::Mesh()
	: m_bufVBO(0)
	, m_bufEBO(0)
	, m_bNeedsCreation(false)
	, m_bClearData(false)
	, m_bDynamic(false)
	, m_IndexCount(0)
{

}

Mesh::~Mesh()
{
	Clear();
	ClearVectors();
}

void Mesh::CopyFrom(const Mesh* m)
{
	Clear();
	ClearVectors();
	AddFrom(m);
}

void Mesh::AddFrom(const Mesh* m, matrix4f mat)
{
	m_Mutex.lock();
	GLuint size = m_Vertices.size();
	GLuint osize = m->m_Vertices.size();
	GLuint isize = m_Indices.size();
	GLuint oisize = m->m_Indices.size();

	//m_Vertices.reserve(size + osize);
	//m_Indices.reserve(isize + oisize);

	// get the inverse transpose of mat for normal calculations
	matrix4f vMat(mat);
	if (vMat.Inverse())
		vMat.Transpose();

	for (GLuint i = 0; i < osize; ++i) {
		GLVertex v(m->m_Vertices[i]);

		vector4df _v(v.pos);
		_v = mat.Multiply(_v);
		v.pos.Set(_v.x + mat.GetDataPointer()[3], _v.y + mat.GetDataPointer()[7], _v.z + mat.GetDataPointer()[11]);

		vector4df _n(v.n.ToVector());
		_n = vMat.Multiply(_n);
		v.n.FromVector(vector3df(_n).Normal());

		m_Vertices.push_back(v);
	}

	for (GLuint i = 0; i < oisize; ++i) {
		m_Indices.push_back(size + m->m_Indices[i]);
	}
	m_Mutex.unlock();
}

void Mesh::MarkAsUncreated()
{
	m_bNeedsCreation = true;
}

void Mesh::ApplyOffset(vector3df offset)
{
	for (GLuint i = 0; i < m_Vertices.size(); ++i) {
		m_Vertices[i].pos += offset;
	}
}

GLuint Mesh::GetVBO()
{
	if (m_bNeedsCreation)
		CreateBufferObjects();

	return m_bufVBO;
}

GLuint Mesh::GetEBO()
{
	if (m_bNeedsCreation)
		CreateBufferObjects();

	return m_bufEBO;
}

GLuint Mesh::GetSize()
{
	return (m_bNeedsCreation ? m_Indices.size() : m_IndexCount);
}

std::vector<GLVertex>* Mesh::GetVertexPointer()
{
	return &m_Vertices;
}

std::vector<GLBoneVertex>* Mesh::GetBoneVertexPointer()
{
	return &m_BoneVertices;
}

std::vector<GLuint>* Mesh::GetIndexPointer()
{
	return &m_Indices;
}

std::vector<Bone>* Mesh::GetSkeletonPointer()
{
	return &m_Bones;
}

map<string, Animation>* Mesh::GetAnimationsPointer()
{
	return &m_Animations;
}

void Mesh::CopyBones(std::vector<Bone>& out)
{
	out.clear();
	for (unsigned int i = 0; i < m_Bones.size(); ++i) {
		out.push_back(m_Bones[i]);
	}
}

void Mesh::CopyAnimations(map<string, Animation>& out)
{
	map<string, Animation>::iterator it = m_Animations.begin();
	while (it != m_Animations.end())
	{
		Animation anim;
		anim.length = it->second.length;

		map<GLfloat, AnimFrame>::iterator fit = it->second.frames.begin();
		while (fit != it->second.frames.end())
		{
			AnimFrame frame;
			for (GLuint i = 0; i < fit->second.rotations.size(); ++i) {
				frame.rotations.push_back(fit->second.rotations[i]);
				frame.translations.push_back(fit->second.translations[i]);
			}
			anim.frames.emplace(fit->first, frame);

			++fit;
		}

		out.emplace(it->first, anim);
		++it;
	}
}

void Mesh::UpdateVBO()
{
	// update just the VBO for this Mesh
	// note if more vertices are added call CreateBufferObjects() instead
	m_Mutex.lock();
	glBindBuffer(GL_ARRAY_BUFFER, m_bufVBO);

	bool bAnimated = IsAnimated();
	size_t size = bAnimated ? m_BoneVertices.size() * sizeof(GLBoneVertex) : m_Vertices.size() * sizeof(GLVertex);

	if (bAnimated)
		glBufferSubData(GL_ARRAY_BUFFER, 0, size, &m_BoneVertices[0]);
	else
		glBufferSubData(GL_ARRAY_BUFFER, 0, size, &m_Vertices[0]);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	m_Mutex.unlock();
}

void Mesh::Clear()
{
	// delete the buffers
	if (m_bufVBO)
		glDeleteBuffers(1, &m_bufVBO);
	if (m_bufEBO)
		glDeleteBuffers(1, &m_bufEBO);

	m_bufVBO = 0;
	m_bufEBO = 0;
	m_IndexCount = 0;
}

void Mesh::ClearVectors()
{
	// clear the vectors
	m_Mutex.lock();
	m_Indices.clear();
	m_Vertices.clear();
	m_BoneVertices.clear();
	m_Mutex.unlock();
}

void Mesh::ChangeAllQuadUV(vector2df uv1, vector2df uv2)
{
	// if we don't have the correct number of vertices do absolutely nothing
	if (m_Vertices.size() % 4 > 0)
		return;

	GLuint size = m_Vertices.size();
	for (GLuint i = 0; i < size; i += 4) {
		m_Vertices[i + 0].uv.u = uv1.x * 8192; m_Vertices[i + 0].uv.v = (1.0f - uv1.y) * 8192;
		m_Vertices[i + 1].uv.u = uv2.x * 8192; m_Vertices[i + 1].uv.v = (1.0f - uv1.y) * 8192;
		m_Vertices[i + 2].uv.u = uv2.x * 8192; m_Vertices[i + 2].uv.v = (1.0f - uv2.y) * 8192;
		m_Vertices[i + 3].uv.u = uv1.x * 8192; m_Vertices[i + 3].uv.v = (1.0f - uv2.y) * 8192;
	}

	UpdateVBO();
}

void Mesh::SetClearData(bool clear)
{
	m_bClearData = clear;
}

void Mesh::CreateBufferObjects()
{
	// If we are a dynamic mesh, follow a different path
	if (m_bDynamic)
	{
		m_Mutex.lock();

		if ((m_Vertices.empty() && m_BoneVertices.empty()) || m_Indices.empty())
		{
			m_Mutex.unlock();
			return;
		}

		// If we don't have a VBO/EBO created, create them now
		if (m_bufVBO == 0 || m_bufEBO == 0)
		{
			glGenBuffers(1, &m_bufVBO);
			glGenBuffers(1, &m_bufEBO);
		}

		// Ensure we have buffers before continuing
		if (m_bufVBO == 0 || m_bufEBO == 0)
		{
			m_Mutex.unlock();
			return;
		}

		// Sort out our buffers!
		bool bAnimated = IsAnimated();
		size_t size = bAnimated ? m_BoneVertices.size() * sizeof(GLBoneVertex) : m_Vertices.size() * sizeof(GLVertex);
		glBindBuffer(GL_ARRAY_BUFFER, m_bufVBO);
		glBufferData(GL_ARRAY_BUFFER, size, NULL, GL_STREAM_DRAW);
		if (bAnimated)
			glBufferSubData(GL_ARRAY_BUFFER, 0, size, &m_BoneVertices[0]);
		else
			glBufferSubData(GL_ARRAY_BUFFER, 0, size, &m_Vertices[0]);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_bufEBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_Indices.size() * sizeof(GLuint), NULL, GL_STREAM_DRAW);
		glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, m_Indices.size() * sizeof(GLuint), &m_Indices[0]);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		// set the index count
		m_IndexCount = m_Indices.size();

		// Set the needs creation flag
		m_bNeedsCreation = false;
		m_Mutex.unlock();
	}
	else
	{
		// create the buffers used by this GLObject
		m_Mutex.lock();
		Clear();

		if ((m_Vertices.empty() && m_BoneVertices.empty()) || m_Indices.empty())
		{
			m_Mutex.unlock();
			return;
		}

		// create the vertex buffer object
		bool bAnimated = IsAnimated();
		size_t size = bAnimated ? m_BoneVertices.size() * sizeof(GLBoneVertex) : m_Vertices.size() * sizeof(GLVertex);
		glGenBuffers(1, &m_bufVBO);
		if (m_bufVBO > 0)
		{
			glBindBuffer(GL_ARRAY_BUFFER, m_bufVBO);
			if (bAnimated)
				glBufferData(GL_ARRAY_BUFFER, size, &m_BoneVertices[0], GL_STATIC_DRAW);
			else
				glBufferData(GL_ARRAY_BUFFER, size, &m_Vertices[0], GL_STATIC_DRAW);

			// create the element buffer object
			glGenBuffers(1, &m_bufEBO);
			if (m_bufEBO > 0)
			{
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_bufEBO);
				glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_Indices.size() * sizeof(GLuint), &m_Indices[0], GL_STATIC_DRAW);

				// set the index count
				m_IndexCount = m_Indices.size();

				// clear if we are set to
				if (m_bClearData)
				{
					m_Vertices.clear();
					m_BoneVertices.clear();
					m_Indices.clear();
				}
			}
			else
			{
				glDeleteBuffers(1, &m_bufVBO);
				m_bufVBO = 0;
			}

			//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0)
		}

		// set the needs creation flag
		m_bNeedsCreation = false;
		m_Mutex.unlock();
	}
}

// .OBJ MESH LOADING

void ReadFloat(float& out, string& line, int& offset)
{
	// used in OBJ loading to read a float
	int place = line.find(' ', offset + 1);
	if (place == string::npos)
		place = line.length();
	int length = -offset + place;
	out = (float)atof(line.substr(offset, length).c_str());
	offset += length + 1;
}

bool IsNear(const float& one, float& two)
{
	return (abs(one - two) < 0.01f);
}

bool DoesVertexExist(vector<GLVertex>& verts, vector3df& pos, vector2df& uv, vector3df& normal, GLuint& index)
{
	for (GLuint i = 0; i < verts.size(); ++i) {
		if (IsNear(verts[i].pos.x, pos.x) &&
			IsNear(verts[i].pos.y, pos.y) &&
			IsNear(verts[i].pos.z, pos.z) && 
			IsNear((GLfloat)verts[i].uv.u / 8192.f, uv.x) &&
			IsNear((GLfloat)verts[i].uv.v / 8192.f, uv.y)/* &&
			IsNear((float)verts[i].n.x, normal.x) &&
			IsNear((float)verts[i].n.y, normal.y) &&
			IsNear((float)verts[i].n.z, normal.z)*/)
		{
			index = i;
			return true;
		}
	}

	return false;
}

bool DoesVertexExist(vector<GLBoneVertex>& verts, vector3df& pos, vector2df& uv, vector3df& normal, GLuint& index)
{
	for (GLuint i = 0; i < verts.size(); ++i) {
		if (IsNear(verts[i].pos.x, pos.x) &&
			IsNear(verts[i].pos.y, pos.y) &&
			IsNear(verts[i].pos.z, pos.z) &&
			IsNear((GLfloat)verts[i].uv.u / 8192.f, uv.x) &&
			IsNear((GLfloat)verts[i].uv.v / 8192.f, uv.y)/* &&
									   IsNear((float)verts[i].n.x, normal.x) &&
									   IsNear((float)verts[i].n.y, normal.y) &&
									   IsNear((float)verts[i].n.z, normal.z)*/)
		{
			index = i;
			return true;
		}
	}

	return false;
}

GLuint CountChar(const string& str, const char c)
{
	int count = 0;

	for (unsigned int i = 0; i < str.length(); ++i) {
		if (str[i] == c)
			++count;
	}

	return count;
}

bool Mesh::LoadFromOBJ(string filename)
{
	// load a mesh from a .obj file
	vector<vector3df> vertices;
	vector<vector2df> uvs;
	vector<vector3df> normals;

	// open the file for reading
	std::ifstream file(string("media/").append(filename), std::ios::in);
	if (!file.is_open())
		return false;

	// read the data into the vectors to assemble later
	string line;
	while (getline(file, line))
	{
		if (line[0] == 'v')
		{
			if (line[1] == 't')
			{
				// uv coordinates
				float x, y;
				int offset = 3;
				ReadFloat(x, line, offset);
				ReadFloat(y, line, offset);
				Limit(x, 0.0f, 1.0f);
				Limit(y, 0.0f, 1.0f);
				uvs.push_back(vector2df(x, y));
				//cout << x << ", " << y << " - " << uvs.size() << endl;
			}
			else if (line[1] == 'n')
			{
				// vertex normals
				float x, y, z;
				int offset = 3;
				ReadFloat(x, line, offset);
				ReadFloat(y, line, offset);
				ReadFloat(z, line, offset);
				normals.push_back(vector3df(x, y, z));
				//cout << x << ", " << y << ", " << z << " - " << normals.size() << endl;
			}
			else
			{
				// vertex positions
				float x, y, z;
				int offset = 2;
				ReadFloat(x, line, offset);
				ReadFloat(y, line, offset);
				ReadFloat(z, line, offset);
				vertices.push_back(vector3df(x, y, z));
				//cout << x << ", " << y << ", " << z << " - " << vertices.size() << endl;
			}
		}
		else if (line[0] == 'f')
		{
			// faces
			GLuint count = CountChar(line, '/') / 2;
			int vi, ui, ni, offset = 2, tmp;

			for (unsigned int i = 0; i < count; ++i) {
				tmp = line.find('/', offset);
				vi = atoi(line.substr(offset, tmp - offset).c_str()) - 1;
				offset += (tmp - offset) + 1;
				if (uvs.size() > 0)
				{
					tmp = line.find('/', offset);
					ui = atoi(line.substr(offset, tmp - offset).c_str()) - 1;
					offset += (tmp - offset) + 1;
				}
				else ++offset;
				tmp = line.find(' ', offset);
				if (line.find(' ', offset) == string::npos)
					tmp = line.length();
				ni = atoi(line.substr(offset, tmp - offset).c_str()) - 1;
				offset += (tmp - offset) + 1;

				//cout << vi << "/" << ui << "/" << ni << " ";

				GLuint index = m_Vertices.size();
				vector2df uv;
				if (uvs.size() > 0)
					uv = uvs[ui];
				if (count == 3)
				{
					if (DoesVertexExist(m_Vertices, vertices[vi], uv, normals[ni], index))
						m_Indices.push_back(index);
					else
					{
						m_Vertices.push_back(GLVertex(vertices[vi], GLColour(), uv, GLNormal(normals[ni])));
						m_Indices.push_back(index);
					}
				}
				else
				{
					m_Vertices.push_back(GLVertex(vertices[vi], GLColour(), uv, GLNormal(normals[ni])));
				}
			}

			if (count > 3)
			{
				GLuint index = m_Vertices.size();
				GLuint start = index - count;
				for (GLuint i = 1; i < count - 1; ++i) {
					m_Indices.push_back(start); m_Indices.push_back(start + i); m_Indices.push_back(start + i + 1);
				}
			}
		}
	}
	file.close();
	CreateBufferObjects();

	return true;
}

// .AOBJ MESH LOADING

bool Mesh::LoadFromAOBJ(string filename)
{
	// load a mesh from an .aob file
	vector<vector3df> vertices;
	vector<vector2df> uvs;
	vector<vector3df> normals;
	string animName("");

	// open the file for reading
	std::ifstream file(string("media/").append(filename), std::ios::in);
	if (!file.is_open())
		return false;

	// read the data into the vectors to assemble later
	string line;
	while (getline(file, line))
	{
		if (line[0] == 'v')
		{
			if (line[1] == 't')
			{
				// uv coordinates
				float x, y;
				int offset = 3;
				ReadFloat(x, line, offset);
				ReadFloat(y, line, offset);
				uvs.push_back(vector2df(x, y));
				//cout << x << ", " << y << " - " << uvs.size() << endl;
			}
			else if (line[1] == 'n')
			{
				// vertex normals
				float x, y, z;
				int offset = 3;
				ReadFloat(x, line, offset);
				ReadFloat(y, line, offset);
				ReadFloat(z, line, offset);
				normals.push_back(vector3df(x, y, z));
				//cout << x << ", " << y << ", " << z << " - " << normals.size() << endl;
			}
			else
			{
				// vertex positions
				float x, y, z;
				int offset = 2;
				ReadFloat(x, line, offset);
				ReadFloat(y, line, offset);
				ReadFloat(z, line, offset);
				vertices.push_back(vector3df(x, y, z));
				//cout << x << ", " << y << ", " << z << " - " << vertices.size() << endl;
			}
		}
		else if (line[0] == 'b')
		{
			// read in this bone's bind position
			Bone b;
			int offset = 2;
			float x, y, z;
			ReadFloat(x, line, offset);
			ReadFloat(y, line, offset);
			ReadFloat(z, line, offset);
			b.bind.Set(x, y, z);

			int p = atoi(line.substr(offset, line.length() - 1).c_str());
			b.parent = p;
			m_Bones.push_back(b);

			//cout << x << ", " << y << ", " << z << " : " << p << endl;
		}
		else if (line[0] == 'f')
		{
			// faces
			GLuint count = CountChar(line, '/') / 2;
			int vi, ui, ni, offset = 2, tmp;
			vector4di bones;
			vector4df weights;

			for (unsigned int i = 0; i < count; ++i) {
				bool hasbone = true;
				tmp = line.find('/', offset);
				vi = atoi(line.substr(offset, tmp - offset).c_str()) - 1;
				offset += (tmp - offset) + 1;
				if (uvs.size() > 0)
				{
					tmp = line.find('/', offset);
					ui = atoi(line.substr(offset, tmp - offset).c_str()) - 1;
					offset += (tmp - offset) + 1;
				}
				else ++offset;
				tmp = line.find('\\', offset);
				if (line.find('\\', offset) == string::npos)
				{
					hasbone = false;
					tmp = line.find(' ', offset);
					if (line.find(' ', offset) == string::npos)
						tmp = line.length();
				}
				ni = atoi(line.substr(offset, tmp - offset).c_str()) - 1;
				offset += (tmp - offset) + 1;

				if (hasbone)
				{
					// find out how many bones there are for this vertex
					GLuint bonecount = 0;

					// see if we're at the end of the line here
					offset--;
					if (line.find(' ', offset) == string::npos)
						tmp = line.length();
					else
						tmp = line.find(' ', offset);

					// count the bones
					bonecount = CountChar(line.substr(offset, tmp - offset), '\\');
					++offset;

					int _bones[4];
					float _weights[4];
					unsigned int j = 0;

					for (; j < 4; ++j) {
						_bones[j] = -1;
						_weights[j] = 0;
					}

					for (j = 0; j < bonecount; ++j) {
						tmp = line.find(',', offset);
						_bones[j] = atoi(line.substr(offset, tmp - offset).c_str());
						offset += (tmp - offset) + 1;

						if (j < bonecount - 1)
						{
							tmp = line.find('\\', offset);
							_weights[j] = (float)atof(line.substr(offset, tmp - offset).c_str());
							offset += (tmp - offset) + 1;
						}
						else
						{
							if (line.find(' ', offset) == string::npos)
								tmp = line.length();
							else
								tmp = line.find(' ', offset);
							_weights[j] = (float)atof(line.substr(offset, tmp - offset).c_str());
							offset += (tmp - offset) + 1;
						}

						//cout << "\\" << _bones[j] << "," << _weights[j];
					}

					bones.Set(_bones);
					weights.Set(_weights);
				}

				//cout << vi << "/" << ui << "/" << ni << " ";

				GLuint index = m_BoneVertices.size();
				vector2df uv;
				if (uvs.size() > 0)
					uv = uvs[ui];
				if (count == 3)
				{
					if (DoesVertexExist(m_BoneVertices, vertices[vi], uv, normals[ni], index))
						m_Indices.push_back(index);
					else
					{
						m_BoneVertices.push_back(GLBoneVertex(vertices[vi], GLColour(), uv, GLNormal(normals[ni]), GLColour(), GLNormal(0, 1, 0), bones, weights));
						m_Indices.push_back(index);
					}
				}
				else
				{
					m_BoneVertices.push_back(GLBoneVertex(vertices[vi], GLColour(), uv, GLNormal(normals[ni]), GLColour(), GLNormal(0, 1, 0), bones, weights));
				}
				//cout << weights.x << ", " << weights.y << ", " << weights.z << ", " << weights.w << endl;
			}

			if (count > 3)
			{
				GLuint index = m_BoneVertices.size();
				GLuint start = index - count;
				for (GLuint i = 1; i < count - 1; ++i) {
					m_Indices.push_back(start); m_Indices.push_back(start + i); m_Indices.push_back(start + i + 1);
				}
			}
		}
		else if (line[0] == 'a')
		{
			// start of a new animation
			int offset = line.find('"', 3) - 3;
			animName = line.substr(3, offset);
			Animation anim;
			offset = animName.length() + 4;
			ReadFloat(anim.length, line, offset);

			m_Animations.emplace(animName, anim);
		}
		else if (line[0] == 't')
		{
			Animation& anim = m_Animations[animName];
			GLfloat time = (GLfloat)atof(line.substr(2, line.length() - 2).c_str());

			AnimFrame frame;
			frame.time = time;
			anim.frames.emplace(time, frame);
		}
		else if (line[0] == 'r')
		{
			Animation& anim = m_Animations[animName];
			int offset = 2;
			float x, y, z;
			ReadFloat(x, line, offset);
			ReadFloat(y, line, offset);
			ReadFloat(z, line, offset);
			vector3df rot(x, y, z);
			offset += 2;
			ReadFloat(x, line, offset);
			ReadFloat(y, line, offset);
			ReadFloat(z, line, offset);
			vector3df pos(x, y, z);

			AnimFrame& frame = (--anim.frames.end())->second;
			frame.rotations.push_back(rot);
			frame.translations.push_back(pos);
		}
	}
	file.close();
	CreateBufferObjects();

	return true;
}

// TERRAIN CUBE CREATION

void Mesh::AddQuad(GLVertex ver1, GLVertex ver2, GLVertex ver3, GLVertex ver4, bool flip)
{
	// add a quad to this GLObject - this quad has a white colour and is a square / rectangle
	// returns an integer position of the new vertices added
	GLuint iIndex = m_Vertices.size();

	// sort out the UV coordinates
	GLushort temp;
	ver1.uv.v = 8192 - ver1.uv.v;
	ver2.uv.v = 8192 - ver2.uv.v;
	ver3.uv.v = 8192 - ver3.uv.v;
	ver4.uv.v = 8192 - ver4.uv.v;

	temp = ver1.uv.v;
	ver1.uv.v = ver4.uv.v;
	ver4.uv.v = temp;

	temp = ver2.uv.v;
	ver2.uv.v = ver3.uv.v;
	ver3.uv.v = temp;

	// stop textures from having dem annoyin white lines yo
	/*ver1.uv *= 1.0000002f;
	ver2.uv *= 1.0000002f;
	ver3.uv *= 1.0000002f;
	ver4.uv *= 1.0000002f;*/

	// push back the vertices
	m_Vertices.push_back(ver1); m_Vertices.push_back(ver2); m_Vertices.push_back(ver3); m_Vertices.push_back(ver4);

	// add to the indices
	if (flip)
	{
		m_Indices.push_back(iIndex + 1); m_Indices.push_back(iIndex + 2); m_Indices.push_back(iIndex + 3);
		m_Indices.push_back(iIndex + 3); m_Indices.push_back(iIndex + 0); m_Indices.push_back(iIndex + 1);
	}
	else
	{
		m_Indices.push_back(iIndex + 0); m_Indices.push_back(iIndex + 1); m_Indices.push_back(iIndex + 2);
		m_Indices.push_back(iIndex + 2); m_Indices.push_back(iIndex + 3); m_Indices.push_back(iIndex + 0);
	}
}

void Mesh::CreateTerrainBlockMesh(const vector4df uvs[6])
{
	AddQuad(GLVertex(vector3df(-5, 0, 5), GLColour(), vector2df(uvs[5].x, uvs[5].y), GLNormal(0, 0, 1)),
		GLVertex(vector3df(5, 0, 5), GLColour(), vector2df(uvs[5].z, uvs[5].y), GLNormal(0, 0, 1)),
		GLVertex(vector3df(5, 10, 5), GLColour(), vector2df(uvs[5].z, uvs[5].w), GLNormal(0, 0, 1)),
		GLVertex(vector3df(-5, 10, 5), GLColour(), vector2df(uvs[5].x, uvs[5].w), GLNormal(0, 0, 1)));
	AddQuad(GLVertex(vector3df(5, 0, -5), GLColour(), vector2df(uvs[4].x, uvs[4].y), GLNormal(0, 0, -1)),
		GLVertex(vector3df(-5, 0, -5), GLColour(), vector2df(uvs[4].z, uvs[4].y), GLNormal(0, 0, -1)),
		GLVertex(vector3df(-5, 10, -5), GLColour(), vector2df(uvs[4].z, uvs[4].w), GLNormal(0, 0, -1)),
		GLVertex(vector3df(5, 10, -5), GLColour(), vector2df(uvs[4].x, uvs[4].w), GLNormal(0, 0, -1)));
	AddQuad(GLVertex(vector3df(-5, 0, -5), GLColour(), vector2df(uvs[2].x, uvs[2].y), GLNormal(-1, 0, 0)),
		GLVertex(vector3df(-5, 0, 5), GLColour(), vector2df(uvs[2].z, uvs[2].y), GLNormal(-1, 0, 0)),
		GLVertex(vector3df(-5, 10, 5), GLColour(), vector2df(uvs[2].z, uvs[2].w), GLNormal(-1, 0, 0)),
		GLVertex(vector3df(-5, 10, -5), GLColour(), vector2df(uvs[2].x, uvs[2].w), GLNormal(-1, 0, 0)));
	AddQuad(GLVertex(vector3df(5, 0, 5), GLColour(), vector2df(uvs[3].x, uvs[3].y), GLNormal(1, 0, 0)),
		GLVertex(vector3df(5, 0, -5), GLColour(), vector2df(uvs[3].z, uvs[3].y), GLNormal(1, 0, 0)),
		GLVertex(vector3df(5, 10, -5), GLColour(), vector2df(uvs[3].z, uvs[3].w), GLNormal(1, 0, 0)),
		GLVertex(vector3df(5, 10, 5), GLColour(), vector2df(uvs[3].x, uvs[3].w), GLNormal(1, 0, 0)));
	AddQuad(GLVertex(vector3df(5, 10, -5), GLColour(), vector2df(uvs[0].x, uvs[0].y), GLNormal(0, 1, 0)),
		GLVertex(vector3df(-5, 10, -5), GLColour(), vector2df(uvs[0].z, uvs[0].y), GLNormal(0, 1, 0)),
		GLVertex(vector3df(-5, 10, 5), GLColour(), vector2df(uvs[0].z, uvs[0].w), GLNormal(0, 1, 0)),
		GLVertex(vector3df(5, 10, 5), GLColour(), vector2df(uvs[0].x, uvs[0].w), GLNormal(0, 1, 0)));
	AddQuad(GLVertex(vector3df(5, 0, 5), GLColour(), vector2df(uvs[1].x, uvs[1].y), GLNormal(0, -1, 0)),
		GLVertex(vector3df(-5, 0, 5), GLColour(), vector2df(uvs[1].z, uvs[1].y), GLNormal(0, -1, 0)),
		GLVertex(vector3df(-5, 0, -5), GLColour(), vector2df(uvs[1].z, uvs[1].w), GLNormal(0, -1, 0)),
		GLVertex(vector3df(5, 0, -5), GLColour(), vector2df(uvs[1].x, uvs[1].w), GLNormal(0, -1, 0)));
	CreateBufferObjects();
}

bool Mesh::IsAnimated()
{
	return !m_BoneVertices.empty();
}

// Custom mesh creation

void Mesh::AddTriangle(const GLVertex& ver1, const GLVertex& ver2, const GLVertex& ver3)
{
	// add a triangle to this GLObject
	GLuint iIndex = m_Vertices.size();

	// sort out the UV coordinates
	/*ver1.uv.v = 8192 - ver1.uv.v;
	ver2.uv.v = 8192 - ver2.uv.v;
	ver3.uv.v = 8192 - ver3.uv.v;*/

	// calculate the tangent
	/*GLNormal tangent = CalculateTangent(ver1, ver2, ver3);
	ver1.t = tangent; ver2.t = tangent; ver3.t = tangent;*/

	// push back the vertices
	m_Vertices.push_back(ver1); m_Vertices.push_back(ver2); m_Vertices.push_back(ver3);

	// add to the indices
	m_Indices.push_back(iIndex + 0); m_Indices.push_back(iIndex + 1); m_Indices.push_back(iIndex + 2);
}

void Mesh::ReserveVertices(size_t count)
{
	m_Vertices.reserve(count);
}

void Mesh::ReserveIndices(size_t count)
{
	m_Indices.reserve(count);
}

void Mesh::SetDynamic(bool dynamic)
{
	m_bDynamic = dynamic;
}
