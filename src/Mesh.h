#ifndef _MESH_H_
#define _MESH_H_

#include "Misc.h"
#include <mutex>

// A Mesh is basically a trimmed down version of GLObject - I could have extended that but I didn't want a lot of the crap involved with it

class Mesh {
	public:
		Mesh();
		~Mesh();

		void CopyFrom(const Mesh* m);
		void AddFrom(const Mesh* m, matrix4f mat = matrix4f());
		void MarkAsUncreated();

		void ApplyOffset(vector3df offset);

		bool LoadFromOBJ(string filename);
		bool LoadFromAOBJ(string filename);
		void CreateTerrainBlockMesh(const vector4df uvs[6]);

		bool IsAnimated();

		GLuint GetVBO();
		GLuint GetEBO();
		GLuint GetSize();

		std::vector<GLVertex>* GetVertexPointer();
		std::vector<GLBoneVertex>* GetBoneVertexPointer();
		std::vector<GLuint>* GetIndexPointer();
		std::vector<Bone>* GetSkeletonPointer();
		map<string, Animation>* GetAnimationsPointer();
		void CopyBones(std::vector<Bone>& out);
		void CopyAnimations(map<string, Animation>& out);

		void ClearVectors();

		void ChangeAllQuadUV(vector2df uv1, vector2df uv2);

		void SetClearData(bool clear);

		void AddTriangle(const GLVertex& ver1, const GLVertex& ver2, const GLVertex& ver3);
		void AddQuad(GLVertex ver1, GLVertex ver2, GLVertex ver3, GLVertex ver4, bool flip = false);

		void ReserveVertices(size_t count);
		void ReserveIndices(size_t count);

		void SetDynamic(bool dynamic);

	protected:

	private:
		std::vector<GLVertex>		m_Vertices;
		std::vector<GLBoneVertex>	m_BoneVertices;
		std::vector<GLuint>			m_Indices;
		std::vector<Bone>			m_Bones;
		map<string, Animation>		m_Animations;
		GLuint						m_bufVBO;
		GLuint						m_bufEBO;
		bool						m_bNeedsCreation;
		bool						m_bClearData;
		bool						m_bDynamic;
		std::mutex					m_Mutex;
		GLuint						m_IndexCount;

		void UpdateVBO();
		void Clear();
		void CreateBufferObjects();
};

#endif
