#
# Makefile - improved version of my generic (enough) makefile - includes release and debug builds
# Should be easy enough to add more build targets - see editor_of_gods project for examples (or figure it out)
# Place source files in /src - obj files will go in the relevant build directory for the target
# Note this WILL compile EVERYTHING in src as it looks for .cpp files to compile to objects
# Can easily specify exact files/ordering if you create your own <target>_SOURCES and modify accordingly
#

# Executable name
PROGRAM = pyre_of_gods.out

# Include directories
INCLUDEDIRS = \
	
	
# Library directories
LIBDIRS = \
	

# Libraries
LIBS = \
	-lz -lpthread -lGLEW -lGL -lpng -lglfw

LDFLAGS = $(LIBDIRS) $(LIBS)

# Source files
SOURCES = $(wildcard src/*.cpp)
OBJECTS = $(notdir $(SOURCES:.cpp=.o))

# Common for all targets (other than clean)
CXXFLAGS = -DESRI_UNIX -std=c++17 $(INCLUDEDIRS)
CXX = g++

#
# Beginning of build-specific settings
#

# Release build settings
RELEASE_DIR = release
RELEASE_TARGET = $(RELEASE_DIR)/$(PROGRAM)
RELEASE_OBJ = $(addprefix $(RELEASE_DIR)/obj/,$(OBJECTS))
RELEASE_FLAGS = -O3 -no-pie

# Debug build settings
DEBUG_DIR = debug
DEBUG_TARGET = $(DEBUG_DIR)/$(PROGRAM)
DEBUG_OBJ = $(addprefix $(DEBUG_DIR)/obj/,$(OBJECTS))
DEBUG_FLAGS = -g -D_DEBUG

#
# Common build targets
#

.PHONY: clean remake prep

# Default make target
all: prep release

# Clean target
clean:
	rm -f $(RELEASE_TARGET) $(RELEASE_OBJ) $(DEBUG_TARGET) $(DEBUG_OBJ)

# Remake target
remake:
	clean all

# Prep target
prep:
	@mkdir -p $(RELEASE_DIR) $(RELEASE_DIR)/obj $(DEBUG_DIR) $(DEBUG_DIR)/obj

#
# Beginning of build-specific targets
#

# Release target
release: $(RELEASE_TARGET)

$(RELEASE_TARGET): $(RELEASE_OBJ)
	$(CXX) $(LIBDIRS) $(RELEASE_FLAGS) -o $@ $^ $(LIBS)

$(RELEASE_DIR)/obj/%.o: src/%.cpp
	$(CXX) $(CXXFLAGS) $(RELEASE_FLAGS) -c -o $@ $<

# Debug target
debug: $(DEBUG_TARGET)

$(DEBUG_TARGET): $(DEBUG_OBJ)
	$(CXX) $(LIBDIRS) $(DEBUG_FLAGS) -o $@ $^ $(LIBS)

$(DEBUG_DIR)/obj/%.o: src/%.cpp
	$(CXX) $(CXXFLAGS) $(DEBUG_FLAGS) -c -o $@ $<

